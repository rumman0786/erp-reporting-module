/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author rumman
 * should contain orderInHand,WFCA,WFBA,DeliveryStatus
 * ???
 */
public class ReportDailyReport {
  private String orderCondition;//fromDate, toDate,
    //for order in hand
    BigDecimal tTotal = new BigDecimal(0),tTotalAmout = new BigDecimal(0),tTodayDelivery = new BigDecimal(0) ,tBalanceQuantity = new BigDecimal(0);
    //for wfca
    BigDecimal tValue = new BigDecimal(0),tTotalValue = new BigDecimal(0);
    public void initiate(String orderCondition) throws IOException, //String fromDate, String toDate,
            DocumentException, Exception {
//        this.fromDate = fromDate;
//        this.toDate = toDate;
        String desktopPath = System.getProperty("user.home") + "\\Desktop\\";
        this.orderCondition = orderCondition ;
        System.out.println("Before:: In initiate");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String result = desktopPath+"Daily_Report" + "_"+date+ "_.pdf";
        System.out.println(result);
        Document document = new Document(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream( result));
        final Chunk NEWLINE = new Chunk("\n");
         System.out.println("Before:: In initiate after date");
        document.open();
        
        document.add(createTableOrderInHand());
        document.add(new Paragraph("Amount: "+tTotal));
        document.add(new Paragraph("Total Amount : "+tTotalAmout));
        document.add(new Paragraph("Total Today Delivery: "+tTodayDelivery));
        document.add(new Paragraph("Total Balance QUantity: "+tBalanceQuantity));
        document.add( Chunk.NEWLINE );
        document.add( Chunk.NEWLINE );
        
        document.add(createTableWFCA());
        document.add(new Paragraph("Amount: "+tTotal));
        document.add(new Paragraph("Total Amount : "+tTotalAmout));
        document.add(new Paragraph("Total Today Delivery: "+tTodayDelivery));
        document.add(new Paragraph("Total Balance QUantity: "+tBalanceQuantity));
        document.add( Chunk.NEWLINE );
        document.add( Chunk.NEWLINE );
        
        document.add(createTableWFBA());
        document.add(new Paragraph("Amount: "+tTotal));
        document.add(new Paragraph("Total Amount : "+tTotalAmout));
        document.add(new Paragraph("Total Today Delivery: "+tTodayDelivery));
        document.add(new Paragraph("Total Balance QUantity: "+tBalanceQuantity));
        document.add( Chunk.NEWLINE );
        document.add( Chunk.NEWLINE );
        
        
        
        document.close();
        System.out.println("After:: In Create Table");
    }

    private PdfPTable createTableOrderInHand() throws Exception {
        
        System.out.println("In Create Table 1");
        PdfPTable table = new PdfPTable(14);
        table.setWidths(new int[]{2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 2, 2});
        PdfPCell cell;
        String[] coloumns = {"Buyer", "L/C#", "L/C Date",
            "Last Date of\n shipment", "Expiry Date", "L/C Sight",
            "PI No", "Count", "Qty", "Rate", "Amount", "Total Amount",
            "Today\nDelivery", "Bal Qty (kg)"};
        Font font = new Font(Font.FontFamily.COURIER, 7, Font.BOLD,
                BaseColor.BLACK);
        cell = new PdfPCell(new Phrase("Order In Hand", font));
        cell.setColspan(14);
        table.addCell(cell);
        for (String colname : coloumns) {
            cell = new PdfPCell(new Phrase(colname, font));
            table.addCell(cell);
        }
        FetchDataOrderInHand fd = new FetchDataOrderInHand(orderCondition);//fromDate, toDate,
        System.out.println("In Create Table 2");
        fd.readDataBase();
        ArrayList<OrderInHand> orderInHand = fd.getOrderInHand();

        for (OrderInHand o : orderInHand) {
            cell = new PdfPCell(new Phrase(o.getName(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLast_shipment_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getExpiry_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_sight_days(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getPi_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getCount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getQuantity(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getRate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getAmount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTotalAmount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTodayDelivery(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getBalanceQuantity(), font));
            table.addCell(cell);
            
            tTotal = tTotal.add(BigDecimal.valueOf(o.getAmount())); 
            tTotalAmout = tTotalAmout.add(BigDecimal.valueOf(o.getAmount()));
            tTodayDelivery = tTodayDelivery.add(BigDecimal.valueOf(o.getTodayDelivery())); 
            tBalanceQuantity = tBalanceQuantity.add(BigDecimal.valueOf(o.getBalanceQuantity())); 
        }
        
        return table;
    }
    private PdfPTable createTableWFCA() throws Exception {
        
        System.out.println("ReportWFCA In Create Table 1");
        PdfPTable table = new PdfPTable(16);
        table.setWidths(new int[]{2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2});
        PdfPCell cell;
        String[] coloumns = {"Buyer", "L/C#", "L/C Date",
            "Last Date of\n shipment", "Expiry Date", "L/C Sight",
            "PI No","Del.Com.Date","Submission Date","Comments",
            "Doc Han ov. Date", "Count", "Qty", "Rate", "Value", "Total Value"
            };
        Font font = new Font(Font.FontFamily.COURIER, 7, Font.BOLD,
                BaseColor.BLACK);
        cell = new PdfPCell(new Phrase("WFCA", font));
        cell.setColspan(16);
        table.addCell(cell);
        for (String colname : coloumns) {
            cell = new PdfPCell(new Phrase(colname, font));
            table.addCell(cell);
        }
        FetchDataWFCA fd = new FetchDataWFCA(orderCondition);//fromDate, toDate,
        System.out.println("ReportWFCA :In Create Table 2");
        fd.readDataBase();
        ArrayList<WFCA> wfcaObject = fd.getWFCA();

        for (WFCA o : wfcaObject) {
            cell = new PdfPCell(new Phrase(o.getBuyer(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLast_shipment_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getExpiry_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_sight_days(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getPi_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getDel_Com_Date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getSubmissionDate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getComments(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getDocHandovDate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getCount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getQuantity(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getRate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getValue(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTotalValue(), font));
            table.addCell(cell);
            tValue = tValue.add(BigDecimal.valueOf(o.getValue())); 
            tTotalValue = tTotalValue.add(BigDecimal.valueOf(o.getTotalValue()));
            
        }
        
        return table;
    }
    
    private PdfPTable createTableWFBA() throws Exception {
        
        System.out.println("ReportWFCA In Create Table 1");
        PdfPTable table = new PdfPTable(17);
        table.setWidths(new int[]{2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2});
        PdfPCell cell;
        String[] coloumns = {"Buyer", "L/C#", "L/C Date",
            "Last Date of\n shipment", "Expiry Date", "L/C Sight",
            "PI No","Doc.Rec.Date","Bank Sub Dt","Comments",
            "Maturity Date", "LDBP No","Count", "Qty", "Rate", "Value", "Total Value"
            };
        Font font = new Font(Font.FontFamily.COURIER, 7, Font.BOLD,
                BaseColor.BLACK);
        cell = new PdfPCell(new Phrase("WFBA", font));
        cell.setColspan(17);
        table.addCell(cell);
        for (String colname : coloumns) {
            cell = new PdfPCell(new Phrase(colname, font));
            table.addCell(cell);
        }
        FetchDataWFBA fd = new FetchDataWFBA(orderCondition);//fromDate, toDate,
        System.out.println("ReportWFCA :In Create Table 2");
        fd.readDataBase();
        ArrayList<WFBA> wfbaObject = fd.getWFBA();

        for (WFBA o : wfbaObject) {
            cell = new PdfPCell(new Phrase(o.getBuyer(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLast_shipment_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getExpiry_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_sight_days(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getPi_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getDoc_Rec_Date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getBank_Sub_Date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getComments(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getMaturityDate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLdbpNO(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getCount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getQuantity(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getRate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getValue(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTotalValue(), font));
            table.addCell(cell);
            tValue = tValue.add(BigDecimal.valueOf(o.getValue())); 
            tTotalValue = tTotalValue.add(BigDecimal.valueOf(o.getTotalValue()));
            
        }
        
        return table;
    }
}
