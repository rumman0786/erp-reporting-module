/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class WFBA {

    private String buyer;
    private String lc_id;
    private String lc_date;
    private String last_shipment_date;
    private String expiry_date;
    private String lc_sight_days;
    private String pi_id;
    private String doc_Rec_Date;
    private String bank_Sub_Date;
    private String comments;
    private String maturityDate;
    private String ldbpNO;
    private String count;
    private double quantity;
    private double rate;
    private double value = 0.0;
    private double totalValue = 0.0;

    public WFBA(String buyer, String lc_id, String lc_date, String last_shipment_date, String expiry_date,
            String lc_sight_days, String pi_id, String doc_Rec_Date, String bank_Sub_Date, String comments,
            String maturityDate, String ldbpNO, String count, double quantity, double rate, double value, double totalValue) {
        this.buyer = buyer;
        this.lc_id = lc_id;
        this.lc_date = lc_date;
        this.last_shipment_date = last_shipment_date;
        this.expiry_date = expiry_date;
        this.lc_sight_days = lc_sight_days;
        this.pi_id = pi_id;
        this.doc_Rec_Date = doc_Rec_Date;
        this.bank_Sub_Date = bank_Sub_Date;
        this.comments = comments;
        this.maturityDate = maturityDate;
        this.ldbpNO = ldbpNO;
        this.count = count;
        this.quantity = quantity;
        this.rate = rate;
        this.value = value;
        this.totalValue = totalValue;
    }

    public String getBuyer() {
        return buyer;
    }

    public String getLc_id() {
        return lc_id;
    }

    public String getLc_date() {
        return lc_date;
    }

    public String getLast_shipment_date() {
        return last_shipment_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public String getLc_sight_days() {
        return lc_sight_days;
    }

    public String getPi_id() {
        return pi_id;
    }

    public String getDoc_Rec_Date() {
        return doc_Rec_Date;
    }

    public String getBank_Sub_Date() {
        return bank_Sub_Date;
    }

    public String getComments() {
        return comments;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public String getLdbpNO() {
        return ldbpNO;
    }

    public String getCount() {
        return count;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getRate() {
        return rate;
    }

    public double getValue() {
        return value;
    }

    public double getTotalValue() {
        return totalValue;
    }
}
