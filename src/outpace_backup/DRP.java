/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class DRP {

    private String buyer;
    private String lc_id;
    private String lc_date;
    private String last_shipment_date;
    private String expiry_date;
    private String docRecDate;
    private String submissionDate;
    private String issuingDateMaturity;
    private String maturityDate;
    private String purchaseDate;
    private String fddIssueDate;
    private double fddAmount;
    private String adjustedDate;
    private String realizationDate;
    private String ldbpNO;
    private String count;
    private double quantity;
    private double rate;
    private double value = 0.0;
    private double totalValue = 0.0;
    private double exim = 0.0;
    private double scb = 0.0;

    public DRP(String buyer, String lc_id, String lc_date, String last_shipment_date,
            String expiry_date, String docRecDate, String submissionDate,
            String issuingDateMaturity, String maturityDate, String purchaseDate,
            String fddIssueDate, double fddAmount, String adjustedDate,
            String realizationDate, String ldbpNO, String count, double quantity,
            double rate, double value, double totalValue, double exim, double scb) {
        this.buyer = buyer;
        this.lc_id = lc_id;
        this.lc_date = lc_date;
        this.last_shipment_date = last_shipment_date;
        this.expiry_date = expiry_date;
        this.docRecDate = docRecDate;
        this.submissionDate = submissionDate;
        this.issuingDateMaturity = issuingDateMaturity;
        this.maturityDate = maturityDate;
        this.purchaseDate = purchaseDate;
        this.fddIssueDate = fddIssueDate;
        this.fddAmount = fddAmount;
        this.adjustedDate = adjustedDate;
        this.realizationDate = realizationDate;
        this.ldbpNO = ldbpNO;
        this.count = count;
        this.quantity = quantity;
        this.rate = rate;
        this.value = value;
        this.totalValue = totalValue;
        this.exim = exim;
        this.scb = scb;

    }

    public String getBuyer() {
        return buyer;
    }

    public String getLc_id() {
        return lc_id;
    }

    public String getLc_date() {
        return lc_date;
    }

    public String getLast_shipment_date() {
        return last_shipment_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public String getDocRecDate() {
        return docRecDate;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public String getIssuingDateMaturity() {
        return issuingDateMaturity;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public String getFddIssueDate() {
        return fddIssueDate;
    }

    public double getFddAmount() {
        return fddAmount;
    }

    public String getAdjustedDate() {
        return adjustedDate;
    }

    public String getRealizationDate() {
        return realizationDate;
    }

    public String getLdbpNO() {
        return ldbpNO;
    }

    public String getCount() {
        return count;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getRate() {
        return rate;
    }

    public double getValue() {
        return value;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public double getExim() {
        return exim;
    }

    public double getScb() {
        return scb;
    }
    
    
}
