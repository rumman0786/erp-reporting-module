/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class WFCA {

    private String buyer;
    private String lc_id;
    private String lc_date;
    private String last_shipment_date;
    private String expiry_date;
    private String lc_sight_days;
    private String pi_id;
    private String Del_Com_Date;
    private String submissionDate;
    private String comments;
    private String docHandovDate;
    private String count;
    private double quantity;
    private double rate;
    private double value = 0.0;
    private double totalValue = 0.0;

    public WFCA(String buyer, String lc_id, String lc_date, String last_shipment_date, String expiry_date,
            String lc_sight_days, String pi_id, String Del_Com_Date, String submissionDate, String comments,String docHandovDate,
            String count, double quantity, double rate, double value, double totalValue) {
        // TODO Auto-generated constructor stub
        this.buyer = buyer;
        this.lc_id = lc_id;
        this.lc_date = lc_date;
        this.last_shipment_date = last_shipment_date;
        this.expiry_date = expiry_date;
        this.lc_sight_days = lc_sight_days;
        this.pi_id = pi_id;
        this.Del_Com_Date = Del_Com_Date;
        this.submissionDate = submissionDate;
        this.comments = comments ;
        this.docHandovDate = docHandovDate;
        this.count = count;
        this.quantity = quantity;
        this.rate = rate;
        this.value = value;
        this.totalValue = totalValue;
    }

    public String getBuyer() {
        return buyer;
    }

    public String getLc_id() {
        return lc_id;
    }

    public String getLc_date() {
        return lc_date;
    }

    public String getLast_shipment_date() {
        return last_shipment_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public String getLc_sight_days() {
        return lc_sight_days;
    }

    public String getPi_id() {
        return pi_id;
    }

    public String getDel_Com_Date() {
        return Del_Com_Date;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public String getComments() {
        return comments;
    }

    public String getDocHandovDate() {
        return docHandovDate;
    }

    public String getCount() {
        return count;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getRate() {
        return rate;
    }

    public double getValue() {
        return value;
    }

    public double getTotalValue() {
        return totalValue;
    }

}
