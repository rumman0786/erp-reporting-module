/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outpace_backup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rumman
 */
public class SummaryCalculation {

    private double orderInHandSummary, wfcaSummary, wfbaSummary, totalAcceptedBillSummary;
    private double oihBalanceQuantityValue, advanceDelivery, replacement, totalSummary;
    private double lcReceived, bankAccpRcv, realize, shipmentKG;
    private double eximDup, scbDup, hsbcDup, eximWfba, scbWfba, hsbcWfba;
    private double eximTotalAccpBill, eximPurchasedBill, eximOverdueBill, eximDrpNetAmount;
    private double scbTotalAccpBill, scbPurchasedBill, scbOverdueBill, scbDrpNetAmount;
    private double hsbcTotalAccpBill, hsbcPurchasedBill, hsbcOverdueBill, hsbcDrpNetAmount;
    private double totalAccpBill, totalPurchaseBill, overdueBillNOTPurchased, overdueBillPurchased, drpNetAmount, totalOverDueBill;

    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public SummaryCalculation() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/outspace?user=root");

        } catch (ClassNotFoundException ex) {
            System.out.println("MYSQL class not found\n" + ex);
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            System.out.println("SQL Exception\n" + ex);
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public double getOrderInHandSummary() {
        orderInHandSummary = 0.0;
        String query = "SELECT * FROM orderinhand ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                orderInHandSummary += quantity * rate;

                System.out.println("Quantity: " + quantity);
                System.out.println("Rate: " + rate);
                System.out.println("OIH: value to be added: " + orderInHandSummary);
//            OrderInHand o = new OrderInHand(name, lc_id, issue_date,
//                    shipment_date, expiry_date, sight_days, pi_id, count,
//                    quantity, rate,amount,totalAmount,todayDelivery,balanceQuantity);
//            orderInHand.add(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }

        return orderInHandSummary;
    }

    public double getWfcaSummary() {
        wfcaSummary = 0.0;
        String query = "SELECT * FROM wfca ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                wfcaSummary += quantity * rate;

                System.out.println("Quantity: " + quantity);
                System.out.println("Rate: " + rate);
                System.out.println("WFCA: value to be added:: " + wfcaSummary);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return wfcaSummary;
    }

    public double getWfbaSummary() {
        wfbaSummary = 0.0;
        String query = "SELECT * FROM wfba ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                wfbaSummary += quantity * rate;

                System.out.println("Quantity: " + quantity);
                System.out.println("Rate: " + rate);
                System.out.println("WFCA: value to be added:: " + wfbaSummary);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return wfbaSummary;
    }

    public double getTotalAcceptedBillSummary() {
        totalAcceptedBillSummary = 0.0;
        String query = "SELECT * FROM wfca ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                totalAcceptedBillSummary += quantity * rate;

                System.out.println("Quantity: " + quantity);
                System.out.println("Rate: " + rate);
                System.out.println("WFCA: value to be added:: " + totalAcceptedBillSummary);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return totalAcceptedBillSummary;
    }

    public double getAdvanceDelivery() {
        advanceDelivery = 0.0;
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sCurrentDate = dateFormat.format(currentDate);

        String query = "SELECT * FROM deliverystatus WHERE ((lc_id IS NULL OR lc_id='') OR (pi_id IS NULL OR pi_id='')) AND date = " + sCurrentDate;
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("quantity"));
                advanceDelivery += quantity;

                System.out.println("getAdvanceDelivery():: Quantity: " + quantity);
                System.out.println("getAdvanceDelivery():: Advance Delivery:: " + advanceDelivery);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return advanceDelivery;
    }

    public double getReplacement() {
        replacement = 0.0;
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sCurrentDate = dateFormat.format(currentDate);

        String query = "SELECT * FROM deliverystatus WHERE ( quantity < 0 ) AND date = " + sCurrentDate;
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("quantity"));

                double rate = Double.parseDouble(resultSet
                        .getString("rate"));
                replacement += ((quantity * rate) * (-1));

                System.out.println("getReplacement():: Quantity: " + quantity);
                System.out.println("getReplacement():: Replacement: value to be added:: " + replacement);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }

        return replacement;
    }
    public double getBankAccpRcv() {
        bankAccpRcv = 0.0;
        String query = "SELECT * FROM drp "
                + " WHERE YEAR(table_entry_date) = YEAR(CURDATE())"
                + " AND MONTH(table_entry_date) = MONTH(CURDATE())";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));

                double rate = Double.parseDouble(resultSet
                        .getString("Rate"));
                bankAccpRcv += quantity * rate;

                System.out.println("getBankAccpRcv():: Quantity: " + quantity);
                System.out.println("getBankAccpRcv():: Replacement: value to be added:: " + bankAccpRcv);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bankAccpRcv;
    }

    public double getRealize() {
        realize = 0.0;
        String query = "SELECT * FROM realization "
                + " WHERE YEAR(Realization_Date) = YEAR(CURDATE())"
                + " AND MONTH(Realization_Date) = MONTH(CURDATE())";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));

                double rate = Double.parseDouble(resultSet
                        .getString("Rate"));
                realize += quantity * rate;

                System.out.println("getRealize():: Quantity: " + quantity);
                System.out.println("getRealize():: Replacement: value to be added:: " + realize);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return realize;
    }

    /*shows shipment in kg from delivery status based on date of delivery
     for the current month
     */
    public double getShipmentKG() {
        shipmentKG = 0.0;
        String query = "SELECT * FROM deliverystatus "
                + " WHERE YEAR(date) = YEAR(CURDATE())"
                + " AND MONTH(date) = MONTH(CURDATE())";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
//                double rate = Double.parseDouble(resultSet
//                        .getString("Rate"));
                shipmentKG += quantity;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + shipmentKG);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return shipmentKG;
    }
    /*
     All the DUP's are they for WFBA Table to be asked??? 
    
     */

    public double getEximDup() {
        eximDup = 0.0;
//        String query = "SELECT * FROM wfba "
//                + " WHERE YEAR(table_entry_date) = YEAR(CURDATE())"
//                + " AND MONTH(table_entry_date) = MONTH(CURDATE()) AND beneficiary_bank_id = 3 ";
        String query = "SELECT * FROM wfca "
                + " WHERE beneficiary_bank_id = 3 ";

        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet
                        .getString("Rate"));
                eximDup += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + eximDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eximDup;
    }

    public double getScbDup() {
        scbDup = 0.0;
//        String query = "SELECT * FROM wfba "
//                + " WHERE YEAR(table_entry_date) = YEAR(CURDATE())"
//                + " AND MONTH(table_entry_date) = MONTH(CURDATE()) AND beneficiary_bank_id = 2 ";
        String query = "SELECT * FROM wfca "
                + " WHERE beneficiary_bank_id = 2 ";

        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet
                        .getString("Rate"));
                eximDup += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + scbDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scbDup;
    }

    public double getHsbcDup() {
        hsbcDup = 0.0;
//        String query = "SELECT * FROM wfba "
//                + " WHERE YEAR(table_entry_date) = YEAR(CURDATE())"
//                + " AND MONTH(table_entry_date) = MONTH(CURDATE()) AND beneficiary_bank_id = 49 ";
        String query = "SELECT * FROM wfca "
                + " WHERE beneficiary_bank_id = 49 ";

        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                hsbcDup += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hsbcDup;
    }

    public double getEximWfba() {
        eximWfba = 0.0;
        String query = "SELECT * FROM wfba "
                + " WHERE beneficiary_bank_id = 3 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                eximWfba += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eximWfba;
    }

    public double getHsbcWfba() {
        hsbcWfba = 0.0;
        String query = "SELECT * FROM wfba "
                + " WHERE beneficiary_bank_id = 49 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                hsbcWfba += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hsbcWfba;
    }

    public double getScbWfba() {
        scbWfba = 0.0;
        String query = "SELECT * FROM wfba "
                + " WHERE beneficiary_bank_id = 2 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                scbWfba += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scbWfba;
    }

    public double getEximTotalAccpBill() {
        eximTotalAccpBill = 0.0;
        String query = "SELECT * FROM drp "
                + " WHERE beneficiary_bank_id = 3 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                eximTotalAccpBill += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eximTotalAccpBill;
    }

    public double getScbTotalAccpBill() {
        scbTotalAccpBill = 0.0;
        String query = "SELECT * FROM drp "
                + " WHERE beneficiary_bank_id = 2 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                scbTotalAccpBill += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scbTotalAccpBill;
    }

    public double getHsbcTotalAccpBill() {
        hsbcTotalAccpBill = 0.0;
        String query = "SELECT * FROM drp "
                + " WHERE beneficiary_bank_id = 49 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                hsbcTotalAccpBill += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hsbcTotalAccpBill;
    }
//## need to add condition after change in DB
    public double getEximPurchasedBill() {
        eximPurchasedBill = 0.0;
        String query = "SELECT * FROM purchasereport "
                + " WHERE beneficiary_bank_id = 3 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                eximPurchasedBill += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eximPurchasedBill;
    }
//## need to add condition after change in DB
    public double getScbPurchasedBill() {
        scbPurchasedBill = 0.0;
        String query = "SELECT * FROM purchasereport "
                + " WHERE beneficiary_bank_id = 2 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                scbPurchasedBill += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Replacement: value to be added:: " + hsbcDup);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scbPurchasedBill;
    }
//## need to add condition after change in DB
    public double getHsbcPurchasedBill() {
        hsbcPurchasedBill = 0.0;
        String query = "SELECT * FROM purchasereport "
                + " WHERE beneficiary_bank_id = 49 ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                hsbcPurchasedBill += quantity * rate;

                System.out.println("getShipmentKG():: Quantity: " + quantity);
                System.out.println("getShipmentKG():: Rate:: " + rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hsbcPurchasedBill;
    }

    public double getEximOverdueBill() {
        eximOverdueBill = 0.0;
        String query = "SELECT * FROM drp WHERE (Fdd_Issue_date IS NULL OR Fdd_Issue_date='') AND  beneficiary_bank_id = 3 AND table_entry_date < CURRENT_DATE();";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                eximOverdueBill += quantity * rate;

                System.out.println("getEximOverdueBill():: Quantity: " + quantity);
                System.out.println("getEximOverdueBill():: Rate: " + rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eximOverdueBill;
    }
    public double getScbOverdueBill() {
        scbOverdueBill = 0.0;
        String query = "SELECT * FROM drp WHERE (Fdd_Issue_date IS NULL OR Fdd_Issue_date='') AND  beneficiary_bank_id = 2 AND table_entry_date < CURRENT_DATE()";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                scbOverdueBill += quantity * rate;

                System.out.println("getScbOverdueBill():: Quantity: " + quantity);
                System.out.println("getScbOverdueBill():: Rate:: " + rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scbOverdueBill;
    }
    public double getHsbcOverdueBill() {
        hsbcOverdueBill = 0.0;
        String query = "SELECT * FROM drp WHERE (Fdd_Issue_date IS NULL OR Fdd_Issue_date='') AND  beneficiary_bank_id = 49 AND table_entry_date < CURRENT_DATE()";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
//
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                scbOverdueBill += quantity * rate;

                System.out.println("getHsbcOverdueBill():: Quantity: " + quantity);
                System.out.println("getHsbcOverdueBill():: Rate:: " + rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hsbcOverdueBill;
    }
    public double getEximDrpNetAmount() {
        eximDrpNetAmount = getEximTotalAccpBill() - getEximPurchasedBill() - getEximOverdueBill() ;
        return eximDrpNetAmount;
    }
    public double getScbDrpNetAmount() {
        scbDrpNetAmount = getScbTotalAccpBill() - getScbPurchasedBill() - getScbOverdueBill() ;
        return scbDrpNetAmount;
    }
    
    public double getHsbcDrpNetAmount() {
        hsbcDrpNetAmount = getHsbcDrpNetAmount() - getHsbcPurchasedBill() - getHsbcOverdueBill() ;
        return hsbcDrpNetAmount;
    }

    public double getTotalAccpBill() {
        totalAccpBill = getEximTotalAccpBill() + getScbTotalAccpBill() + getHsbcTotalAccpBill();
        return totalAccpBill;
    }

    public double getTotalPurchaseBill() {
        totalPurchaseBill = getEximPurchasedBill() + getScbPurchasedBill() + getHsbcPurchasedBill() ;
        return totalPurchaseBill;
    }

    public double getOverdueBillNOTPurchased() {
        overdueBillNOTPurchased = getEximOverdueBill() + getScbOverdueBill() + getHsbcOverdueBill() ;
        return overdueBillNOTPurchased;
    }

    public double getOverdueBillPurchased() {
        overdueBillPurchased = 0;
            String query = "SELECT * FROM purchasereport WHERE (Fdd_Issue_date IS NULL OR Fdd_Issue_date='') AND table_entry_date < CURRENT_DATE()";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                overdueBillPurchased += quantity * rate;

                System.out.println("getHsbcOverdueBill():: Quantity: " + quantity);
                System.out.println("getHsbcOverdueBill():: Rate:: " + rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
  
        return overdueBillPurchased;
    }

    public double getDrpNetAmount() {
        drpNetAmount = getEximDrpNetAmount() + getScbDrpNetAmount() + getHsbcDrpNetAmount() ;
        return drpNetAmount;
    }

    public double getTotalOverDueBill() {
        totalOverDueBill = getHsbcOverdueBill() + getEximOverdueBill() + getScbOverdueBill() ;
        return totalOverDueBill;
    }
   //below advance delivery,replacement,local yarn over all the advanced table
   public double getTotalSummary() {
        totalSummary = 0.0;
        String query = "SELECT * FROM deliverystatus "
                + "WHERE date = CURDATE()";                
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                totalSummary += quantity;
                System.out.println("Quantity: " + quantity);
                System.out.println("Rate: " + rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
       return totalSummary;
    }
       public double getLcReceived() {
        lcReceived = 0.0;
        String query = "SELECT * FROM orderinhand "
                + "WHERE YEAR(lc_received_date) = YEAR(CURDATE()) "
                + "AND MONTH(lc_received_date) = MONTH(CURDATE())";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double quantity = Double.parseDouble(resultSet
                        .getString("Quantity"));
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                lcReceived += quantity * rate;
                System.out.println("Quantity: " + quantity);
                System.out.println("Rate: " + rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
           return lcReceived;
    }
    //just below table how ???
    //differnt than oihSummary but how ??? fix later...
    public double getOihBalanceQuantityValue() {
        oihBalanceQuantityValue = 0.0;
        String query = "SELECT * FROM orderinhand ";
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                double balanceQuantity = Double.parseDouble(resultSet
                        .getString("Balance_Quantity"));
                double rate = Double.parseDouble(resultSet.getString("Rate"));
                lcReceived += balanceQuantity * rate;
                System.out.println("Quantity: " + balanceQuantity);
                System.out.println("Rate: " + rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SummaryCalculation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return oihBalanceQuantityValue;
    }

}