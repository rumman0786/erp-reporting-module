
import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class FetchDataDeliveryStatus {

    private String fromDate, toDate;
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private String deliveryStatusQuery = null;
    private ArrayList<DeliveryStatus> DeliveryStatusList;

    public FetchDataDeliveryStatus(String fromDate, String toDate, String orderCondition) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        String orderby = " ";
        if (!orderCondition.equals(null)) {
            orderby = " ORDER BY " + orderCondition;
        }
        deliveryStatusQuery = "SELECT * FROM deliverystatus "
                //+ " WHERE lc.issue_date BETWEEN CAST( '" + fromDate + "' AS DATE) AND CAST( '" + toDate + "'  AS DATE) "
                + orderby;
        System.out.println(deliveryStatusQuery);
    }

    public void readDataBase() throws Exception {
        try {
            System.out.println("In deliverystatus.readDataBase() start");
            // this will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // setup the connection with the DB.
            System.out.println("In deliverystatus.readDataBase() 1");
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/outspace?user=root");
            // System.out.println("Conneted");
            // statements allow to issue SQL queries to the database
            System.out.println(deliveryStatusQuery);
            statement = connect.createStatement();
            // resultSet gets the result of the SQL query
            resultSet = statement.executeQuery(deliveryStatusQuery);
            System.out.println("In deliverystatus.readDataBase() 3");
            // piRegisterList = new ArrayList<PIRegister>();
            DeliveryStatusList = new ArrayList<DeliveryStatus>();
            writeResultSet(resultSet);
            System.out.println("In deliverystatus.readDataBase() End");
            // preparedStatements can use variables and are more efficient

        } catch (Exception e) {
            System.out.print(e);
            //throw e;
        } finally {
            // close();
        }

    }

    private void writeResultSet(ResultSet resultSet) throws SQLException {
        // resultSet is initialised before the first data set
        while (resultSet.next()) {
            String buyer = resultSet.getString("buyer");
            String date = resultSet.getString("date");
            String chalan = resultSet.getString("chalan");
            String lc_id = resultSet.getString("lc_id");
            String pi_id = resultSet.getString("pi_id");
            String count = resultSet.getString("count");
            String lot_id = resultSet.getString("lot_id");
            double quantity = Double.parseDouble(resultSet
                    .getString("quantity"));
            double rate = Double.parseDouble(resultSet.getString("rate"));
            double amount = Double.parseDouble(resultSet.getString("amount"));
            String remarks = resultSet.getString("remarks");
            DeliveryStatus deliveryStatus = new DeliveryStatus(buyer, date, chalan,
                    lc_id, pi_id, count, lot_id, quantity, rate, amount, remarks);
            DeliveryStatusList.add(deliveryStatus);
            System.out.println(lc_id);
        }
    }
    
    public ArrayList<DeliveryStatus> getdDeliveryStatuses() {
        return DeliveryStatusList;
    }

    private void close(Closeable c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (Exception e) {

        }
    }

}
