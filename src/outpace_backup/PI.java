
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rumman
 */
public class PI {

    private String buyer, pi_id, pi_date, expiry_date, sight_days;
    ArrayList<CountDetails> countList = new ArrayList<CountDetails>();
    double totalAmount = 0.0 ;
    int count_size = 0;
//    public PI(String pi_id, ArrayList<CountDetails> count) {
//        this.pi_id = pi_id;
//        this.count = count;
//        count_size++ ; 
//    }

    PI(String buyer, String pi_id, String pi_date, String offer_validity_date,
            String payment_period){//, String description, double quantity, double rate) {
        this.buyer = buyer ;
        this.pi_id = pi_id ;
        this.pi_date = pi_date ;
        this.expiry_date = offer_validity_date;
        this.sight_days = payment_period ; 
    }

    public boolean addCountDetails(CountDetails count) {
        if (countList.contains(count)) {
            return false;
        } else {
            countList.add(count);
            count_size++;
        }
        totalAmount = getTotalAmount();
        return true;
    }

    public double getTotalAmount() {
        double totalAmount = 0.0;
        if (!countList.isEmpty()) {
            for (CountDetails c : countList) {
                totalAmount += c.getAmount();
            }
        }
        return totalAmount;
    }

    public String getBuyer() {
        return buyer;
    }

    public String getPi_id() {
        return pi_id;
    }

    public String getPi_date() {
        return pi_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public String getSight_days() {
        return sight_days;
    }

    public ArrayList<CountDetails> getCountList() {
        return countList;
    }

    public int getCount_size() {
        return count_size;
    }
    

}
