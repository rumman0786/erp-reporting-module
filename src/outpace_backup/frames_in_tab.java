/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author licy
 */

import com.itextpdf.text.DocumentException;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.stage.WindowEvent;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import java.util.Locale;
//import static jdk.nashorn.internal.codegen.Compiler.LOG;
import net.proteanit.sql.DbUtils;
import java.awt.event.*;
import java.awt.*;
import javax.swing.JFrame;

public class frames_in_tab extends javax.swing.JFrame {
    String lc_id = null;
    String count = null;
    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    String orderCondition = " Buyer ASC " ;
    String fromDate, toDate;
    String lc_id_OIH = null;
    String count_OIH = null;
    String lc_id_WFCA = null;
    String count_WFCA = null;
    String sfrom_LC , sto_LC,sqlQuery=null;
    String sfrom_PI , sto_PI ;
    String ordercondition = " name ";
    String buyer ,lcId,lcDate,lastDateOfShipment,expiryDate,lcSight,piNo,countDetails,quantity,rate,amount,totalAmount;
    String buyer_wfca ,lcId_wfca,lcDate_wfca,lastDateOfShipment_wfca,expiryDate_wfca,lcSight_wfca,piNo_wfca,countDetails_wfca,quantity_wfca,rate_wfca,amount_wfca,totalAmount_wfca,del_Com_Date_wfca,Submission_Date_wfca,comments_wfca,docHandDate_wfca;
    String buyer_wfba ,lcId_wfba,lcDate_wfba,lastDateOfShipment_wfba,expiryDate_wfba,lcSight_wfba,piNo_wfba,LDPB_No_wfba,count_wfba,quantity_wfba,rate_wfba,amount_wfba,totalAmount_wfba,sDocRecDate_wfba,sBankSUBDate_wfba,comments_wfba,sMaturityDate_wfba;
    String buyer_drp ,lcId_drp,lcDate_drp,lastDateOfShipment_drp,expiryDate_drp,sDocRecDate_drp,sBankSUBDate_drp,DateChooser_DocRecDate_drp,DateChooser_BankSUBDate_drp,DateChooser_Adjust_drp,DateChooser_Issue_drp,DateChooser_MaturyDate_drp,DateChooser_PurchaseDate_drp,DateChooser_Realization_drp,DateChooser_FddAdd_drp,sIssueDate_drp,sMaturityDate_drp,sPurchaseDate_drp,sFddDate_drp,amount_drp,sAdjustDate_drp,sRealizationDate_drp,LDPB_No_drp,count_drp,quantity_drp,rate_drp,Value_drp,total_drp,exim_drp,scb_drp;
    String buyer_DRP ,lcId_DRP,lcDate_DRP,lastDateOfShipment_DRP,expiryDate_DRP,sDocRecDate_DRP,sBankSUBDate_DRP,sIssueDate_DRP,sMaturityDate_DRP,sPurchaseDate_DRP,sFddDate_DRP,amount_DRP,sAdjustDate_DRP,sRealizationDate_DRP,LDPB_No_DRP,count_DRP,quantity_DRP,rate_DRP,Value_DRP,total_DRP,exim_DRP,scb_DRP;
    String buyer_RE ,lcId_RE,lcDate_RE,lastDateOfShipment_RE,expiryDate_RE,sDocRecDate_RE,sBankSUBDate_RE,sIssueDate_RE,sMaturityDate_RE,sPurchaseDate_RE,sFddDate_RE,amount_RE,sAdjustDate_RE,sRealizationDate_RE,LDPB_No_RE,count_RE,quantity_RE,rate_RE,Value_RE,total_RE,exim_RE,scb_RE;
    String buyer_PU ,lcId_PU,lcDate_PU,lastDateOfShipment_PU,expiryDate_PU,sDocRecDate_PU,sBankSUBDate_PU,sIssueDate_PU,sMaturityDate_PU,sPurchaseDate_PU,sFddDate_PU,amount_PU,sAdjustDate_PU,sRealizationDate_PU,LDPB_No_PU,count_PU,quantity_PU,rate_PU,Value_PU,total_PU,exim_PU,scb_PU;
    

    /**
     * Creates new form frames_in_tab
     */
    public frames_in_tab() {
        initComponents();
        conn = loginframe.MYSQLSetup();
        updateTable();
        updateTable1();
        updateTableOrderInHand();
        updateTableWFCA();
        updateTableWFCA2();
        
        updateTableWFBA();
        updateTableWFBA2();
        updateTableDRP();
        updateTableDRP1();
        updateTableRealization();
        updateTableDRP2();
        updateTablePurchase();
        sqlQuery = "SELECT b.name, lc.lc_id,lc.issue_date,lc.shipment_date, lc.expiry_date, "
                + " lc.sight_days, lpr.pi_id, g.description, g.quantity, g.unit_price "
                + " FROM letter_of_credit AS lc "
                + " JOIN buyer AS b ON b.buyer_id = lc.buyer_id "
                + " JOIN lc_pi_relation AS lpr ON lpr.lc_id=lc.lc_id "
                + " JOIN goods AS g ON g.pi_id=lpr.pi_id ";
        updateLCTable();
         sqlQuery = "SELECT b.name,p.pi_id,p.pi_date,p.offer_validity,p.payment_period, "
                + " g.description,g.quantity,g.unit_price FROM proforma_invoice AS p "
                + " JOIN buyer AS b ON b.buyer_id = p.buyer_id "
                + " JOIN goods AS g ON g.pi_id = p.pi_id " ;
        updatePITable();
        
        
        
        
    }
    
    
    public void updateTable() {
        
        try {
            String sqlQuery = "SELECT b.name, lc.lc_id,lc.issue_date,lc.shipment_date, lc.expiry_date, lc.sight_days, lpr.pi_id, g.description, g.quantity, g.unit_price"
                    + " FROM letter_of_credit AS lc"
                    + " INNER JOIN buyer AS b ON b.buyer_id = lc.buyer_id"
                    + " INNER JOIN lc_pi_relation AS lpr ON lpr.lc_id=lc.lc_id"
                    + " INNER JOIN goods AS g ON g.pi_id=lpr.pi_id"
                    //+ " WHERE lc.lc_id='000113040333'";
                    //+ " WHERE lc.issue_date BETWEEN CAST( '"+fromDate +"' AS DATE) AND CAST( '"+toDate+"'  AS DATE)"
                    //+ "WHERE `track_time` >= fromDate\n" 
                    //+ "    AND   `track_time` <= toDate"
                    + " ORDER BY b.name ASC ";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            primary1st.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    
    public void updateTable1() {
        try {
            String sqlQuery = "SELECT * FROM orderinhand";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            OrderInHandTable.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    
    private void updateTableWFCA() {
        try {
            String sqlQuery = "SELECT * FROM wfca";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_WFCA.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    private void updateTableWFCA2() {
        try {
            String sqlQuery = "SELECT * FROM wfca";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_WFCA1.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    private void updateTableOrderInHand() {
        try {
            String sqlQuery = "SELECT * FROM orderinhand";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_OrderInHand.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    
    private void updateTableWFBA() {
        try {
            String sqlQuery = "SELECT * FROM wfba";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_WFBA.setModel(DbUtils.resultSetToTableModel(rs));
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    
    private void updateTableWFBA2() {
        try {
            String sqlQuery = "SELECT * FROM wfba";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_WFBA1.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    
    
    private void updateTableDRP() {
        try {
            String sqlQuery = "SELECT * FROM drp";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_DRP.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    private void updateTableDRP1() {
        try {
            String sqlQuery = "SELECT * FROM drp";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_DRP1.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    
    
    
    private void updateTableRealization(){
        System.out.println("Table re");
        try {
            String sqlQuery = "SELECT * FROM realization";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_Realization.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    
     private void updateTableDRP2() {
        try {
            String sqlQuery = "SELECT * FROM drp";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_DRP2.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
     
    private void updateTablePurchase(){
        System.out.println("Table re");
        try {
            String sqlQuery = "SELECT * FROM purchasereport";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_Purchase.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    
    
    private void updateLCTable(){
    try {
            
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            Table_LC.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    
    
    
    private void updatePITable(){
    try {
            
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            Table_PI.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }
    
    
    
    public void updateSortedTable(String sortCondition) {
        String orderBy = " ORDER BY " + sortCondition ; 
        try {
            String sqlQuery = "SELECT * FROM orderinhand " + orderBy;
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            //primary1st.setModel(DbUtils.resultSetToTableModel(rs));
            OrderInHandTable.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane21 = new javax.swing.JScrollPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        panel_OrderInHand = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        primary1st = new javax.swing.JTable();
        txt_quantity = new javax.swing.JTextField();
        txt_unit = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txt_amount = new javax.swing.JTextField();
        txt_todayDelivery = new javax.swing.JTextField();
        txt_totalAmount = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt_name = new javax.swing.JTextField();
        txt_lc = new javax.swing.JTextField();
        txt_issue = new javax.swing.JTextField();
        txt_shipment = new javax.swing.JTextField();
        txt_expiry = new javax.swing.JTextField();
        txt_sight = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_description = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txt_balanceQuantity = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_pi = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        OrderInHandTable = new javax.swing.JTable();
        txt_amount1 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_todayDelivery1 = new javax.swing.JTextField();
        txt_totalAmount1 = new javax.swing.JTextField();
        txt_comments = new javax.swing.JTextField();
        txt_balanceQuantity1 = new javax.swing.JTextField();
        bEditInfo = new javax.swing.JButton();
        bCreateOrderInHandReport = new javax.swing.JButton();
        bAddToWFCAReport = new javax.swing.JButton();
        bDeleteFromOrderInHandReport = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        rDate = new javax.swing.JRadioButton();
        rBuyer = new javax.swing.JRadioButton();
        rPi = new javax.swing.JRadioButton();
        rCount = new javax.swing.JRadioButton();
        rRate = new javax.swing.JRadioButton();
        rQuality = new javax.swing.JRadioButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jLabel106 = new javax.swing.JLabel();
        jLabel107 = new javax.swing.JLabel();
        jDateChooser5 = new com.toedter.calendar.JDateChooser();
        jButton12 = new javax.swing.JButton();
        jLabel108 = new javax.swing.JLabel();
        txt_description1 = new javax.swing.JTextField();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jDateChooser4 = new com.toedter.calendar.JDateChooser();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        table_OrderInHand = new javax.swing.JTable();
        bCreateWFCAReport = new javax.swing.JButton();
        bDeleteFromWFCAReport = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        DateChooser_DelComDate_WFCA = new com.toedter.calendar.JDateChooser();
        DateChooser_SubmissionDate_WFCA = new com.toedter.calendar.JDateChooser();
        txt_value_WFCA = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        DateChooser_DocumentHandOverDate_WFCA = new com.toedter.calendar.JDateChooser();
        txt_totalvalue_WFCA = new javax.swing.JTextField();
        txt_comments_WFCA = new javax.swing.JTextField();
        bEditInfo_WFCA = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        table_WFCA = new javax.swing.JTable();
        bCreate_OIH_report = new javax.swing.JButton();
        bAddToWFCAReport1 = new javax.swing.JButton();
        bDeleteFrom_OIH_Report = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        dateChooser_DelComDate_OIH = new com.toedter.calendar.JDateChooser();
        dateChooser_SubmissionDate_OIH = new com.toedter.calendar.JDateChooser();
        txt_amount_OIH = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txt_todayDelivery_OIH = new javax.swing.JTextField();
        dateChooser_DocumentHandOver_OIH = new com.toedter.calendar.JDateChooser();
        txt_totalAmount_OIH = new javax.swing.JTextField();
        txt_comments_OIH = new javax.swing.JTextField();
        txt_balanceQuantity_OIH = new javax.swing.JTextField();
        bEditInfo_OIH = new javax.swing.JButton();
        rDate1 = new javax.swing.JRadioButton();
        rBuyer1 = new javax.swing.JRadioButton();
        rPi1 = new javax.swing.JRadioButton();
        rCount1 = new javax.swing.JRadioButton();
        rRate1 = new javax.swing.JRadioButton();
        rQuality1 = new javax.swing.JRadioButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel109 = new javax.swing.JLabel();
        jLabel110 = new javax.swing.JLabel();
        jDateChooser6 = new com.toedter.calendar.JDateChooser();
        jButton15 = new javax.swing.JButton();
        jLabel111 = new javax.swing.JLabel();
        txt_description2 = new javax.swing.JTextField();
        jButton16 = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        jDateChooser7 = new com.toedter.calendar.JDateChooser();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        table_WFCA1 = new javax.swing.JTable();
        DateChooser_DelComDate_WFCA1 = new com.toedter.calendar.JDateChooser();
        DateChooser_SubmissionDate_WFCA1 = new com.toedter.calendar.JDateChooser();
        txt_value_WFCA1 = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        DateChooser_Maturitydate_WFCA = new com.toedter.calendar.JDateChooser();
        txt_totalvalue_WFCA1 = new javax.swing.JTextField();
        txt_comments_WFCA1 = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        txt_LDBP_WFCA = new javax.swing.JTextField();
        jScrollPane8 = new javax.swing.JScrollPane();
        table_WFBA = new javax.swing.JTable();
        bCreateWFCAReport1 = new javax.swing.JButton();
        bDeleteFromWFCAReport1 = new javax.swing.JButton();
        bEditInfo1_WFCA = new javax.swing.JButton();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        txt_LDPD_WFBA = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        txt_comments_WFBA = new javax.swing.JTextField();
        txt_totalvalue_WFBA = new javax.swing.JTextField();
        DateChooser_MaturyDate_WFBA = new com.toedter.calendar.JDateChooser();
        DateChooser_BankSUBDate_WFCA = new com.toedter.calendar.JDateChooser();
        DateChooser_DocRecDate_WFBA = new com.toedter.calendar.JDateChooser();
        txt_value_WFBA = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        bEditInfo_WFBA = new javax.swing.JButton();
        bCreateWFBAReport_WFBA = new javax.swing.JButton();
        bDeleteFromWFBAReport_WFBA = new javax.swing.JButton();
        bAddToWFBAReport1 = new javax.swing.JButton();
        rDate2 = new javax.swing.JRadioButton();
        rBuyer2 = new javax.swing.JRadioButton();
        rPi2 = new javax.swing.JRadioButton();
        rCount2 = new javax.swing.JRadioButton();
        rRate2 = new javax.swing.JRadioButton();
        rQuality2 = new javax.swing.JRadioButton();
        jLabel51 = new javax.swing.JLabel();
        DateChooser_DocRecDate_WFCA = new com.toedter.calendar.JDateChooser();
        jLabel52 = new javax.swing.JLabel();
        DateChooser_BankSUBDate_WFBA = new com.toedter.calendar.JDateChooser();
        DateChooser_DocumentHandOverDate_WFCA1 = new com.toedter.calendar.JDateChooser();
        jLabel53 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jLabel54 = new javax.swing.JLabel();
        DateChooser_Issuedate_WFCA = new com.toedter.calendar.JDateChooser();
        jLabel55 = new javax.swing.JLabel();
        DateChooser_Issuedate_WFCA1 = new com.toedter.calendar.JDateChooser();
        jLabel112 = new javax.swing.JLabel();
        jDateChooser8 = new com.toedter.calendar.JDateChooser();
        jLabel113 = new javax.swing.JLabel();
        jDateChooser9 = new com.toedter.calendar.JDateChooser();
        jButton18 = new javax.swing.JButton();
        jLabel114 = new javax.swing.JLabel();
        txt_description3 = new javax.swing.JTextField();
        jButton19 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        table_WFBA1 = new javax.swing.JTable();
        txt_totalvalue_WFBA1 = new javax.swing.JTextField();
        DateChooser_MaturyDate_WFBA1 = new com.toedter.calendar.JDateChooser();
        DateChooser_BankSUBDate_WFBA1 = new com.toedter.calendar.JDateChooser();
        DateChooser_DocRecDate_WFBA1 = new com.toedter.calendar.JDateChooser();
        txt_value_WFBA1 = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        txt_LDPB_WFBA = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        txt_comments_WFBA1 = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        DateChooser_PurchaseDate_WFBA = new com.toedter.calendar.JDateChooser();
        jLabel64 = new javax.swing.JLabel();
        DateChooser_Issue_WFBA = new com.toedter.calendar.JDateChooser();
        jScrollPane11 = new javax.swing.JScrollPane();
        table_DRP = new javax.swing.JTable();
        bDeleteFromWFCAReport2 = new javax.swing.JButton();
        bCreateDRPReport2 = new javax.swing.JButton();
        bEditInfo_WFBA1 = new javax.swing.JButton();
        bCreateWFBAReport_WFBA1 = new javax.swing.JButton();
        bAddToDRPReport = new javax.swing.JButton();
        rDate3 = new javax.swing.JRadioButton();
        rBuyer3 = new javax.swing.JRadioButton();
        rPi3 = new javax.swing.JRadioButton();
        rCount3 = new javax.swing.JRadioButton();
        rRate3 = new javax.swing.JRadioButton();
        rQuality3 = new javax.swing.JRadioButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jLabel65 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jButton9 = new javax.swing.JButton();
        jLabel115 = new javax.swing.JLabel();
        jDateChooser10 = new com.toedter.calendar.JDateChooser();
        jLabel116 = new javax.swing.JLabel();
        jDateChooser11 = new com.toedter.calendar.JDateChooser();
        jButton21 = new javax.swing.JButton();
        jLabel117 = new javax.swing.JLabel();
        txt_description4 = new javax.swing.JTextField();
        jButton22 = new javax.swing.JButton();
        jButton23 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane15 = new javax.swing.JScrollPane();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane13 = new javax.swing.JScrollPane();
        table_DRP1 = new javax.swing.JTable();
        txt_totalvalue_DRP = new javax.swing.JTextField();
        DateChooser_MaturyDate_DRP = new com.toedter.calendar.JDateChooser();
        DateChooser_BankSUBDate_DRP = new com.toedter.calendar.JDateChooser();
        DateChooser_DocRecDate_DRP = new com.toedter.calendar.JDateChooser();
        txt_amount_DRP = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        txt_LDPB_DRP = new javax.swing.JTextField();
        jLabel69 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        txt_fddammount_DRP = new javax.swing.JTextField();
        jLabel73 = new javax.swing.JLabel();
        DateChooser_FddAdd_DRP = new com.toedter.calendar.JDateChooser();
        jLabel74 = new javax.swing.JLabel();
        DateChooser_PurchaseDate_DRP = new com.toedter.calendar.JDateChooser();
        jLabel75 = new javax.swing.JLabel();
        DateChooser_Issue_DRP = new com.toedter.calendar.JDateChooser();
        jLabel76 = new javax.swing.JLabel();
        DateChooser_Adjust_DRP = new com.toedter.calendar.JDateChooser();
        jLabel77 = new javax.swing.JLabel();
        DateChooser_Realization_DRP = new com.toedter.calendar.JDateChooser();
        jScrollPane14 = new javax.swing.JScrollPane();
        table_Realization = new javax.swing.JTable();
        jLabel78 = new javax.swing.JLabel();
        jLabel79 = new javax.swing.JLabel();
        DateChooser_FddAdd_RE = new com.toedter.calendar.JDateChooser();
        jLabel80 = new javax.swing.JLabel();
        DateChooser_PurchaseDate_RE = new com.toedter.calendar.JDateChooser();
        jLabel81 = new javax.swing.JLabel();
        DateChooser_Adjust_RE = new com.toedter.calendar.JDateChooser();
        jLabel82 = new javax.swing.JLabel();
        DateChooser_Issue_RE = new com.toedter.calendar.JDateChooser();
        bCreateDRPReport1 = new javax.swing.JButton();
        bAddToRealization = new javax.swing.JButton();
        bDeleteFromDRPReport_DRP = new javax.swing.JButton();
        bEditInfo_DRP = new javax.swing.JButton();
        bEditInfo_RE = new javax.swing.JButton();
        bCreateREeport = new javax.swing.JButton();
        bDeleteFromrealization = new javax.swing.JButton();
        jLabel83 = new javax.swing.JLabel();
        DateChooser_Realization_RE = new com.toedter.calendar.JDateChooser();
        rQuality4 = new javax.swing.JRadioButton();
        rRate4 = new javax.swing.JRadioButton();
        rCount4 = new javax.swing.JRadioButton();
        rPi4 = new javax.swing.JRadioButton();
        rBuyer4 = new javax.swing.JRadioButton();
        rDate4 = new javax.swing.JRadioButton();
        jButton10 = new javax.swing.JButton();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel118 = new javax.swing.JLabel();
        jDateChooser12 = new com.toedter.calendar.JDateChooser();
        jLabel119 = new javax.swing.JLabel();
        jDateChooser13 = new com.toedter.calendar.JDateChooser();
        jButton24 = new javax.swing.JButton();
        jLabel120 = new javax.swing.JLabel();
        txt_description5 = new javax.swing.JTextField();
        jButton25 = new javax.swing.JButton();
        jButton26 = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane18 = new javax.swing.JScrollPane();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane16 = new javax.swing.JScrollPane();
        table_DRP2 = new javax.swing.JTable();
        jLabel84 = new javax.swing.JLabel();
        txt_amount_DRP1 = new javax.swing.JTextField();
        jLabel85 = new javax.swing.JLabel();
        txt_totalvalue_DRP1 = new javax.swing.JTextField();
        jLabel86 = new javax.swing.JLabel();
        DateChooser_DocRecDate_DRP1 = new com.toedter.calendar.JDateChooser();
        jLabel87 = new javax.swing.JLabel();
        DateChooser_BankSUBDate_DRP1 = new com.toedter.calendar.JDateChooser();
        jLabel88 = new javax.swing.JLabel();
        txt_LDPB_DRP1 = new javax.swing.JTextField();
        jLabel89 = new javax.swing.JLabel();
        DateChooser_Issue_DRP1 = new com.toedter.calendar.JDateChooser();
        jLabel90 = new javax.swing.JLabel();
        DateChooser_MaturyDate_DRP1 = new com.toedter.calendar.JDateChooser();
        jLabel91 = new javax.swing.JLabel();
        DateChooser_PurchaseDate_DRP1 = new com.toedter.calendar.JDateChooser();
        jLabel92 = new javax.swing.JLabel();
        DateChooser_FddAdd_DRP1 = new com.toedter.calendar.JDateChooser();
        jLabel93 = new javax.swing.JLabel();
        txt_fddammount_DRP1 = new javax.swing.JTextField();
        jLabel94 = new javax.swing.JLabel();
        DateChooser_Realization_DRP1 = new com.toedter.calendar.JDateChooser();
        jLabel95 = new javax.swing.JLabel();
        DateChooser_Adjust_DRP1 = new com.toedter.calendar.JDateChooser();
        jLabel96 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox();
        bEditInfo_DRP1 = new javax.swing.JButton();
        bCreateDRPReport3 = new javax.swing.JButton();
        bAddToPurchase = new javax.swing.JButton();
        bDeleteFromDRPReport_DRP1 = new javax.swing.JButton();
        jScrollPane17 = new javax.swing.JScrollPane();
        table_Purchase = new javax.swing.JTable();
        jLabel97 = new javax.swing.JLabel();
        DateChooser_Issue_PU = new com.toedter.calendar.JDateChooser();
        jLabel98 = new javax.swing.JLabel();
        DateChooser_PurchaseDate_PU = new com.toedter.calendar.JDateChooser();
        jLabel99 = new javax.swing.JLabel();
        DateChooser_FddAdd_PU = new com.toedter.calendar.JDateChooser();
        jLabel100 = new javax.swing.JLabel();
        DateChooser_Realization_PU = new com.toedter.calendar.JDateChooser();
        jLabel101 = new javax.swing.JLabel();
        DateChooser_Adjust_PU = new com.toedter.calendar.JDateChooser();
        bEditInfo_RE1 = new javax.swing.JButton();
        bDeleteFromrealization1 = new javax.swing.JButton();
        bCreateREeport1 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        rDate5 = new javax.swing.JRadioButton();
        rBuyer5 = new javax.swing.JRadioButton();
        rPi5 = new javax.swing.JRadioButton();
        rCount5 = new javax.swing.JRadioButton();
        rRate5 = new javax.swing.JRadioButton();
        rQuality5 = new javax.swing.JRadioButton();
        jLabel121 = new javax.swing.JLabel();
        jDateChooser14 = new com.toedter.calendar.JDateChooser();
        jLabel122 = new javax.swing.JLabel();
        jDateChooser15 = new com.toedter.calendar.JDateChooser();
        jButton27 = new javax.swing.JButton();
        jLabel123 = new javax.swing.JLabel();
        txt_description6 = new javax.swing.JTextField();
        jButton28 = new javax.swing.JButton();
        jButton29 = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane19 = new javax.swing.JScrollPane();
        Table_LC = new javax.swing.JTable();
        jLabel102 = new javax.swing.JLabel();
        DateChooser_from = new com.toedter.calendar.JDateChooser();
        jLabel103 = new javax.swing.JLabel();
        DateChooser_to = new com.toedter.calendar.JDateChooser();
        LC_preview = new javax.swing.JButton();
        bcreateLC_Report = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane20 = new javax.swing.JScrollPane();
        Table_PI = new javax.swing.JTable();
        jLabel104 = new javax.swing.JLabel();
        jLabel105 = new javax.swing.JLabel();
        DateChooser_from1 = new com.toedter.calendar.JDateChooser();
        DateChooser_to1 = new com.toedter.calendar.JDateChooser();
        PI_preview = new javax.swing.JButton();
        bcreatePI_Report = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPane21.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane21.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane21.setAutoscrolls(true);

        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        primary1st.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        primary1st.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                primary1stMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(primary1st);

        jLabel11.setText("Amount");

        jLabel14.setText("Total amout");

        jLabel15.setText("Today delivery");

        txt_amount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_amountActionPerformed(evt);
            }
        });

        txt_todayDelivery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_todayDeliveryActionPerformed(evt);
            }
        });

        txt_totalAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalAmountActionPerformed(evt);
            }
        });

        jButton1.setText("Add To Order In Hand");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel8.setText("description");

        jLabel9.setText("quantity");

        jLabel10.setText("unit_price");

        txt_issue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_issueActionPerformed(evt);
            }
        });

        txt_shipment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_shipmentActionPerformed(evt);
            }
        });

        txt_expiry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_expiryActionPerformed(evt);
            }
        });

        jLabel1.setText("name");

        jLabel2.setText("lc_id");

        jLabel3.setText("issue_date");

        jLabel4.setText("shipment_date");

        jLabel5.setText("expiry_date");

        jLabel6.setText("sight_date");

        jLabel16.setText("Balance quantity");

        jLabel7.setText("pi_id");

        txt_pi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_piActionPerformed(evt);
            }
        });

        OrderInHandTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        OrderInHandTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                OrderInHandTableMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(OrderInHandTable);

        txt_amount1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_amount1ActionPerformed(evt);
            }
        });

        jLabel12.setText("Document Handover Date");

        txt_todayDelivery1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_todayDelivery1ActionPerformed(evt);
            }
        });

        txt_totalAmount1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalAmount1ActionPerformed(evt);
            }
        });

        txt_comments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_commentsActionPerformed(evt);
            }
        });

        bEditInfo.setText("Edit Info");
        bEditInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfoActionPerformed(evt);
            }
        });

        bCreateOrderInHandReport.setText("Order In Hand Report");
        bCreateOrderInHandReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateOrderInHandReportActionPerformed(evt);
            }
        });

        bAddToWFCAReport.setText("Add to WFCA");
        bAddToWFCAReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAddToWFCAReportActionPerformed(evt);
            }
        });

        bDeleteFromOrderInHandReport.setText("Delete From Order In Hand");
        bDeleteFromOrderInHandReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromOrderInHandReportActionPerformed(evt);
            }
        });

        jLabel17.setText("Amount");

        jLabel18.setText("Total amount");

        jLabel13.setText("Del. Com. Date");

        jLabel19.setText("Today delivery");

        jLabel20.setText("Comments");

        jLabel21.setText("Balance quantity");

        jLabel22.setText("Submission Date");

        rDate.setText("Date");
        rDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rDateActionPerformed(evt);
            }
        });

        rBuyer.setText("Buyer");
        rBuyer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rBuyerActionPerformed(evt);
            }
        });

        rPi.setText("PI");
        rPi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rPiActionPerformed(evt);
            }
        });

        rCount.setText("Count");
        rCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rCountActionPerformed(evt);
            }
        });

        rRate.setText("Rate");
        rRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRateActionPerformed(evt);
            }
        });

        rQuality.setText("Quality");
        rQuality.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rQualityActionPerformed(evt);
            }
        });

        jButton2.setText("Next Report");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel106.setText("From");

        jLabel107.setText("To");

        jButton12.setText("Date");

        jLabel108.setText("Search");

        jButton13.setText("LC");

        jButton14.setText("PI");

        javax.swing.GroupLayout panel_OrderInHandLayout = new javax.swing.GroupLayout(panel_OrderInHand);
        panel_OrderInHand.setLayout(panel_OrderInHandLayout);
        panel_OrderInHandLayout.setHorizontalGroup(
            panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_OrderInHandLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(719, 719, 719))
            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 1883, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                            .addGap(63, 63, 63)
                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(46, 46, 46)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(38, 38, 38)
                                    .addComponent(txt_description, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(72, 72, 72)
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(30, 30, 30)
                                    .addComponent(txt_lc, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(46, 46, 46)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(38, 38, 38)
                                    .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txt_pi, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(48, 48, 48)
                                            .addComponent(txt_sight, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(46, 46, 46)
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel15)
                                            .addGap(37, 37, 37)
                                            .addComponent(txt_todayDelivery, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel16)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txt_balanceQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(48, 48, 48)
                                            .addComponent(txt_issue, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(46, 46, 46)
                                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(43, 43, 43)
                                            .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel4)
                                            .addGap(48, 48, 48)
                                            .addComponent(txt_shipment, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(46, 46, 46)
                                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(50, 50, 50)
                                            .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(48, 48, 48)
                                            .addComponent(txt_expiry, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(46, 46, 46)
                                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(50, 50, 50)
                                            .addComponent(txt_totalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(229, 229, 229)
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel108)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txt_description1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addComponent(jLabel106)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jDateChooser4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(32, 32, 32)
                                            .addComponent(jLabel107)
                                            .addGap(18, 18, 18)
                                            .addComponent(jDateChooser5, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(18, 18, 18)
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_OrderInHandLayout.createSequentialGroup()
                                            .addGap(4, 4, 4)
                                            .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addGap(3, 3, 3)
                                            .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                            .addGap(54, 54, 54)
                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(txt_totalAmount1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(63, 63, 63)
                                                    .addComponent(txt_amount1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGap(18, 18, 18)
                                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel19)
                                                .addComponent(jLabel21)))
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                                    .addGap(2, 2, 2)
                                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGap(18, 18, 18)
                                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(txt_comments, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGap(18, 18, 18)
                                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel12))))
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addGap(6, 6, 6)
                                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(txt_todayDelivery1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(txt_balanceQuantity1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                    .addGap(35, 35, 35)
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(bCreateOrderInHandReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(bEditInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(103, 103, 103)
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(bDeleteFromOrderInHandReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(bAddToWFCAReport, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGap(116, 116, 116)
                            .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(rDate)
                                .addComponent(rQuality)
                                .addComponent(rCount)
                                .addComponent(rRate)
                                .addComponent(rBuyer)
                                .addComponent(rPi)))
                        .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1866, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        panel_OrderInHandLayout.setVerticalGroup(
            panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel1))
                    .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(txt_description, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton1))
                .addGap(7, 7, 7)
                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel2))
                    .addComponent(txt_lc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel9))
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_issue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel10)
                                    .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(15, 15, 15)
                        .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_shipment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel11)
                                    .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(12, 12, 12)
                        .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txt_expiry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel14))
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(txt_totalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel106)
                                    .addComponent(jDateChooser4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel107)
                                        .addComponent(jDateChooser5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(33, 33, 33)
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel108)
                                    .addComponent(txt_description1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addComponent(jButton12)
                                .addGap(17, 17, 17)
                                .addComponent(jButton13)
                                .addGap(16, 16, 16)
                                .addComponent(jButton14)))))
                .addGap(11, 11, 11)
                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jLabel6))
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(txt_sight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jLabel15))
                    .addComponent(txt_todayDelivery, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(txt_pi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel16))
                    .addComponent(txt_balanceQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel17)
                                    .addComponent(txt_amount1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addComponent(jLabel18))
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel19)
                                    .addComponent(txt_todayDelivery1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                        .addGap(15, 15, 15)
                                        .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel21)
                                            .addComponent(txt_totalAmount1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                        .addGap(16, 16, 16)
                                        .addComponent(txt_balanceQuantity1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(12, 12, 12)
                        .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel13)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel22))
                                .addGap(18, 18, 18)
                                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_comments, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel20))))
                        .addGap(44, 44, 44)
                        .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bEditInfo)
                            .addComponent(bAddToWFCAReport)))
                    .addGroup(panel_OrderInHandLayout.createSequentialGroup()
                        .addComponent(rDate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rBuyer)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rPi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rRate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rQuality)))
                .addGroup(panel_OrderInHandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bCreateOrderInHandReport)
                    .addComponent(bDeleteFromOrderInHandReport))
                .addGap(4, 4, 4)
                .addComponent(jButton2)
                .addContainerGap(1054, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(panel_OrderInHand);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1972, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 982, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Order IN Hand", jPanel1);

        table_OrderInHand.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_OrderInHand.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_OrderInHandMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(table_OrderInHand);

        bCreateWFCAReport.setText("WFCA  Report");
        bCreateWFCAReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateWFCAReportActionPerformed(evt);
            }
        });

        bDeleteFromWFCAReport.setText("Delete From WFCA");
        bDeleteFromWFCAReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromWFCAReportActionPerformed(evt);
            }
        });

        jLabel23.setText("value");

        jLabel24.setText("Total value");

        jLabel25.setText("Del. Com. Date");

        jLabel26.setText("Comments");

        jLabel27.setText("Submission Date");

        txt_value_WFCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_value_WFCAActionPerformed(evt);
            }
        });

        jLabel28.setText("Document Handover Date");

        txt_totalvalue_WFCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalvalue_WFCAActionPerformed(evt);
            }
        });

        txt_comments_WFCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_comments_WFCAActionPerformed(evt);
            }
        });

        bEditInfo_WFCA.setText("Edit Info");
        bEditInfo_WFCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_WFCAActionPerformed(evt);
            }
        });

        table_WFCA.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_WFCA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_WFCAMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(table_WFCA);

        bCreate_OIH_report.setText("Order In Hand Report");
        bCreate_OIH_report.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreate_OIH_reportActionPerformed(evt);
            }
        });

        bAddToWFCAReport1.setText("Add to WFCA");
        bAddToWFCAReport1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAddToWFCAReport1ActionPerformed(evt);
            }
        });

        bDeleteFrom_OIH_Report.setText("Delete From Order In Hand");
        bDeleteFrom_OIH_Report.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFrom_OIH_ReportActionPerformed(evt);
            }
        });

        jLabel29.setText("Amount");

        jLabel30.setText("Total amount");

        jLabel31.setText("Del. Com. Date");

        jLabel32.setText("Today delivery");

        jLabel33.setText("Comments");

        jLabel34.setText("Balance quantity");

        jLabel35.setText("Submission Date");

        txt_amount_OIH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_amount_OIHActionPerformed(evt);
            }
        });

        jLabel36.setText("Document Handover Date");

        txt_todayDelivery_OIH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_todayDelivery_OIHActionPerformed(evt);
            }
        });

        txt_totalAmount_OIH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalAmount_OIHActionPerformed(evt);
            }
        });

        txt_comments_OIH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_comments_OIHActionPerformed(evt);
            }
        });

        bEditInfo_OIH.setText("Edit Info");
        bEditInfo_OIH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_OIHActionPerformed(evt);
            }
        });

        rDate1.setText("Date");
        rDate1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rDate1ActionPerformed(evt);
            }
        });

        rBuyer1.setText("Buyer");
        rBuyer1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rBuyer1ActionPerformed(evt);
            }
        });

        rPi1.setText("PI");
        rPi1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rPi1ActionPerformed(evt);
            }
        });

        rCount1.setText("Count");
        rCount1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rCount1ActionPerformed(evt);
            }
        });

        rRate1.setText("Rate");
        rRate1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRate1ActionPerformed(evt);
            }
        });

        rQuality1.setText("Quality");
        rQuality1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rQuality1ActionPerformed(evt);
            }
        });

        jButton3.setText("Next Report");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Back");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel109.setText("From");

        jLabel110.setText("To");

        jButton15.setText("Date");

        jLabel111.setText("Search");

        jButton16.setText("LC");

        jButton17.setText("PI");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bCreate_OIH_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bEditInfo_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(103, 103, 103)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bDeleteFrom_OIH_Report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bAddToWFCAReport1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(txt_totalAmount_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(63, 63, 63)
                                                .addComponent(txt_amount_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel32)
                                            .addComponent(jLabel34)))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addGap(2, 2, 2)
                                                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txt_comments_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(dateChooser_DelComDate_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel36))))
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(txt_todayDelivery_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(txt_balanceQuantity_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(dateChooser_SubmissionDate_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(dateChooser_DocumentHandOver_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(252, 252, 252)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(jLabel111)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txt_description2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(jLabel109)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jDateChooser7, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(32, 32, 32)
                                        .addComponent(jLabel110)
                                        .addGap(18, 18, 18)
                                        .addComponent(jDateChooser6, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                        .addGap(4, 4, 4)
                                        .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(3, 3, 3)
                                        .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addGap(2, 2, 2)
                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txt_comments_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(DateChooser_DelComDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_value_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addGap(34, 34, 34)
                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(24, 24, 24))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel28)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(DateChooser_DocumentHandOverDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(DateChooser_SubmissionDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_totalvalue_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(35, 35, 35)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(bCreateWFCAReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(bEditInfo_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(104, 104, 104)
                                        .addComponent(bDeleteFromWFCAReport, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(105, 105, 105)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(rBuyer1)
                                            .addComponent(rQuality1)
                                            .addComponent(rPi1)
                                            .addComponent(rCount1)
                                            .addComponent(rRate1)
                                            .addComponent(rDate1)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                        .addGap(43, 43, 43)
                                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addGap(283, 283, 283))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addComponent(jScrollPane5))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel29)
                                    .addComponent(txt_amount_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addComponent(jLabel30))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel32)
                                    .addComponent(txt_todayDelivery_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(15, 15, 15)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel34)
                                            .addComponent(txt_totalAmount_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(16, 16, 16)
                                        .addComponent(txt_balanceQuantity_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(dateChooser_SubmissionDate_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(dateChooser_DocumentHandOver_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel31)
                                    .addComponent(dateChooser_DelComDate_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel35))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_comments_OIH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel36)
                                    .addComponent(jLabel33))))
                        .addGap(44, 44, 44))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel109)
                                    .addComponent(jDateChooser7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel110)
                                        .addComponent(jDateChooser6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(33, 33, 33)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel111)
                                    .addComponent(txt_description2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jButton15)
                                .addGap(17, 17, 17)
                                .addComponent(jButton16)
                                .addGap(16, 16, 16)
                                .addComponent(jButton17)))
                        .addGap(17, 17, 17)))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bEditInfo_OIH)
                    .addComponent(bAddToWFCAReport1))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bCreate_OIH_report)
                    .addComponent(bDeleteFrom_OIH_Report))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_value_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_totalvalue_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel24)
                            .addComponent(jLabel23))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(DateChooser_SubmissionDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(DateChooser_DocumentHandOverDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel25)
                                    .addComponent(DateChooser_DelComDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel27))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_comments_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel28)
                                    .addComponent(jLabel26))))
                        .addGap(44, 44, 44)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bEditInfo_WFCA)
                            .addComponent(bDeleteFromWFCAReport))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bCreateWFCAReport)
                            .addComponent(jButton4)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(rDate1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rBuyer1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rPi1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rCount1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rRate1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rQuality1)))
                .addGap(16, 16, 16)
                .addComponent(jButton3)
                .addContainerGap(183, Short.MAX_VALUE))
        );

        jScrollPane6.setViewportView(jPanel3);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 1813, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(159, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6)
                .addContainerGap())
        );

        jTabbedPane1.addTab("WFCA", jPanel2);

        table_WFCA1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_WFCA1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_WFCA1MouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(table_WFCA1);

        txt_value_WFCA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_value_WFCA1ActionPerformed(evt);
            }
        });

        jLabel37.setText("Maturity.Date");

        txt_totalvalue_WFCA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalvalue_WFCA1ActionPerformed(evt);
            }
        });

        txt_comments_WFCA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_comments_WFCA1ActionPerformed(evt);
            }
        });

        jLabel38.setText("value");

        jLabel39.setText("Total value");

        jLabel40.setText("Del. Com. Date");

        jLabel41.setText("Comments");

        jLabel42.setText("Submission Date");

        jLabel43.setText("LDPD No.");

        txt_LDBP_WFCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_LDBP_WFCAActionPerformed(evt);
            }
        });

        table_WFBA.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_WFBA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_WFBAMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(table_WFBA);

        bCreateWFCAReport1.setText("WFCA Report");
        bCreateWFCAReport1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateWFCAReport1ActionPerformed(evt);
            }
        });

        bDeleteFromWFCAReport1.setText("Delete From WFCA Report");
        bDeleteFromWFCAReport1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromWFCAReport1ActionPerformed(evt);
            }
        });

        bEditInfo1_WFCA.setText("Edit Info");
        bEditInfo1_WFCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo1_WFCAActionPerformed(evt);
            }
        });

        jLabel44.setText("value");

        jLabel45.setText("Total value");

        jLabel46.setText("Comments");

        jLabel47.setText("LDPD No.");

        jLabel48.setText("Doc. REC. Date");

        jLabel49.setText("Bank.SUB.Date");

        txt_comments_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_comments_WFBAActionPerformed(evt);
            }
        });

        txt_totalvalue_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalvalue_WFBAActionPerformed(evt);
            }
        });

        txt_value_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_value_WFBAActionPerformed(evt);
            }
        });

        jLabel50.setText("Maturity.Date");

        bEditInfo_WFBA.setText("Edit Info");
        bEditInfo_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_WFBAActionPerformed(evt);
            }
        });

        bCreateWFBAReport_WFBA.setText("WFBA  Report");
        bCreateWFBAReport_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateWFBAReport_WFBAActionPerformed(evt);
            }
        });

        bDeleteFromWFBAReport_WFBA.setText("Delete From WFBA");
        bDeleteFromWFBAReport_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromWFBAReport_WFBAActionPerformed(evt);
            }
        });

        bAddToWFBAReport1.setText("ADD to WFBA");
        bAddToWFBAReport1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAddToWFBAReport1ActionPerformed(evt);
            }
        });

        rDate2.setText("Date");
        rDate2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rDate2ActionPerformed(evt);
            }
        });

        rBuyer2.setText("Buyer");
        rBuyer2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rBuyer2ActionPerformed(evt);
            }
        });

        rPi2.setText("PI");
        rPi2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rPi2ActionPerformed(evt);
            }
        });

        rCount2.setText("Count");
        rCount2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rCount2ActionPerformed(evt);
            }
        });

        rRate2.setText("Rate");
        rRate2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRate2ActionPerformed(evt);
            }
        });

        rQuality2.setText("Quality");
        rQuality2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rQuality2ActionPerformed(evt);
            }
        });

        jLabel51.setText("Doc. REC. Date");

        jLabel52.setText("Bank.SUB.Date");

        jLabel53.setText("Document Handover Date");

        jButton5.setText("Next Report");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("Back");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jLabel54.setText("Issue Date");

        jLabel55.setText("Issue Date");

        jLabel112.setText("From");

        jLabel113.setText("To");

        jButton18.setText("Date");

        jLabel114.setText("Search");

        jButton19.setText("LC");

        jButton20.setText("PI");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(154, 154, 154)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(bEditInfo1_WFCA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(bCreateWFCAReport1, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE))
                        .addGap(174, 174, 174)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(bDeleteFromWFCAReport1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(bAddToWFBAReport1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(97, 97, 97)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jLabel55))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(DateChooser_Issuedate_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txt_comments_WFBA)
                                        .addComponent(DateChooser_DocRecDate_WFBA, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                        .addComponent(txt_value_WFBA)))
                                .addGap(32, 32, 32)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel47)
                                    .addComponent(jLabel50))
                                .addGap(24, 24, 24)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_LDPD_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_totalvalue_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(DateChooser_BankSUBDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(DateChooser_MaturyDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(102, 102, 102)
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(rDate2)
                                            .addComponent(rBuyer2)
                                            .addComponent(rQuality2)
                                            .addComponent(rPi2)
                                            .addComponent(rCount2)
                                            .addComponent(rRate2)))
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(55, 55, 55)
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)))))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bCreateWFBAReport_WFBA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bEditInfo_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(91, 91, 91)
                                .addComponent(bDeleteFromWFBAReport_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(61, 61, 61))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(jPanel5Layout.createSequentialGroup()
                                                    .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(12, 12, 12)))
                                            .addComponent(jLabel43)
                                            .addComponent(jLabel37))
                                        .addGap(18, 18, 18)))
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(DateChooser_DocRecDate_WFCA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txt_comments_WFCA1, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(DateChooser_DelComDate_WFCA1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txt_value_WFCA1, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txt_LDBP_WFCA, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(34, 34, 34)
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanel5Layout.createSequentialGroup()
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(24, 24, 24)
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txt_totalvalue_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(DateChooser_SubmissionDate_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addComponent(DateChooser_Issuedate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(DateChooser_Maturitydate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel53)
                                    .addComponent(jLabel52, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel54))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(DateChooser_BankSUBDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(DateChooser_DocumentHandOverDate_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(255, 255, 255)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel114)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_description3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel112)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jDateChooser8, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addComponent(jLabel113)
                                .addGap(18, 18, 18)
                                .addComponent(jDateChooser9, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 1509, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 1518, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(71, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_value_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_totalvalue_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel39)
                                    .addComponent(jLabel38))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(DateChooser_SubmissionDate_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel42)
                                    .addComponent(DateChooser_DelComDate_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel40))
                                .addGap(17, 17, 17)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel41)
                                        .addComponent(txt_comments_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(DateChooser_DocumentHandOverDate_WFCA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel53))
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(23, 23, 23)
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(DateChooser_DocRecDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(DateChooser_BankSUBDate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jLabel52, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel43)
                            .addComponent(txt_LDBP_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel54)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel112)
                                    .addComponent(jDateChooser8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel113)
                                        .addComponent(jDateChooser9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(33, 33, 33)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel114)
                                    .addComponent(txt_description3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jButton18)
                                .addGap(17, 17, 17)
                                .addComponent(jButton19)
                                .addGap(16, 16, 16)
                                .addComponent(jButton20)))
                        .addGap(17, 17, 17)
                        .addComponent(DateChooser_Issuedate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel37)
                    .addComponent(DateChooser_Maturitydate_WFCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bEditInfo1_WFCA)
                    .addComponent(bAddToWFBAReport1))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bCreateWFCAReport1)
                    .addComponent(bDeleteFromWFCAReport1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_value_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_totalvalue_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel45)
                    .addComponent(jLabel44))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(DateChooser_BankSUBDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel48)
                            .addComponent(DateChooser_DocRecDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel49))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_comments_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel46)
                            .addComponent(jLabel47)
                            .addComponent(txt_LDPD_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(DateChooser_MaturyDate_WFBA, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel50)
                            .addComponent(jLabel55)
                            .addComponent(DateChooser_Issuedate_WFCA1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(rDate2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rBuyer2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rPi2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rCount2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rRate2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rQuality2)))
                .addGap(14, 14, 14)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bEditInfo_WFBA)
                        .addComponent(bDeleteFromWFBAReport_WFBA)))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bCreateWFBAReport_WFBA)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButton5)))
                .addContainerGap(81, Short.MAX_VALUE))
        );

        jScrollPane9.setViewportView(jPanel5);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("WFBA", jPanel4);

        table_WFBA1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_WFBA1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_WFBA1MouseClicked(evt);
            }
        });
        jScrollPane10.setViewportView(table_WFBA1);

        txt_totalvalue_WFBA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalvalue_WFBA1ActionPerformed(evt);
            }
        });

        txt_value_WFBA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_value_WFBA1ActionPerformed(evt);
            }
        });

        jLabel56.setText("Maturity.Date");

        jLabel57.setText("value");

        jLabel58.setText("Total value");

        jLabel59.setText("Comments");

        jLabel60.setText("LDPD No.");

        jLabel61.setText("Doc. REC. Date");

        jLabel62.setText("Bank.SUB.Date");

        txt_comments_WFBA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_comments_WFBA1ActionPerformed(evt);
            }
        });

        jLabel63.setText("Purchase.Date");

        jLabel64.setText("Issue Date");

        table_DRP.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_DRP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_DRPMouseClicked(evt);
            }
        });
        jScrollPane11.setViewportView(table_DRP);

        bDeleteFromWFCAReport2.setText("Delete From WFBA Report");
        bDeleteFromWFCAReport2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromWFCAReport2ActionPerformed(evt);
            }
        });

        bCreateDRPReport2.setText("DRP Report");
        bCreateDRPReport2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateDRPReport2ActionPerformed(evt);
            }
        });

        bEditInfo_WFBA1.setText("Edit Info");
        bEditInfo_WFBA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_WFBA1ActionPerformed(evt);
            }
        });

        bCreateWFBAReport_WFBA1.setText("WFBA  Report");
        bCreateWFBAReport_WFBA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateWFBAReport_WFBA1ActionPerformed(evt);
            }
        });

        bAddToDRPReport.setText("ADD to DRP");
        bAddToDRPReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAddToDRPReportActionPerformed(evt);
            }
        });

        rDate3.setText("Date");
        rDate3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rDate3ActionPerformed(evt);
            }
        });

        rBuyer3.setText("Buyer");
        rBuyer3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rBuyer3ActionPerformed(evt);
            }
        });

        rPi3.setText("PI");
        rPi3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rPi3ActionPerformed(evt);
            }
        });

        rCount3.setText("Count");
        rCount3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rCount3ActionPerformed(evt);
            }
        });

        rRate3.setText("Rate");
        rRate3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRate3ActionPerformed(evt);
            }
        });

        rQuality3.setText("Quality");
        rQuality3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rQuality3ActionPerformed(evt);
            }
        });

        jButton7.setText("Go To Realization Report");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setText("Back");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jLabel65.setText("Bank Name");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "EXIM", "SCB" }));

        jButton9.setText("Go To Purchase Report");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jLabel115.setText("From");

        jLabel116.setText("To");

        jButton21.setText("Date");

        jLabel117.setText("Search");

        jButton22.setText("LC");

        jButton23.setText("PI");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(298, 298, 298)
                .addComponent(bCreateDRPReport2, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(139, 139, 139)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rPi3)
                    .addComponent(rDate3)
                    .addComponent(rBuyer3)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rQuality3)
                            .addComponent(rCount3)
                            .addComponent(rRate3))
                        .addGap(98, 98, 98)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(1001, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane11))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(74, 74, 74)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGap(91, 91, 91)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(bCreateWFBAReport_WFBA1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(bEditInfo_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(91, 91, 91)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(bDeleteFromWFCAReport2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(bAddToDRPReport, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel7Layout.createSequentialGroup()
                                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                                        .addGap(2, 2, 2)
                                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel57, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(jLabel61, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                    .addComponent(txt_comments_WFBA1)
                                                    .addComponent(DateChooser_DocRecDate_WFBA1, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                                    .addComponent(txt_value_WFBA1)))
                                            .addGroup(jPanel7Layout.createSequentialGroup()
                                                .addComponent(jLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(DateChooser_Issue_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel7Layout.createSequentialGroup()
                                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel65, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(DateChooser_PurchaseDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGap(34, 34, 34)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel62, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel60)
                                            .addComponent(jLabel56))
                                        .addGap(24, 24, 24)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(DateChooser_BankSUBDate_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_totalvalue_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel7Layout.createSequentialGroup()
                                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(DateChooser_MaturyDate_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(txt_LDPB_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(305, 305, 305)
                                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                                        .addComponent(jLabel117)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(txt_description4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                                        .addComponent(jLabel115)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jDateChooser10, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(32, 32, 32)
                                                        .addComponent(jLabel116)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jDateChooser11, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                                                        .addGap(4, 4, 4)
                                                        .addComponent(jButton23, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                                        .addGap(3, 3, 3)
                                                        .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 1903, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(DateChooser_PurchaseDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel65)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(110, 110, 110)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bEditInfo_WFBA1)
                            .addComponent(bAddToDRPReport))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bCreateWFBAReport_WFBA1)
                            .addComponent(bDeleteFromWFCAReport2)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_value_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_totalvalue_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel58)
                            .addComponent(jLabel57))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createSequentialGroup()
                                        .addComponent(DateChooser_BankSUBDate_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(txt_LDPB_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel61)
                                            .addComponent(DateChooser_DocRecDate_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel62))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(txt_comments_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel59)
                                            .addComponent(jLabel60))))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel64)
                                        .addComponent(DateChooser_Issue_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel56))
                                    .addComponent(DateChooser_MaturyDate_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel63)
                                .addGap(197, 197, 197))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel115)
                                            .addComponent(jDateChooser10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel116)
                                                .addComponent(jDateChooser11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(33, 33, 33)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel117)
                                            .addComponent(txt_description4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addComponent(jButton21)
                                        .addGap(17, 17, 17)
                                        .addComponent(jButton22)
                                        .addGap(16, 16, 16)
                                        .addComponent(jButton23)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(239, 239, 239)
                        .addComponent(rDate3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rBuyer3)
                        .addGap(0, 0, 0)
                        .addComponent(rPi3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rCount3)
                            .addComponent(jButton8))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rRate3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rQuality3))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jButton7))))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(64, 64, 64)
                        .addComponent(bCreateDRPReport2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton9)
                .addGap(491, 491, 491))
        );

        jScrollPane12.setViewportView(jPanel7);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1982, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 1962, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 993, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 953, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(29, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("DRP", jPanel6);

        table_DRP1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_DRP1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_DRP1MouseClicked(evt);
            }
        });
        jScrollPane13.setViewportView(table_DRP1);

        txt_totalvalue_DRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalvalue_DRPActionPerformed(evt);
            }
        });

        txt_amount_DRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_amount_DRPActionPerformed(evt);
            }
        });

        jLabel66.setText("Maturity.Date");

        jLabel67.setText("value");

        jLabel68.setText("Total value");

        jLabel69.setText("LDPB No.");

        jLabel70.setText("Doc. REC. Date");

        jLabel71.setText("Bank.SUB.Date");

        jLabel72.setText("Fdd Ammount");

        txt_fddammount_DRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_fddammount_DRPActionPerformed(evt);
            }
        });

        jLabel73.setText("Fdd.Date");

        jLabel74.setText("Purchase.Date");

        jLabel75.setText("Issue Date");

        jLabel76.setText("Adjust.Date");

        jLabel77.setText("Realization.Date");

        table_Realization.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_Realization.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_RealizationMouseClicked(evt);
            }
        });
        jScrollPane14.setViewportView(table_Realization);

        jLabel78.setText("Realization.Date");

        jLabel79.setText("Fdd.Date");

        jLabel80.setText("Purchase.Date");

        jLabel81.setText("Adjust.Date");

        jLabel82.setText("Issue Date");

        bCreateDRPReport1.setText("DRP Report");
        bCreateDRPReport1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateDRPReport1ActionPerformed(evt);
            }
        });

        bAddToRealization.setText("Add to Realization");
        bAddToRealization.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAddToRealizationActionPerformed(evt);
            }
        });

        bDeleteFromDRPReport_DRP.setText("Delete From DRP Report");
        bDeleteFromDRPReport_DRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromDRPReport_DRPActionPerformed(evt);
            }
        });

        bEditInfo_DRP.setText("Edit Info");
        bEditInfo_DRP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_DRPActionPerformed(evt);
            }
        });

        bEditInfo_RE.setText("Edit Info");
        bEditInfo_RE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_REActionPerformed(evt);
            }
        });

        bCreateREeport.setText("Realization report");
        bCreateREeport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateREeportActionPerformed(evt);
            }
        });

        bDeleteFromrealization.setText("Delete From DRP");
        bDeleteFromrealization.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromrealizationActionPerformed(evt);
            }
        });

        jLabel83.setText("Bank Name");

        rQuality4.setText("Quality");
        rQuality4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rQuality4ActionPerformed(evt);
            }
        });

        rRate4.setText("Rate");
        rRate4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRate4ActionPerformed(evt);
            }
        });

        rCount4.setText("Count");
        rCount4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rCount4ActionPerformed(evt);
            }
        });

        rPi4.setText("PI");
        rPi4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rPi4ActionPerformed(evt);
            }
        });

        rBuyer4.setText("Buyer");
        rBuyer4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rBuyer4ActionPerformed(evt);
            }
        });

        rDate4.setText("Date");
        rDate4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rDate4ActionPerformed(evt);
            }
        });

        jButton10.setText("Back");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "EXIM", "SCB" }));

        jLabel118.setText("From");

        jLabel119.setText("To");

        jButton24.setText("Date");

        jLabel120.setText("Search");

        jButton25.setText("LC");

        jButton26.setText("PI");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel67, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(61, 61, 61)
                                .addComponent(txt_amount_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(jLabel68, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(74, 74, 74)
                                .addComponent(txt_totalvalue_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(bEditInfo_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(103, 103, 103)
                                .addComponent(bAddToRealization, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(bCreateDRPReport1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(103, 103, 103)
                                .addComponent(bDeleteFromDRPReport_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabel73, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel77, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jLabel83, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel66))
                                        .addGap(12, 12, 12)
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(DateChooser_Realization_DRP, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                                    .addComponent(DateChooser_FddAdd_DRP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(DateChooser_MaturyDate_DRP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGap(34, 34, 34)
                                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                                .addComponent(jLabel72)
                                                                .addGap(65, 65, 65)
                                                                .addComponent(txt_fddammount_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                                .addComponent(jLabel76, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(DateChooser_Adjust_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGap(235, 235, 235)
                                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                                .addComponent(jLabel120)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(txt_description5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                                .addComponent(jLabel118)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jDateChooser12, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(32, 32, 32)
                                                                .addComponent(jLabel119)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jDateChooser13, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGap(18, 18, 18)
                                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                                                .addGap(4, 4, 4)
                                                                .addComponent(jButton26, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                                .addGap(3, 3, 3)
                                                                .addComponent(jButton25, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                            .addComponent(jButton24, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                                        .addComponent(jLabel74, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(24, 24, 24)
                                                        .addComponent(DateChooser_PurchaseDate_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                .addGap(203, 203, 203)
                                                .addComponent(jLabel75, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(24, 24, 24)
                                                .addComponent(DateChooser_Issue_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                .addComponent(jLabel69)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(txt_LDPB_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                .addComponent(jLabel70, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(DateChooser_DocRecDate_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(34, 34, 34)
                                        .addComponent(jLabel71, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(24, 24, 24)
                                        .addComponent(DateChooser_BankSUBDate_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(bCreateREeport, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(415, 415, 415)
                                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                .addComponent(bEditInfo_RE, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(104, 104, 104))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                                .addComponent(jLabel81, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(DateChooser_Adjust_RE, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(32, 32, 32)))
                                        .addComponent(bDeleteFromrealization, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel79, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel82, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(10, 10, 10)
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(DateChooser_Issue_RE, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(DateChooser_FddAdd_RE, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(74, 74, 74)
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel78)
                                            .addComponent(jLabel80, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(24, 24, 24)
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(DateChooser_PurchaseDate_RE, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(DateChooser_Realization_RE, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(148, 148, 148)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rDate4)
                                    .addComponent(rBuyer4)
                                    .addComponent(rQuality4)
                                    .addComponent(rPi4)
                                    .addComponent(rCount4)
                                    .addComponent(rRate4)))))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 1931, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane14)))
                .addGap(17, 21, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_amount_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_totalvalue_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel67)
                            .addComponent(jLabel68))))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(DateChooser_DocRecDate_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DateChooser_BankSUBDate_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel70)
                            .addComponent(jLabel71))))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(DateChooser_Issue_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_LDPB_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel75)))
                    .addComponent(jLabel69))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel74)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel66, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(DateChooser_MaturyDate_DRP, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel73)
                                    .addComponent(DateChooser_FddAdd_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(DateChooser_Realization_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel77))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel83)
                                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bEditInfo_DRP)
                                    .addComponent(bAddToRealization)))
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel9Layout.createSequentialGroup()
                                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel118)
                                        .addComponent(jDateChooser12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel119)
                                            .addComponent(jDateChooser13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(33, 33, 33)
                                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel120)
                                        .addComponent(txt_description5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel9Layout.createSequentialGroup()
                                    .addComponent(jButton24)
                                    .addGap(17, 17, 17)
                                    .addComponent(jButton25)
                                    .addGap(16, 16, 16)
                                    .addComponent(jButton26))))
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bCreateDRPReport1)
                            .addComponent(bDeleteFromDRPReport_DRP))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel82)
                                            .addComponent(DateChooser_Issue_RE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel79)
                                            .addComponent(DateChooser_FddAdd_RE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel80)
                                            .addComponent(DateChooser_PurchaseDate_RE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel78)
                                            .addComponent(DateChooser_Realization_RE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(21, 21, 21)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel81)
                                    .addComponent(DateChooser_Adjust_RE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(45, 45, 45)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bEditInfo_RE)
                                    .addComponent(bDeleteFromrealization)))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(rDate4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rBuyer4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rPi4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rCount4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rRate4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rQuality4)))
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bCreateREeport)
                            .addComponent(jButton10)))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(DateChooser_PurchaseDate_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_fddammount_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel72))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(DateChooser_Adjust_DRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel76))))
                .addGap(864, 864, 864))
        );

        jScrollPane15.setViewportView(jPanel9);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane15, javax.swing.GroupLayout.DEFAULT_SIZE, 1962, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 971, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 22, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Realization", jPanel8);

        table_DRP2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_DRP2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_DRP2MouseClicked(evt);
            }
        });
        jScrollPane16.setViewportView(table_DRP2);

        jLabel84.setText("value");

        txt_amount_DRP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_amount_DRP1ActionPerformed(evt);
            }
        });

        jLabel85.setText("Total value");

        txt_totalvalue_DRP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalvalue_DRP1ActionPerformed(evt);
            }
        });

        jLabel86.setText("Doc. REC. Date");

        jLabel87.setText("Bank.SUB.Date");

        jLabel88.setText("LDPB No.");

        jLabel89.setText("Issue Date");

        jLabel90.setText("Maturity.Date");

        jLabel91.setText("Purchase.Date");

        jLabel92.setText("Fdd.Date");

        jLabel93.setText("Fdd Ammount");

        txt_fddammount_DRP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_fddammount_DRP1ActionPerformed(evt);
            }
        });

        jLabel94.setText("Realization.Date");

        jLabel95.setText("Adjust.Date");

        jLabel96.setText("Bank Name");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "EXIM", "SCB" }));

        bEditInfo_DRP1.setText("Edit Info");
        bEditInfo_DRP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_DRP1ActionPerformed(evt);
            }
        });

        bCreateDRPReport3.setText("DRP Report");
        bCreateDRPReport3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateDRPReport3ActionPerformed(evt);
            }
        });

        bAddToPurchase.setText("Add to Purchase");
        bAddToPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAddToPurchaseActionPerformed(evt);
            }
        });

        bDeleteFromDRPReport_DRP1.setText("Delete From DRP Report");
        bDeleteFromDRPReport_DRP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromDRPReport_DRP1ActionPerformed(evt);
            }
        });

        table_Purchase.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_Purchase.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_PurchaseMouseClicked(evt);
            }
        });
        jScrollPane17.setViewportView(table_Purchase);

        jLabel97.setText("Issue Date");

        jLabel98.setText("Purchase.Date");

        jLabel99.setText("Fdd.Date");

        jLabel100.setText("Realization.Date");

        jLabel101.setText("Adjust.Date");

        bEditInfo_RE1.setText("Edit Info");
        bEditInfo_RE1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_RE1ActionPerformed(evt);
            }
        });

        bDeleteFromrealization1.setText("Delete From DRP");
        bDeleteFromrealization1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromrealization1ActionPerformed(evt);
            }
        });

        bCreateREeport1.setText("Purchase report");
        bCreateREeport1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateREeport1ActionPerformed(evt);
            }
        });

        jButton11.setText("Back");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        rDate5.setText("Date");
        rDate5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rDate5ActionPerformed(evt);
            }
        });

        rBuyer5.setText("Buyer");
        rBuyer5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rBuyer5ActionPerformed(evt);
            }
        });

        rPi5.setText("PI");
        rPi5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rPi5ActionPerformed(evt);
            }
        });

        rCount5.setText("Count");
        rCount5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rCount5ActionPerformed(evt);
            }
        });

        rRate5.setText("Rate");
        rRate5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRate5ActionPerformed(evt);
            }
        });

        rQuality5.setText("Quality");
        rQuality5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rQuality5ActionPerformed(evt);
            }
        });

        jLabel121.setText("From");

        jLabel122.setText("To");

        jButton27.setText("Date");

        jLabel123.setText("Search");

        jButton28.setText("LC");

        jButton29.setText("PI");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel84, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(61, 61, 61)
                                .addComponent(txt_amount_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(jLabel85, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(74, 74, 74)
                                .addComponent(txt_totalvalue_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(bCreateDRPReport3, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(103, 103, 103)
                                .addComponent(bDeleteFromDRPReport_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(jLabel86, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(DateChooser_DocRecDate_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel88)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txt_LDPB_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(34, 34, 34)
                                .addComponent(jLabel87, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24)
                                .addComponent(DateChooser_BankSUBDate_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(bEditInfo_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(103, 103, 103)
                                .addComponent(bAddToPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel92, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel94, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel96, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel90))
                                .addGap(12, 12, 12)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(DateChooser_Realization_DRP1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(DateChooser_FddAdd_DRP1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(DateChooser_MaturyDate_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(34, 34, 34)
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addGroup(jPanel11Layout.createSequentialGroup()
                                                    .addComponent(jLabel93)
                                                    .addGap(65, 65, 65)
                                                    .addComponent(txt_fddammount_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel11Layout.createSequentialGroup()
                                                    .addComponent(jLabel95, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(DateChooser_Adjust_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(jPanel11Layout.createSequentialGroup()
                                                .addComponent(jLabel91, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(24, 24, 24)
                                                .addComponent(DateChooser_PurchaseDate_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel11Layout.createSequentialGroup()
                                                .addComponent(jLabel89, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(24, 24, 24)
                                                .addComponent(DateChooser_Issue_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(237, 237, 237)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel123)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txt_description6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel121)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jDateChooser14, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(32, 32, 32)
                                        .addComponent(jLabel122)
                                        .addGap(18, 18, 18)
                                        .addComponent(jDateChooser15, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                                        .addGap(4, 4, 4)
                                        .addComponent(jButton29, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addGap(3, 3, 3)
                                        .addComponent(jButton28, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jButton27, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(86, 86, 86)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel101, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(DateChooser_Adjust_PU, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel99, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel97, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(10, 10, 10)
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(DateChooser_Issue_PU, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(DateChooser_FddAdd_PU, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(74, 74, 74)
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel100)
                                            .addComponent(jLabel98, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(24, 24, 24)
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(DateChooser_PurchaseDate_PU, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(DateChooser_Realization_PU, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(bCreateREeport1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(415, 415, 415)
                                        .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(bEditInfo_RE1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(104, 104, 104)
                                        .addComponent(bDeleteFromrealization1, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(29, 29, 29)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rDate5)
                            .addComponent(rBuyer5)
                            .addComponent(rQuality5)
                            .addComponent(rPi5)
                            .addComponent(rCount5)
                            .addComponent(rRate5)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 1906, Short.MAX_VALUE)
                            .addComponent(jScrollPane17))))
                .addContainerGap(56, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_amount_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_totalvalue_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel84)
                            .addComponent(jLabel85))))
                .addGap(18, 18, 18)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(DateChooser_DocRecDate_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DateChooser_BankSUBDate_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel86)
                            .addComponent(jLabel87))))
                .addGap(21, 21, 21)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(DateChooser_Issue_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_LDPB_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel89)
                        .addComponent(jLabel88)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel91)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel90, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(DateChooser_MaturyDate_DRP1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel92)
                                    .addComponent(DateChooser_FddAdd_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(DateChooser_Realization_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel94))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel96)
                            .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bEditInfo_DRP1)
                            .addComponent(bAddToPurchase))
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bCreateDRPReport3)
                            .addComponent(bDeleteFromDRPReport_DRP1)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(DateChooser_PurchaseDate_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_fddammount_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel93))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(DateChooser_Adjust_DRP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel95)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel121)
                                    .addComponent(jDateChooser14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel122)
                                        .addComponent(jDateChooser15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(33, 33, 33)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel123)
                                    .addComponent(txt_description6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jButton27)
                                .addGap(17, 17, 17)
                                .addComponent(jButton28)
                                .addGap(16, 16, 16)
                                .addComponent(jButton29)))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel97)
                                    .addComponent(DateChooser_Issue_PU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel99)
                                    .addComponent(DateChooser_FddAdd_PU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel98)
                                    .addComponent(DateChooser_PurchaseDate_PU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel100)
                                    .addComponent(DateChooser_Realization_PU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(21, 21, 21)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel101)
                            .addComponent(DateChooser_Adjust_PU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(54, 54, 54)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bEditInfo_RE1)
                            .addComponent(bDeleteFromrealization1))
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bCreateREeport1)
                            .addComponent(jButton11)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(rDate5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rBuyer5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rPi5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rCount5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rRate5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rQuality5)))
                .addGap(433, 433, 433))
        );

        jScrollPane18.setViewportView(jPanel11);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane18, javax.swing.GroupLayout.DEFAULT_SIZE, 1962, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane18, javax.swing.GroupLayout.DEFAULT_SIZE, 971, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Purchase", jPanel10);

        Table_LC.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane19.setViewportView(Table_LC);

        jLabel102.setText("From");

        jLabel103.setText("To");

        LC_preview.setText("Preview");
        LC_preview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LC_previewActionPerformed(evt);
            }
        });

        bcreateLC_Report.setText("LC Reprt");
        bcreateLC_Report.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcreateLC_ReportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane19)
                .addContainerGap())
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(168, 168, 168)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel102)
                        .addGap(18, 18, 18)
                        .addComponent(DateChooser_from, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(122, 122, 122)
                        .addComponent(jLabel103)
                        .addGap(18, 18, 18)
                        .addComponent(DateChooser_to, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(LC_preview, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(97, 97, 97)
                        .addComponent(bcreateLC_Report, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(1272, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane19, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel103, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel102))
                    .addComponent(DateChooser_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DateChooser_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(69, 69, 69)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LC_preview)
                    .addComponent(bcreateLC_Report))
                .addContainerGap(577, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("LC_Register", jPanel12);

        Table_PI.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane20.setViewportView(Table_PI);

        jLabel104.setText("From");

        jLabel105.setText("To");

        PI_preview.setText("Preview");
        PI_preview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PI_previewActionPerformed(evt);
            }
        });

        bcreatePI_Report.setText("PI Reprt");
        bcreatePI_Report.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcreatePI_ReportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane20))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addComponent(jLabel104)
                        .addGap(18, 18, 18)
                        .addComponent(DateChooser_from1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(122, 122, 122)
                        .addComponent(jLabel105)
                        .addGap(18, 18, 18)
                        .addComponent(DateChooser_to1, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 1267, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(214, 214, 214)
                .addComponent(PI_preview, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(97, 97, 97)
                .addComponent(bcreatePI_Report, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane20, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel105, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel104))
                    .addComponent(DateChooser_from1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DateChooser_to1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(69, 69, 69)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PI_preview)
                    .addComponent(bcreatePI_Report))
                .addContainerGap(590, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("PI_Register", jPanel13);

        jScrollPane21.setViewportView(jTabbedPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane21, javax.swing.GroupLayout.DEFAULT_SIZE, 1989, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane21, javax.swing.GroupLayout.DEFAULT_SIZE, 1023, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void primary1stMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_primary1stMouseClicked
        // TODO add your handling code here:
        try {
            int row = primary1st.getSelectedRow();
            String lc_id = (primary1st.getModel().getValueAt(row, 1).toString());
            String count = (primary1st.getModel().getValueAt(row, 7).toString());
            String sql = "SELECT b.name, lc.lc_id,lc.issue_date,lc.shipment_date, lc.expiry_date, lc.sight_days, lpr.pi_id, g.description, g.quantity, g.unit_price"
            + " FROM letter_of_credit AS lc"
            + " INNER JOIN buyer AS b ON b.buyer_id = lc.buyer_id"
            + " INNER JOIN lc_pi_relation AS lpr ON lpr.lc_id=lc.lc_id"
            + " INNER JOIN goods AS g ON g.pi_id=lpr.pi_id"
            //+ " WHERE lc.lc_id='000113040333'";
            //+ " WHERE lc.issue_date BETWEEN CAST( '"+fromDate +"' AS DATE) AND CAST( '"+toDate+"'  AS DATE)"
            //+ " ORDER BY b.name ASC ";
            + " WHERE lc.lc_id = '" + lc_id + "' AND g.description = '" + count + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                String add1 = rs.getString("b.name");
                txt_name.setText(add1);
                String add2 = rs.getString("lc_id");
                txt_lc.setText(add2);
                String add3 = rs.getString("issue_date");
                txt_issue.setText(add3);
                String add4 = rs.getString("shipment_date");
                txt_shipment.setText(add4);
                String add5 = rs.getString("expiry_date");
                txt_expiry.setText(add5);
                String add6 = rs.getString("sight_days");
                txt_sight.setText(add6);
                String add7 = rs.getString("pi_id");
                txt_pi.setText(add7);
                String add8 = rs.getString("description");
                txt_description.setText(add8);
                String quantity = rs.getString("quantity");
                txt_quantity.setText(quantity);
                String rate = rs.getString("unit_price");
                txt_unit.setText(rate);
                System.out.println("after end");

                double dAmount = Double.parseDouble(quantity);
                txt_amount.setText(String.valueOf(dAmount));
                txt_totalAmount.setText("0.0");
                txt_balanceQuantity.setText(String.valueOf(dAmount));
                txt_todayDelivery.setText("0.0");

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_primary1stMouseClicked

    private void txt_amountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_amountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_amountActionPerformed

    private void txt_todayDeliveryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_todayDeliveryActionPerformed
        Double balanceQuantity = Double.parseDouble(txt_balanceQuantity.getText()) - Double.parseDouble(txt_todayDelivery.getText());
        txt_balanceQuantity.setText(String.valueOf(balanceQuantity));
    }//GEN-LAST:event_txt_todayDeliveryActionPerformed

    private void txt_totalAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalAmountActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            String sql = "INSERT IGNORE INTO orderinhand (Buyer, LC_ID, LC_Date, Last_Date_Of_Shipment,"
            + " Expiry_Date, LC_sight_Days, PI_No, Count, Quantity, Rate, Amount,"
            + " Total_Amount, Today_Delivery, Balance_Quantity) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_name.getText());
            pst.setString(2, txt_lc.getText());
            pst.setString(3, txt_issue.getText());
            pst.setString(4, txt_shipment.getText());
            pst.setString(5, txt_expiry.getText());
            pst.setString(6, txt_sight.getText());
            pst.setString(7, txt_pi.getText());
            pst.setString(8, txt_description.getText());
            pst.setString(9, txt_quantity.getText());
            pst.setString(10, txt_unit.getText());
            pst.setString(11, txt_amount.getText());
            pst.setString(12, txt_totalAmount.getText());
            pst.setString(13, txt_todayDelivery.getText());
            pst.setString(14, txt_balanceQuantity.getText());

            pst.executeUpdate();
            System.out.println(":::::");

        } catch (Exception e) {
            System.out.println(e);
        }
        updateTable1();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txt_issueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_issueActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_issueActionPerformed

    private void txt_shipmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_shipmentActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_shipmentActionPerformed

    private void txt_expiryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_expiryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_expiryActionPerformed

    private void txt_piActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_piActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_piActionPerformed

    private void OrderInHandTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_OrderInHandTableMouseClicked
        try {
            int row = OrderInHandTable.getSelectedRow();
            lc_id = (OrderInHandTable.getModel().getValueAt(row, 1).toString());
            count = (OrderInHandTable.getModel().getValueAt(row, 7).toString());
            String sql = "SELECT * FROM orderinhand"
            + " WHERE LC_ID = '" + lc_id + "' AND Count = '" + count + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                String buyer = rs.getString("Buyer");
                //txt_name.setText(add1);
                String lcId = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                String lcDate = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                String lastDateOfShipment = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                String expiryDate = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);
                String lcSight = rs.getString("LC_sight_Days");
                //txt_sight.setText(lcSight);
                String piNo = rs.getString("PI_No");
                //txt_pi.setText(piNo);
                String countDetails = rs.getString("Count");
                String quantity = rs.getString("Quantity");
                String rate = rs.getString("Rate");
                String amount = rs.getString("Amount");
                String totalAmount = rs.getString("Total_Amount");
                String todayDelivery = rs.getString("Today_Delivery");
                String balanceQuantity = rs.getString("Balance_Quantity");

                txt_amount1.setText(amount);
                txt_totalAmount1.setText(totalAmount);
                txt_balanceQuantity1.setText(balanceQuantity);
                txt_todayDelivery1.setText(todayDelivery);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_OrderInHandTableMouseClicked

    private void txt_amount1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_amount1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_amount1ActionPerformed

    private void txt_todayDelivery1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_todayDelivery1ActionPerformed
        // TODO add your handling code here:
        Double balanceQuantity = Double.parseDouble(txt_balanceQuantity1.getText()) - Double.parseDouble(txt_todayDelivery1.getText());
        txt_balanceQuantity1.setText(String.valueOf(balanceQuantity));
    }//GEN-LAST:event_txt_todayDelivery1ActionPerformed

    private void txt_totalAmount1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalAmount1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalAmount1ActionPerformed

    private void txt_commentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_commentsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_commentsActionPerformed

    private void bEditInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfoActionPerformed
        try {
            String sql = "UPDATE orderinhand SET Amount =  ? , Total_Amount = ? , Today_Delivery = ? , "
            + " Balance_Quantity = ? WHERE  LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_amount1.getText());
            pst.setString(2, txt_totalAmount1.getText());
            pst.setString(3, txt_todayDelivery1.getText());
            pst.setString(4, txt_balanceQuantity1.getText());
            pst.setString(5, lc_id);
            pst.setString(6, count);
            pst.executeUpdate();
            updateTable1();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfoActionPerformed

    private void bCreateOrderInHandReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateOrderInHandReportActionPerformed
        ReportOrderInHand orderInHand = new ReportOrderInHand();
        try {
            orderInHand.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateOrderInHandReportActionPerformed

    private void bAddToWFCAReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAddToWFCAReportActionPerformed
        // TODO add your handling code here:
        MYFIRSTTABLE mf = new MYFIRSTTABLE();
        Date fromDate = jDateChooser1.getDate();
        Date toDate = jDateChooser2.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sFromDate = dateFormat.format(fromDate);
        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);
        try {
            if(!(sFromDate.equals(null)) && !(sToDate.equals(null))){
                mf.initiate(sFromDate,sToDate);
            }else{
                JOptionPane.showMessageDialog(null,"Please Choose both date!!!" );
                //mf.initiate();
            }
        } catch (DocumentException ex) {
            //Logger.getLogger(mainframe.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        } catch (Exception ex) {
            //Logger.getLogger(mainframe.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_bAddToWFCAReportActionPerformed

    private void bDeleteFromOrderInHandReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromOrderInHandReportActionPerformed
        try {
            String sql = "DELETE FROM orderinhand WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lc_id);
            pst.setString(2, count);
            pst.executeUpdate();
            updateTable();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromOrderInHandReportActionPerformed

    private void rDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rDateActionPerformed
        orderCondition = " LC_Date ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rDateActionPerformed

    private void rBuyerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rBuyerActionPerformed
        orderCondition = " Buyer ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rBuyerActionPerformed

    private void rPiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rPiActionPerformed
        orderCondition = " PI_No ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rPiActionPerformed

    private void rCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rCountActionPerformed
        orderCondition = " Count ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rCountActionPerformed

    private void rRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRateActionPerformed
        orderCondition = " Rate ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rRateActionPerformed

    private void rQualityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rQualityActionPerformed
        orderCondition = " Quantity ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rQualityActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
        new WFCAFrame().setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void table_OrderInHandMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_OrderInHandMouseClicked
        try {
            int row = table_OrderInHand.getSelectedRow();
            lc_id_OIH = (table_OrderInHand.getModel().getValueAt(row, 1).toString());
            count_OIH = (table_OrderInHand.getModel().getValueAt(row, 7).toString());
            String sql = "SELECT * FROM orderinhand"
            + " WHERE LC_ID = '" + lc_id_OIH + "' AND Count = '" + count_OIH + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);
                lcSight = rs.getString("LC_sight_Days");
                //txt_sight.setText(lcSight);
                piNo = rs.getString("PI_No");
                //txt_pi.setText(piNo);
                countDetails = rs.getString("Count");
                quantity = rs.getString("Quantity");
                rate = rs.getString("Rate");
                amount = rs.getString("Amount");
                totalAmount = rs.getString("Total_Amount");
                String todayDelivery = rs.getString("Today_Delivery");
                String balanceQuantity = rs.getString("Balance_Quantity");

                txt_amount_OIH.setText(amount);
                txt_totalAmount_OIH.setText(totalAmount);
                txt_balanceQuantity_OIH.setText(balanceQuantity);
                txt_todayDelivery_OIH.setText(todayDelivery);

                txt_comments_OIH.setText("");
                dateChooser_DelComDate_OIH.setDate(null);
                dateChooser_DocumentHandOver_OIH.setDate(null);
                dateChooser_SubmissionDate_OIH.setDate(null);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_OrderInHandMouseClicked

    private void bCreateWFCAReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateWFCAReportActionPerformed
        ReportWFCA mf = new ReportWFCA();
        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateWFCAReportActionPerformed

    private void bDeleteFromWFCAReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromWFCAReportActionPerformed
        try {
            String sql = "DELETE FROM wfca WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lc_id_WFCA);
            pst.setString(2, count_WFCA);
            pst.executeUpdate();
            updateTableWFCA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromWFCAReportActionPerformed

    private void txt_value_WFCAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_value_WFCAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_value_WFCAActionPerformed

    private void txt_totalvalue_WFCAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalvalue_WFCAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalvalue_WFCAActionPerformed

    private void txt_comments_WFCAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_comments_WFCAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_comments_WFCAActionPerformed

    private void bEditInfo_WFCAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_WFCAActionPerformed
        try {
            Date deliveryCompleteDate = DateChooser_DelComDate_WFCA.getDate();
            Date submissionDate = DateChooser_SubmissionDate_WFCA.getDate();
            Date documentHandoverDate = DateChooser_DocumentHandOverDate_WFCA.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String sDeliveryCompleteDate = dateFormat.format(deliveryCompleteDate);
            String sSubmissionDate = dateFormat.format(submissionDate);
            String sDocumentHandoverDate = dateFormat.format(documentHandoverDate);

            String sql = "UPDATE wfca SET Value =  ? , Total_Value = ? , Del_Com_Date = ? , "
            + " Submission_Date = ? , Comments = ? , Doc_Han_Date = ? WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_value_WFCA.getText());
            pst.setString(2, txt_totalvalue_WFCA.getText());
            pst.setString(3, sDeliveryCompleteDate);
            pst.setString(4, sSubmissionDate);
            pst.setString(5, txt_comments_WFCA.getText());
            pst.setString(6, sDocumentHandoverDate);
            pst.setString(7, lc_id_WFCA);
            pst.setString(8, count_WFCA);
            pst.executeUpdate();
            updateTableWFCA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_WFCAActionPerformed

    private void table_WFCAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_WFCAMouseClicked
        try {
            int row = table_WFCA.getSelectedRow();
            lc_id_WFCA = (table_WFCA.getModel().getValueAt(row, 1).toString());
            count_WFCA = (table_WFCA.getModel().getValueAt(row, 11).toString());
            String sql = "SELECT * FROM wfca"
            + " WHERE LC_ID = '" + lc_id_WFCA + "' AND Count = '" + count_WFCA + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_wfca = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_wfca = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_wfca = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_wfca = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_wfca = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);
                lcSight_wfca = rs.getString("LC_sight_Days");
                //txt_sight.setText(lcSight);
                piNo_wfca = rs.getString("PI_No");
                //txt_pi.setText(piNo);
                del_Com_Date_wfca = rs.getString("Del_Com_Date");
                Submission_Date_wfca= rs.getString("Submission_Date");
                comments_wfca = rs.getString("Comments");
                docHandDate_wfca = rs.getString("Doc_Han_Date");
                countDetails = rs.getString("Count");
                quantity_wfca = rs.getString("Quantity");
                rate_wfca = rs.getString("Rate");
                String value_wfca = rs.getString("Value");
                String totalValue_wfca = rs.getString("Total_Value");

                txt_value_WFCA.setText(value_wfca);
                txt_totalvalue_WFCA.setText(totalValue_wfca);
                DateChooser_DelComDate_WFCA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(del_Com_Date_wfca));
                DateChooser_SubmissionDate_WFCA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(Submission_Date_wfca));
                DateChooser_DocumentHandOverDate_WFCA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(docHandDate_wfca));
                txt_comments_WFCA.setText(comments_wfca);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_WFCAMouseClicked

    private void bCreate_OIH_reportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreate_OIH_reportActionPerformed
        ReportOrderInHand mf = new ReportOrderInHand();
        //        Date fromDate = jDateChooser1.getDate();
        //        Date toDate = jDateChooser2.getDate();
        //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //        String sFromDate = dateFormat.format(fromDate);
        //        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);

        try {
            mf.initiate(" Buyer ASC ");
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreate_OIH_reportActionPerformed

    private void bAddToWFCAReport1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAddToWFCAReport1ActionPerformed
        // TODO add your handling code here:
        //        MYFIRSTTABLE mf = new MYFIRSTTABLE();
        Date deliveryCompleteDate = dateChooser_DelComDate_OIH.getDate();
        Date submissionDate = dateChooser_SubmissionDate_OIH.getDate();
        Date documentHandoverDate = dateChooser_DocumentHandOver_OIH.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sDeliveryCompleteDate = dateFormat.format(deliveryCompleteDate);
        String sSubmissionDate = dateFormat.format(submissionDate);
        String sDocumentHandoverDate = dateFormat.format(documentHandoverDate);
        //System.out.println(sFromDate+" TO "+sToDate);
        try {
            String sql = "INSERT IGNORE INTO wfca (Buyer, LC_ID, LC_Date, Last_Date_Of_Shipment,"
            + " Expiry_Date, LC_sight_Days, PI_No, Del_Com_Date, Submission_Date, Comments, Doc_Han_Date,  Count, Quantity, Rate, Value,"
            + " Total_Value) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, buyer);
            pst.setString(2, lcId);
            pst.setString(3, lcDate);
            pst.setString(4, lastDateOfShipment);
            pst.setString(5, expiryDate);
            pst.setString(6, lcSight);
            pst.setString(7, piNo);
            if(!(sDeliveryCompleteDate==null) && !(sDocumentHandoverDate==null) && !(sSubmissionDate==null)){
                pst.setString(8, sDeliveryCompleteDate);
                pst.setString(9, sSubmissionDate);
                pst.setString(10, txt_comments_OIH.getText());
                pst.setString(11, sDocumentHandoverDate);
            }else{
                JOptionPane.showMessageDialog(null,"Please Choose all three dates!!!" );
                //mf.initiate();
            }

            pst.setString(12, countDetails);
            pst.setString(13, quantity);
            pst.setString(14, rate);
            pst.setString(15, amount);
            pst.setString(16, totalAmount);
            pst.executeUpdate();
            System.out.println(":::::");

        } catch (Exception e) {
            System.out.println(e);
        }
        updateTableWFCA();

    }//GEN-LAST:event_bAddToWFCAReport1ActionPerformed

    private void bDeleteFrom_OIH_ReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFrom_OIH_ReportActionPerformed
        try {
            String sql = "DELETE FROM orderinhand WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lc_id_OIH);
            pst.setString(2, count_OIH);
            pst.executeUpdate();
            updateTableOrderInHand();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFrom_OIH_ReportActionPerformed

    private void txt_amount_OIHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_amount_OIHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_amount_OIHActionPerformed

    private void txt_todayDelivery_OIHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_todayDelivery_OIHActionPerformed
        // TODO add your handling code here:
        Double balanceQuantity = Double.parseDouble(txt_balanceQuantity_OIH.getText()) - Double.parseDouble(txt_todayDelivery_OIH.getText());
        txt_balanceQuantity_OIH.setText(String.valueOf(balanceQuantity));
    }//GEN-LAST:event_txt_todayDelivery_OIHActionPerformed

    private void txt_totalAmount_OIHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalAmount_OIHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalAmount_OIHActionPerformed

    private void txt_comments_OIHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_comments_OIHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_comments_OIHActionPerformed

    private void bEditInfo_OIHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_OIHActionPerformed
        try {
            String sql = "UPDATE orderinhand SET Amount =  ? , Total_Amount = ? , Today_Delivery = ? , "
            + " Balance_Quantity = ? WHERE  LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_amount_OIH.getText());
            pst.setString(2, txt_totalAmount_OIH.getText());
            pst.setString(3, txt_todayDelivery_OIH.getText());
            pst.setString(4, txt_balanceQuantity_OIH.getText());
            pst.setString(5, lc_id_OIH);
            pst.setString(6, count_OIH);
            pst.executeUpdate();
            updateTableOrderInHand();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_OIHActionPerformed

    private void rDate1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rDate1ActionPerformed
        orderCondition = " LC_Date ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rDate1ActionPerformed

    private void rBuyer1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rBuyer1ActionPerformed
        orderCondition = " Buyer ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rBuyer1ActionPerformed

    private void rPi1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rPi1ActionPerformed
        orderCondition = " PI_No ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rPi1ActionPerformed

    private void rCount1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rCount1ActionPerformed
        orderCondition = " Count ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rCount1ActionPerformed

    private void rRate1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRate1ActionPerformed
        orderCondition = " Rate ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rRate1ActionPerformed

    private void rQuality1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rQuality1ActionPerformed
        orderCondition = " Quantity ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rQuality1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        dispose();
        new FrameWFBA().setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        dispose();
        new primarydatatable().setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void table_WFCA1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_WFCA1MouseClicked
        // TODO add your handling code here:
        try {
            int row = table_WFCA.getSelectedRow();
            lc_id_WFCA = (table_WFCA.getModel().getValueAt(row, 1).toString());
            count_WFCA = (table_WFCA.getModel().getValueAt(row, 11).toString());
            String sql = "SELECT * FROM wfca"
            + " WHERE LC_ID = '" + lc_id_WFCA + "' AND Count = '" + count_WFCA + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_wfca = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_wfca = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_wfca = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_wfca = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_wfca = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);
                lcSight_wfca = rs.getString("LC_sight_Days");
                //txt_sight.setText(lcSight);
                piNo_wfca = rs.getString("PI_No");
                //txt_pi.setText(piNo);
                del_Com_Date_wfca = rs.getString("Del_Com_Date");
                Submission_Date_wfca= rs.getString("Submission_Date");
                comments_wfca = rs.getString("Comments");
                docHandDate_wfca = rs.getString("Doc_Han_Date");
                countDetails_wfca = rs.getString("Count");
                quantity_wfca = rs.getString("Quantity");
                rate_wfca = rs.getString("Rate");
                String value_wfca = rs.getString("Value");
                String totalValue_wfca = rs.getString("Total_Value");

                txt_value_WFCA.setText(value_wfca);
                txt_totalvalue_WFCA.setText(totalValue_wfca);
                DateChooser_DelComDate_WFCA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(del_Com_Date_wfca));
                DateChooser_SubmissionDate_WFCA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(Submission_Date_wfca));
                DateChooser_DocumentHandOverDate_WFCA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(docHandDate_wfca));
                txt_comments_WFCA.setText(comments_wfca);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_WFCA1MouseClicked

    private void txt_value_WFCA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_value_WFCA1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_value_WFCA1ActionPerformed

    private void txt_totalvalue_WFCA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalvalue_WFCA1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalvalue_WFCA1ActionPerformed

    private void txt_comments_WFCA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_comments_WFCA1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_comments_WFCA1ActionPerformed

    private void txt_LDBP_WFCAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_LDBP_WFCAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_LDBP_WFCAActionPerformed

    private void table_WFBAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_WFBAMouseClicked
        // TODO add your handling code here:
        try {
            int row = table_WFBA.getSelectedRow();
            lcId_wfba = (table_WFBA.getModel().getValueAt(row, 1).toString());
            count_wfba = (table_WFBA.getModel().getValueAt(row, 12).toString());
            String sql = "SELECT * FROM wfba"
            + " WHERE LC_ID = '" + lcId_wfba + "' AND Count = '" + count_wfba + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_wfba = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_wfba = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_wfba = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_wfba = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_wfba = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);
                lcSight_wfba = rs.getString("LC_sight_Days");
                //txt_sight.setText(lcSight);
                piNo_wfba = rs.getString("PI_No");
                //txt_pi.setText(piNo);
                sDocRecDate_wfba= rs.getString("Doc_Rec_Date");
                sBankSUBDate_wfba= rs.getString("Bank_Sub_Date");
                comments_wfba = rs.getString("Comments");
                sMaturityDate_wfba = rs.getString("Maturity_Date");
                LDPB_No_wfba = rs.getString("LDBP_No");
                count_wfba = rs.getString("Count");
                quantity_wfba = rs.getString("Quantity");
                rate_wfba = rs.getString("Rate");
                String value_wfba = rs.getString("Value");
                String totalValue_wfba = rs.getString("Total_Value");

                txt_value_WFBA.setText(value_wfba);
                txt_totalvalue_WFBA.setText(totalValue_wfba);
                DateChooser_DocRecDate_WFBA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_wfba));
                DateChooser_BankSUBDate_WFBA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_wfba));
                txt_comments_WFBA.setText(comments_wfba);
                DateChooser_MaturyDate_WFBA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_wfba));

                txt_LDPD_WFBA.setText(LDPB_No_wfba);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_WFBAMouseClicked

    private void bCreateWFCAReport1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateWFCAReport1ActionPerformed
        ReportWFCA mf = new ReportWFCA();
        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateWFCAReport1ActionPerformed

    private void bDeleteFromWFCAReport1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromWFCAReport1ActionPerformed
        try {
            String sql = "DELETE FROM wfca WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lcId_wfca);
            pst.setString(2, countDetails_wfca);
            pst.executeUpdate();
            updateTableWFCA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromWFCAReport1ActionPerformed

    private void bEditInfo1_WFCAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo1_WFCAActionPerformed
        try {

            Date deliveryCompleteDate_wfca = DateChooser_DelComDate_WFCA.getDate();
            Date submissionDate_wfca = DateChooser_SubmissionDate_WFCA.getDate();
            Date documentHandoverDate_wfca = DateChooser_DocumentHandOverDate_WFCA.getDate();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String sDeliveryCompleteDate = dateFormat.format(deliveryCompleteDate_wfca);

            String sSubmissionDate = dateFormat.format(submissionDate_wfca);

            String sDocumentHandover_WFCA = dateFormat.format(documentHandoverDate_wfca);

            String sql = "UPDATE wfca SET Value =  ? , Total_Value = ? , Del_Com_Date = ? , "
            + " Submission_Date = ? , Comments = ? , Doc_Han_Date = ? WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_value_WFCA.getText());
            pst.setString(2, txt_totalvalue_WFCA.getText());
            pst.setString(3, sDeliveryCompleteDate);
            pst.setString(4, sSubmissionDate);
            pst.setString(5, txt_comments_WFCA.getText());
            pst.setString(6, sDocumentHandover_WFCA);
            pst.setString(7, lc_id_WFCA);
            pst.setString(8, count_WFCA);
            pst.executeUpdate();
            updateTableWFCA();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_bEditInfo1_WFCAActionPerformed

    private void txt_comments_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_comments_WFBAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_comments_WFBAActionPerformed

    private void txt_totalvalue_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalvalue_WFBAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalvalue_WFBAActionPerformed

    private void txt_value_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_value_WFBAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_value_WFBAActionPerformed

    private void bEditInfo_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_WFBAActionPerformed
        try {
            System.out.println("in edit");
            Date DocRecDate_wfba = DateChooser_DocRecDate_WFBA.getDate();
            Date BankSUBDate_wfba = DateChooser_BankSUBDate_WFBA.getDate();
            Date MaturityDate_wfba = DateChooser_MaturyDate_WFBA.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println("in edit");
            String sDocRecDate_WFBA = dateFormat.format(DocRecDate_wfba);
            System.out.println("in edit");
            String sBankSUBDate_WFBA = dateFormat.format(BankSUBDate_wfba);
            System.out.println("in edit");
            String sMaturityDate_WFBA = dateFormat.format(MaturityDate_wfba);
            System.out.println("in edit");

            String sql = "UPDATE wfba SET Value =  ? , Total_Value = ? , Doc_Rec_Date = ? , "
            + " Bank_Sub_Date = ? , Comments = ? , Maturity_Date = ? , LDBP_No = ? WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_value_WFBA.getText());
            pst.setString(2, txt_totalvalue_WFBA.getText());
            pst.setString(3, sDocRecDate_WFBA);
            pst.setString(4, sBankSUBDate_WFBA);
            pst.setString(5, txt_comments_WFBA.getText());
            pst.setString(6, sMaturityDate_WFBA);
            pst.setString(7, txt_LDPD_WFBA.getText());
            pst.setString(8, lcId_wfba);
            pst.setString(9, count_wfba);
            pst.executeUpdate();
            updateTableWFBA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_WFBAActionPerformed

    private void bCreateWFBAReport_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateWFBAReport_WFBAActionPerformed
        ReportWFBA mf = new ReportWFBA();
        //        Date fromDate = jDateChooser1.getDate();
        //        Date toDate = jDateChooser2.getDate();
        //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //        String sFromDate = dateFormat.format(fromDate);
        //        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);

        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateWFBAReport_WFBAActionPerformed

    private void bDeleteFromWFBAReport_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromWFBAReport_WFBAActionPerformed
        try {
            String sql = "DELETE FROM wfba WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lcId_wfba);
            pst.setString(2, count_wfba);
            pst.executeUpdate();
            updateTableWFBA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromWFBAReport_WFBAActionPerformed

    private void bAddToWFBAReport1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAddToWFBAReport1ActionPerformed
        // TODO add your handling code here:
        Date DocRecDate_WFCA = DateChooser_DelComDate_WFCA.getDate();
        Date BankSUBDate_WFCA = DateChooser_SubmissionDate_WFCA.getDate();
        Date IssueDate_WFCA = DateChooser_Issuedate_WFCA.getDate();
        Date MaturityDate_WFCA = DateChooser_Maturitydate_WFCA.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sDocRecDate_WFCA = dateFormat.format(DocRecDate_WFCA);
        String sBankSUBDate_WFCA = dateFormat.format(BankSUBDate_WFCA);
        String sIssueDate_WFCA = dateFormat.format(IssueDate_WFCA);
        String sMaturityDate_WFCA = dateFormat.format(MaturityDate_WFCA);

        //System.out.println(sFromDate+" TO "+sToDate);
        try {
            String sql = "INSERT IGNORE INTO wfba (Buyer, LC_ID, LC_Date, Last_Date_Of_Shipment,"
            + " Expiry_Date, LC_sight_Days, PI_No, Doc_Rec_Date, Bank_Sub_Date, Comments, Maturity_Date, LDBP_No , Count, Quantity, Rate, Value,"
            + " Total_Value) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?)";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, buyer_wfca);
            pst.setString(2, lcId_wfca);
            pst.setString(3, lcDate_wfca);
            pst.setString(4, lastDateOfShipment_wfca);
            pst.setString(5, expiryDate_wfca);
            pst.setString(6, lcSight_wfca);
            pst.setString(7, piNo_wfca);
            if(!(sDocRecDate_WFCA==null) && !(sBankSUBDate_WFCA==null) && !(sMaturityDate_WFCA==null)){
                pst.setString(8, sDocRecDate_WFCA);
                pst.setString(9, sBankSUBDate_WFCA);
                pst.setString(10, txt_comments_WFCA.getText());
                pst.setString(11, sMaturityDate_WFCA);
            }else{
                JOptionPane.showMessageDialog(null,"Please Choose all three dates!!!" );
                //mf.initiate();
            }
            pst.setString(12, txt_LDBP_WFCA.getText());
            pst.setString(13, countDetails_wfca);
            pst.setString(14, quantity_wfca);
            pst.setString(15, rate_wfca);
            pst.setString(16, txt_value_WFCA.getText());
            pst.setString(17, txt_totalvalue_WFCA.getText());
            pst.executeUpdate();
            System.out.println(":::::");

        } catch (Exception e) {
            System.out.println(e);
        }
        updateTableWFBA();

    }//GEN-LAST:event_bAddToWFBAReport1ActionPerformed

    private void rDate2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rDate2ActionPerformed
        orderCondition = " LC_Date ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rDate2ActionPerformed

    private void rBuyer2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rBuyer2ActionPerformed
        orderCondition = " Buyer ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rBuyer2ActionPerformed

    private void rPi2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rPi2ActionPerformed
        orderCondition = " PI_No ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rPi2ActionPerformed

    private void rCount2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rCount2ActionPerformed
        orderCondition = " Count ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rCount2ActionPerformed

    private void rRate2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRate2ActionPerformed
        orderCondition = " Rate ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rRate2ActionPerformed

    private void rQuality2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rQuality2ActionPerformed
        orderCondition = " Quantity ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rQuality2ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        dispose();
        new DRPFrame().setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        dispose();
        new WFCAFrame().setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1MouseClicked

    private void table_WFBA1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_WFBA1MouseClicked
        // TODO add your handling code here:
        try {
            int row = table_WFBA1.getSelectedRow();
            lcId_wfba = (table_WFBA1.getModel().getValueAt(row, 1).toString());
            count_wfba = (table_WFBA1.getModel().getValueAt(row, 12).toString());
            String sql = "SELECT * FROM wfba"
            + " WHERE LC_ID = '" + lcId_wfba + "' AND Count = '" + count_wfba + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_wfba = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_wfba = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_wfba = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_wfba = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_wfba = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);
                lcSight_wfba = rs.getString("LC_sight_Days");
                //txt_sight.setText(lcSight);
                piNo_wfba = rs.getString("PI_No");
                //txt_pi.setText(piNo);
                 sDocRecDate_wfba= rs.getString("Doc_Rec_Date");
                sBankSUBDate_wfba= rs.getString("Bank_Sub_Date");
                comments_wfba = rs.getString("Comments");
                sMaturityDate_wfba = rs.getString("Maturity_Date");
                LDPB_No_wfba = rs.getString("LDBP_No");
                count_wfba = rs.getString("Count");
                quantity_wfba = rs.getString("Quantity");
                rate_wfba = rs.getString("Rate");
                String value_wfba = rs.getString("Value");
                String totalValue_wfba = rs.getString("Total_Value");
                

                txt_value_WFBA1.setText(value_wfba);
                txt_totalvalue_WFBA1.setText(totalValue_wfba);
                DateChooser_DocRecDate_WFBA1.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_wfba));
                DateChooser_BankSUBDate_WFBA1.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_wfba));
                txt_comments_WFBA1.setText(comments_wfba);

                DateChooser_MaturyDate_WFBA1.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_wfba));
                 
                txt_LDPB_WFBA.setText(LDPB_No_wfba);
                
            }
            System.out.println("after loop mouse click");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_WFBA1MouseClicked

    private void txt_totalvalue_WFBA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalvalue_WFBA1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalvalue_WFBA1ActionPerformed

    private void txt_value_WFBA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_value_WFBA1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_value_WFBA1ActionPerformed

    private void txt_comments_WFBA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_comments_WFBA1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_comments_WFBA1ActionPerformed

    private void table_DRPMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_DRPMouseClicked
        // TODO add your handling code here:
        try {

            int row = table_DRP.getSelectedRow();
            lcId_drp = (table_DRP.getModel().getValueAt(row, 1).toString());
            count_drp = (table_DRP.getModel().getValueAt(row, 15).toString());
            String sql = "SELECT * FROM drp"
            + " WHERE LC_ID = '" + lcId_drp + "' AND Count = '" + count_drp + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_drp = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_drp = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_drp = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_drp = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_drp = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);

                sDocRecDate_drp= rs.getString("Doc_Rec_Date");
                sBankSUBDate_drp= rs.getString("Submission_Date");
                sIssueDate_drp= rs.getString("Issuing_Date_Of_Maturity");
                sMaturityDate_drp = rs.getString("Maturity_Date");
                sPurchaseDate_drp= rs.getString("Purchase_Date");
                sFddDate_drp= rs.getString("Fdd_Date");
                amount_drp = rs.getString("Fdd_Amount");
                sAdjustDate_drp = rs.getString("Adjusted_Date");
                sRealizationDate_drp = rs.getString("Realization_Date");
                LDPB_No_drp = rs.getString("LDBP_No");
                count_drp = rs.getString("Count");
                quantity_drp = rs.getString("Quantity");
                rate_drp = rs.getString("Rate");
                Value_drp = rs.getString("Value");
                total_drp = rs.getString("Total");
                exim_drp = rs.getString("Exim");
                scb_drp = rs.getString("SCB");

                //                DateChooser_DocRecDate_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_drp));
                //                DateChooser_BankSUBDate_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_drp));
                //                DateChooser_Adjust_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sAdjustDate_drp));
                //                DateChooser_FddAdd_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sFddDate_drp));
                //                DateChooser_Issue_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sIssueDate_drp));
                //                DateChooser_MaturyDate_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_drp));
                //                DateChooser_PurchaseDate_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sPurchaseDate_drp));
                //                DateChooser_Realization_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sRealizationDate_drp));

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_DRPMouseClicked

    private void bDeleteFromWFCAReport2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromWFCAReport2ActionPerformed
        try {
            String sql = "DELETE FROM wfba WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lcId_wfba);
            pst.setString(2, count_wfba);
            pst.executeUpdate();
            updateTableWFBA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromWFCAReport2ActionPerformed

    private void bCreateDRPReport2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateDRPReport2ActionPerformed
        ReportDRP rDRP = new ReportDRP();
        try {
            rDRP.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateDRPReport2ActionPerformed

    private void bEditInfo_WFBA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_WFBA1ActionPerformed
        try {
            Date DocRecDate_wfba = DateChooser_DocRecDate_WFBA.getDate();
            Date BankSUBDate_wfba = DateChooser_BankSUBDate_WFBA.getDate();
            Date MaturityDate_wfba = DateChooser_MaturyDate_WFBA.getDate();
            //Date FddAddDate_WFBA = DateChooser_FddAdd_WFBA.getDate();
            Date PurchaseDate_WFBA = DateChooser_PurchaseDate_WFBA.getDate();
            //Date AdjustDate_WFBA = DateChooser_Adjust_WFBA.getDate();
            Date IssueDate_WFBA = DateChooser_Issue_WFBA.getDate();
            //Date RealizationDate_WFBA = DateChooser_Realization_WFBA.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String sDocRecDate_wfb = dateFormat.format(DocRecDate_wfba);
            String sBankSUBDate_wfb = dateFormat.format(BankSUBDate_wfba);
            String sMaturityDate_wfb = dateFormat.format(MaturityDate_wfba);
            //String sFddAddDate_WFBA = dateFormat.format(FddAddDate_WFBA);
            String sPurchaseDate_WFBA = dateFormat.format(PurchaseDate_WFBA);
            //String sAdjustDate_WFBA = dateFormat.format(AdjustDate_WFBA);
            String sIssueDate_WFBA = dateFormat.format(IssueDate_WFBA);
            //String sRealizationDate_WFBA = dateFormat.format(RealizationDate_WFBA);

            String sql = "UPDATE wfba SET Value =  ? , Total_Value = ? , Doc_Rec_Date = ? , "
            + " Bank_Sub_Date = ? , Comments = ? ,  Maturity_Date = ? , LDBP_No = ?  WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_value_WFBA.getText());
            pst.setString(2, txt_totalvalue_WFBA.getText());
            pst.setString(3, sDocRecDate_wfb);
            pst.setString(4, sBankSUBDate_wfb);
            pst.setString(5, txt_comments_WFBA.getText());
            pst.setString(6, sMaturityDate_wfb);
            pst.setString(7, txt_LDPB_WFBA.getText());
            //            pst.setString(8, txt_fddammount_WFBA.getText());
            //            pst.setString(9, sFddAddDate_WFBA);
            //            pst.setString(10, sPurchaseDate_WFBA);
            //            pst.setString(11, sAdjustDate_WFBA);
            //            pst.setString(12, sIssueDate_WFBA);
            //            pst.setString(13, sRealizationDate_WFBA);

            pst.setString(8, lcId_wfba);
            pst.setString(9, count_wfba);
            pst.executeUpdate();
            updateTableWFBA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_WFBA1ActionPerformed

    private void bCreateWFBAReport_WFBA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateWFBAReport_WFBA1ActionPerformed
        ReportWFBA mf = new ReportWFBA();
        //        Date fromDate = jDateChooser1.getDate();
        //        Date toDate = jDateChooser2.getDate();
        //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //        String sFromDate = dateFormat.format(fromDate);
        //        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);

        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateWFBAReport_WFBA1ActionPerformed

    private void bAddToDRPReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAddToDRPReportActionPerformed
        // TODO add your handling code here:
        Date DocRecDate_wfba = DateChooser_DocRecDate_WFBA.getDate();
            Date BankSUBDate_wfba = DateChooser_BankSUBDate_WFBA.getDate();
            Date MaturityDate_wfba = DateChooser_MaturyDate_WFBA.getDate();
            //Date FddDate_WFBA = DateChooser_FddAdd_WFBA.getDate();
            Date PurchaseDate_WFBA = DateChooser_PurchaseDate_WFBA.getDate();
            //Date AdjustDate_WFBA = DateChooser_Adjust_WFBA.getDate();
            Date IssueDate_WFBA = DateChooser_Issue_WFBA.getDate();
            //Date RealizationDate_WFBA = DateChooser_Realization_WFBA.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String sDocRecDate_wfb = dateFormat.format(DocRecDate_wfba);
            String sBankSUBDate_wfb = dateFormat.format(BankSUBDate_wfba);
            String sMaturityDate_wfb = dateFormat.format(MaturityDate_wfba);
            //String sFddDate_WFBA = dateFormat.format(FddDate_WFBA);
            String sPurchaseDate_WFBA = dateFormat.format(PurchaseDate_WFBA);
           // String sAdjustDate_WFBA = dateFormat.format(AdjustDate_WFBA);
            String sIssueDate_WFBA = dateFormat.format(IssueDate_WFBA);
            //String sRealizationDate_WFBA = dateFormat.format(RealizationDate_WFBA);

        //System.out.println(sFromDate+" TO "+sToDate);
        try {
            String sql = "INSERT IGNORE INTO drp (Buyer, LC_ID, LC_Date, Last_Date_Of_Shipment,"
            + " Expiry_Date, Doc_Rec_Date, submission_date , Issuing_Date_Of_Maturity , Maturity_Date, Purchase_Date , Fdd_Date ,Fdd_Amount , Adjusted_Date , Realization_Date , LDBP_No , Count, Quantity, Rate, Value,"
            + " Total , Exim , SCB) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,? ,? ,?)";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, buyer_wfba);
            pst.setString(2, lcId_wfba);
            pst.setString(3, lcDate_wfba);
            pst.setString(4, lastDateOfShipment_wfba);
            pst.setString(5, expiryDate_wfba);
            //if(!(sDocRecDate_wfb==null) && !(sBankSUBDate_wfb==null) && !(sMaturityDate_wfb==null) && !(sFddDate_WFBA==null) && !(sPurchaseDate_WFBA==null) && !(sAdjustDate_WFBA==null) && !(sIssueDate_WFBA==null) && !(sRealizationDate_WFBA==null)){
                pst.setString(6, sDocRecDate_wfb);
                pst.setString(7, sBankSUBDate_wfb);
                pst.setString(8, sIssueDate_WFBA);
                pst.setString(9, sMaturityDate_wfb);
                pst.setString(10, sPurchaseDate_WFBA);
                //pst.setString(11, sFddDate_WFBA);
                //pst.setString(12, txt_fddammount_WFBA.getText());
                //pst.setString(13, sAdjustDate_WFBA);
                //pst.setString(14, sRealizationDate_WFBA);
                //} else{
                //JOptionPane.showMessageDialog(null,"Please Choose all three dates!!!" );
                //}
            pst.setString(11, txt_LDPB_WFBA.getText());
            pst.setString(12, count_wfba);
            pst.setString(13, quantity_wfba);
            pst.setString(14, rate_wfba);
            pst.setString(15, txt_value_WFBA.getText());
            pst.setString(16, txt_totalvalue_WFBA.getText());
            pst.setString(17, txt_exim_WFBA.getText());
            pst.setString(18, txt_SCB_WFBA.getText());
            pst.executeUpdate();
            System.out.println(":::::");

        } catch (Exception e) {
            System.out.println(e);
        }
        updateTableDRP();
    }//GEN-LAST:event_bAddToDRPReportActionPerformed

    private void rDate3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rDate3ActionPerformed
        orderCondition = " LC_Date ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rDate3ActionPerformed

    private void rBuyer3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rBuyer3ActionPerformed
        orderCondition = " Buyer ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rBuyer3ActionPerformed

    private void rPi3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rPi3ActionPerformed
        orderCondition = " PI_No ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rPi3ActionPerformed

    private void rCount3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rCount3ActionPerformed
        orderCondition = " Count ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rCount3ActionPerformed

    private void rRate3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRate3ActionPerformed
        orderCondition = " Rate ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rRate3ActionPerformed

    private void rQuality3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rQuality3ActionPerformed
        orderCondition = " Quantity ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rQuality3ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        dispose();
        new RealizationFrame().setVisible(true);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        dispose();
        new FrameWFBA().setVisible(true);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton9ActionPerformed

    private void table_DRP1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_DRP1MouseClicked
        // TODO add your handling code here:
        try {

            int row = table_DRP.getSelectedRow();
            lcId_DRP = (table_DRP.getModel().getValueAt(row, 1).toString());
            count_DRP = (table_DRP.getModel().getValueAt(row, 15).toString());
            String sql = "SELECT * FROM drp"
            + " WHERE LC_ID = '" + lcId_DRP + "' AND Count = '" + count_DRP + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_DRP = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_DRP = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_DRP = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_DRP = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_DRP = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);

                sDocRecDate_DRP= rs.getString("Doc_Rec_Date");
                sBankSUBDate_DRP= rs.getString("Submission_Date");
                sIssueDate_DRP= rs.getString("Issuing_Date_Of_Maturity");
                sMaturityDate_DRP = rs.getString("Maturity_Date");
                sPurchaseDate_DRP= rs.getString("Purchase_Date");
                sFddDate_DRP= rs.getString("Fdd_Date");
                amount_DRP = rs.getString("Fdd_Amount");
                sAdjustDate_DRP = rs.getString("Adjusted_Date");
                sRealizationDate_DRP = rs.getString("Realization_Date");
                LDPB_No_DRP = rs.getString("LDBP_No");
                count_DRP = rs.getString("Count");
                quantity_DRP = rs.getString("Quantity");
                rate_DRP = rs.getString("Rate");
                Value_DRP = rs.getString("Value");
                total_DRP = rs.getString("Total");
                exim_DRP = rs.getString("Exim");
                scb_DRP = rs.getString("SCB");

                txt_amount_DRP.setText(Value_DRP);
                txt_totalvalue_DRP.setText(total_DRP);
                txt_fddammount_DRP.setText(amount_DRP);
                txt_LDPB_DRP.setText(LDPB_No_DRP);

                //txt_amount_DRP.setText(Value_DRP);
                DateChooser_DocRecDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_DRP));
                DateChooser_BankSUBDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_DRP));
                DateChooser_PurchaseDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sPurchaseDate_DRP));
                DateChooser_FddAdd_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sFddDate_DRP));
                DateChooser_Adjust_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sAdjustDate_DRP));
                DateChooser_Realization_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sRealizationDate_DRP));

                DateChooser_Issue_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sIssueDate_DRP));
                //txt_comments_DRP.setText(comments_DRP);

                DateChooser_MaturyDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_DRP));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }

    }//GEN-LAST:event_table_DRP1MouseClicked

    private void txt_totalvalue_DRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalvalue_DRPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalvalue_DRPActionPerformed

    private void txt_amount_DRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_amount_DRPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_amount_DRPActionPerformed

    private void txt_fddammount_DRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_fddammount_DRPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_fddammount_DRPActionPerformed

    private void table_RealizationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_RealizationMouseClicked
        // TODO add your handling code here:
        try {
            System.out.println("ok1");
            int row = table_Realization.getSelectedRow();
            lcId_DRP = (table_Realization.getModel().getValueAt(row, 1).toString());
            count_DRP = (table_Realization.getModel().getValueAt(row, 15).toString());
            String sql = "SELECT * FROM realization"
            + " WHERE LC_ID = '" + lcId_RE + "' AND Count = '" + count_RE + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_RE = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_RE = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_RE = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_RE = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_RE = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);

                sDocRecDate_RE= rs.getString("Doc_Rec_Date");
                sBankSUBDate_RE= rs.getString("Submission_Date");
                sIssueDate_RE= rs.getString("Issuing_Date_Maturity");
                sMaturityDate_RE = rs.getString("Maturity_Date");
                sPurchaseDate_RE= rs.getString("Purchase_Date");
                sFddDate_RE= rs.getString("Fdd_Issue_Date");
                amount_RE = rs.getString("Fdd_Amount");
                sAdjustDate_RE = rs.getString("Adjusted_Date");
                sRealizationDate_RE = rs.getString("Realization_Date");
                LDPB_No_RE = rs.getString("LDBP_No");
                count_RE = rs.getString("Count");
                quantity_RE = rs.getString("Quantity");
                rate_RE = rs.getString("Rate");
                Value_RE = rs.getString("Value");
                total_RE = rs.getString("Total_Value");
                exim_RE = rs.getString("Exim");
                scb_RE = rs.getString("SCB");

                //                txt_amount_RE.setText(Value_RE);
                //                txt_totalvalue_RE.setText(total_RE);
                //                txt_fddammount_RE.setText(amount_RE);
                //                txt_LDPB_RE.setText(LDPB_No_RE);

                //txt_amount_DRP.setText(Value_DRP);
                //                DateChooser_DocRecDate_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_RE));
                //                DateChooser_BankSUBDate_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_RE));
                DateChooser_FddAdd_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sFddDate_RE));
                DateChooser_PurchaseDate_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sPurchaseDate_RE));

                DateChooser_Adjust_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sAdjustDate_RE));
                DateChooser_Issue_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sIssueDate_RE));
                DateChooser_Realization_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sRealizationDate_RE));

                //txt_comments_DRP.setText(comments_DRP);

                //                DateChooser_MaturyDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_RE));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }

    }//GEN-LAST:event_table_RealizationMouseClicked

    private void bCreateDRPReport1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateDRPReport1ActionPerformed
        ReportDRP mf = new ReportDRP();
        //        Date fromDate = jDateChooser1.getDate();
        //        Date toDate = jDateChooser2.getDate();
        //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //        String sFromDate = dateFormat.format(fromDate);
        //        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);

        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateDRPReport1ActionPerformed

    private void bAddToRealizationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAddToRealizationActionPerformed
        // TODO add your handling code here:
        Date DocRecDate_DRP = DateChooser_DocRecDate_DRP.getDate();
        Date BankSUBDate_DRP = DateChooser_BankSUBDate_DRP.getDate();
        Date MaturityDate_DRP= DateChooser_MaturyDate_DRP.getDate();
        Date FddAddDate_DRP = DateChooser_FddAdd_DRP.getDate();
        Date PurchaseDate_DRP = DateChooser_PurchaseDate_DRP.getDate();
        Date AdjustDate_DRP = DateChooser_Adjust_DRP.getDate();
        Date IssueDate_DRP = DateChooser_Issue_DRP.getDate();
        Date RealizationDate_DRP = DateChooser_Realization_DRP.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sDocRecDate_DRP = dateFormat.format(DocRecDate_DRP);
        String sBankSUBDate_DRP = dateFormat.format(BankSUBDate_DRP);
        String sMaturityDate_DRP = dateFormat.format(MaturityDate_DRP);
        String sFddAddDate_DRP = dateFormat.format(FddAddDate_DRP);
        String sPurchaseDate_DRP = dateFormat.format(PurchaseDate_DRP);
        String sAdjustDate_DRP = dateFormat.format(AdjustDate_DRP);
        String sIssueDate_DRP = dateFormat.format(IssueDate_DRP);
        String sRealizationDate_DRP = dateFormat.format(RealizationDate_DRP);

        //System.out.println(sFromDate+" TO "+sToDate);
        try {
            String sql = "INSERT IGNORE INTO realization (Buyer, LC_ID, LC_Date, Last_Date_Of_Shipment,"
            + " Expiry_Date, Doc_Rec_Date, submission_date , Issuing_Date_Maturity , Maturity_Date, Purchase_Date , Fdd_Issue_Date ,Fdd_Amount , Adjusted_Date , Realization_Date , LDBP_No , Count, Quantity, Rate, Value,"
            + " Total_value , Exim , SCB) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,? ,? ,?)";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, buyer_DRP);
            pst.setString(2, lcId_DRP);
            pst.setString(3, lcDate_DRP);
            pst.setString(4, lastDateOfShipment_DRP);
            pst.setString(5, expiryDate_DRP);
            //if(!(sDocRecDate_wfb==null) && !(sBankSUBDate_wfb==null) && !(sMaturityDate_wfb==null) && !(sFddDate_WFBA==null) && !(sPurchaseDate_WFBA==null) && !(sAdjustDate_WFBA==null) && !(sIssueDate_WFBA==null) && !(sRealizationDate_WFBA==null)){
                pst.setString(6, sDocRecDate_DRP);
                pst.setString(7, sBankSUBDate_DRP);
                pst.setString(8, sIssueDate_DRP);
                pst.setString(9, sMaturityDate_DRP);
                pst.setString(10, sPurchaseDate_DRP);
                pst.setString(11, sFddAddDate_DRP);
                pst.setString(12, txt_fddammount_DRP.getText());
                pst.setString(13, sAdjustDate_DRP);
                pst.setString(14, sRealizationDate_DRP);
                //} else{
                //JOptionPane.showMessageDialog(null,"Please Choose all three dates!!!" );
                //}
            pst.setString(15, txt_LDPB_DRP.getText());
            pst.setString(16, count_DRP);
            pst.setString(17, quantity_DRP);
            pst.setString(18, rate_DRP);
            pst.setString(19, txt_amount_DRP.getText());
            pst.setString(20, txt_totalvalue_DRP.getText());
            pst.setString(21, txt_exim_DRP.getText());
            pst.setString(22, txt_SCB_DRP.getText());
            pst.executeUpdate();
            System.out.println(":::::");

        } catch (Exception e) {
            System.out.println(e);
        }
        updateTableRealization();
    }//GEN-LAST:event_bAddToRealizationActionPerformed

    private void bDeleteFromDRPReport_DRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromDRPReport_DRPActionPerformed
        try {
            String sql = "DELETE FROM drp WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lcId_DRP);
            pst.setString(2, count_DRP);
            pst.executeUpdate();
            updateTableDRP();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromDRPReport_DRPActionPerformed

    private void bEditInfo_DRPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_DRPActionPerformed
        try {
            Date DocRecDate_DRP = DateChooser_DocRecDate_DRP.getDate();
            Date BankSUBDate_DRP = DateChooser_BankSUBDate_DRP.getDate();
            Date MaturityDate_DRP= DateChooser_MaturyDate_DRP.getDate();
            Date FddAddDate_DRP = DateChooser_FddAdd_DRP.getDate();
            Date PurchaseDate_DRP = DateChooser_PurchaseDate_DRP.getDate();
            Date AdjustDate_DRP = DateChooser_Adjust_DRP.getDate();
            Date IssueDate_DRP = DateChooser_Issue_DRP.getDate();
            Date RealizationDate_DRP = DateChooser_Realization_DRP.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String sDocRecDate_DRP = dateFormat.format(DocRecDate_DRP);
            String sBankSUBDate_DRP = dateFormat.format(BankSUBDate_DRP);
            String sMaturityDate_DRP = dateFormat.format(MaturityDate_DRP);
            String sFddAddDate_DRP = dateFormat.format(FddAddDate_DRP);
            String sPurchaseDate_DRP = dateFormat.format(PurchaseDate_DRP);
            String sAdjustDate_DRP = dateFormat.format(AdjustDate_DRP);
            String sIssueDate_DRP = dateFormat.format(IssueDate_DRP);
            String sRealizationDate_DRP = dateFormat.format(RealizationDate_DRP);

            String sql = "UPDATE drp SET Value =  ? , Total_Value = ? , Doc_Rec_Date = ? , "
            + " Bank_Sub_Date = ? ,  Maturity_Date = ? , LDBP_No = ? , Fdd_Amount = ? , Fdd_Date = ? , Purchase_Date = ? , Adjusted_Date = ? , Issuing_Date_Of_Maturity = ? , Realization_Date = ? , Exim = ? , SCB = ? WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_amount_DRP.getText());
            pst.setString(2, txt_totalvalue_DRP.getText());
            pst.setString(3, sDocRecDate_DRP);
            pst.setString(4, sBankSUBDate_DRP);
            //pst.setString(5, txt_comments_DRP.getText());
            pst.setString(5, sMaturityDate_DRP);
            pst.setString(6, txt_LDPB_DRP.getText());
            pst.setString(7, txt_fddammount_DRP.getText());
            pst.setString(8, sFddAddDate_DRP);
            pst.setString(9, sPurchaseDate_DRP);
            pst.setString(10, sAdjustDate_DRP);
            pst.setString(11, sIssueDate_DRP);
            pst.setString(12, sRealizationDate_DRP);
            pst.setString(13, txt_exim_DRP.getText());
            pst.setString(14, txt_SCB_DRP.getText());

            pst.setString(15, lcId_DRP);
            pst.setString(16, count_DRP);
            pst.executeUpdate();
            updateTableDRP();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_DRPActionPerformed

    private void bEditInfo_REActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_REActionPerformed
        try {
            //            Date DocRecDate_RE = DateChooser_DocRecDate_RE.getDate();
            //            Date BankSUBDate_RE = DateChooser_BankSUBDate_RE.getDate();
            //            Date MaturityDate_RE= DateChooser_MaturyDate_RE.getDate();
            Date FddAddDate_RE = DateChooser_FddAdd_RE.getDate();
            Date PurchaseDate_RE = DateChooser_PurchaseDate_RE.getDate();
            Date AdjustDate_RE = DateChooser_Adjust_RE.getDate();
            Date IssueDate_RE = DateChooser_Issue_RE.getDate();
            Date RealizationDate_RE = DateChooser_Realization_RE.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            //            String sDocRecDate_RE = dateFormat.format(DocRecDate_RE);
            //            String sBankSUBDate_RE = dateFormat.format(BankSUBDate_RE);
            //            String sMaturityDate_RE = dateFormat.format(MaturityDate_RE);
            String sFAddDate_RE = dateFormat.format(FddAddDate_RE);
            String sPurchaseDate_RE = dateFormat.format(PurchaseDate_RE);
            String sAdjustDate_RE = dateFormat.format(AdjustDate_RE);
            String sIssueDate_RE = dateFormat.format(IssueDate_RE);
            String sRealizationDate_RE = dateFormat.format(RealizationDate_RE);

            String sql = "UPDATE realization SET  Fdd_Date = ? , Purchase_Date = ? , Adjusted_Date = ? , Issuing_Date_Of_Maturity = ? , Realization_Date = ?  WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            //pst.setString(1, txt_amount_DRP.getText());
            //pst.setString(2, txt_totalvalue_DRP.getText());
            //pst.setString(3, sDocRecDate_DRP);
            //pst.setString(4, sBankSUBDate_DRP);
            //pst.setString(5, txt_comments_DRP.getText());
            //pst.setString(5, sMaturityDate_DRP);
            //pst.setString(6, txt_LDPB_DRP.getText());
            //pst.setString(7, txt_fddammount_DRP.getText());
            pst.setString(8, sFAddDate_RE);
            pst.setString(9, sPurchaseDate_DRP);
            pst.setString(10, sAdjustDate_DRP);
            pst.setString(11, sIssueDate_DRP);
            pst.setString(12, sRealizationDate_DRP);
            //pst.setString(13, txt_exim_DRP.getText());
            //pst.setString(14, txt_SCB_DRP.getText());

            //pst.setString(15, lcId_DRP);
            //pst.setString(16, count_DRP);
            //pst.executeUpdate();
            updateTableRealization();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_REActionPerformed

    private void bCreateREeportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateREeportActionPerformed
        ReportRealization mf = new ReportRealization();
        //        Date fromDate = jDateChooser1.getDate();
        //        Date toDate = jDateChooser2.getDate();
        //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //        String sFromDate = dateFormat.format(fromDate);
        //        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);

        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateREeportActionPerformed

    private void bDeleteFromrealizationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromrealizationActionPerformed
        try {
            String sql = "DELETE FROM realization WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lcId_RE);
            pst.setString(2, count_RE);
            pst.executeUpdate();
            updateTableRealization();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromrealizationActionPerformed

    private void rQuality4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rQuality4ActionPerformed
        orderCondition = " Quantity ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rQuality4ActionPerformed

    private void rRate4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRate4ActionPerformed
        orderCondition = " Rate ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rRate4ActionPerformed

    private void rCount4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rCount4ActionPerformed
        orderCondition = " Count ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rCount4ActionPerformed

    private void rPi4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rPi4ActionPerformed
        orderCondition = " PI_No ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rPi4ActionPerformed

    private void rBuyer4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rBuyer4ActionPerformed
        orderCondition = " Buyer ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rBuyer4ActionPerformed

    private void rDate4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rDate4ActionPerformed
        orderCondition = " LC_Date ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rDate4ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        dispose();
        new DRPFrame().setVisible(true);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void table_DRP2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_DRP2MouseClicked
        // TODO add your handling code here:
        try {

            int row = table_DRP2.getSelectedRow();
            lcId_DRP = (table_DRP2.getModel().getValueAt(row, 1).toString());
            count_DRP = (table_DRP2.getModel().getValueAt(row, 15).toString());
            String sql = "SELECT * FROM drp"
            + " WHERE LC_ID = '" + lcId_DRP + "' AND Count = '" + count_DRP + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_DRP = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_DRP = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_DRP = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_DRP = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_DRP = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);

                sDocRecDate_DRP= rs.getString("Doc_Rec_Date");
                sBankSUBDate_DRP= rs.getString("Submission_Date");
                sIssueDate_DRP= rs.getString("Issuing_Date_Of_Maturity");
                sMaturityDate_DRP = rs.getString("Maturity_Date");
                sPurchaseDate_DRP= rs.getString("Purchase_Date");
                sFddDate_DRP= rs.getString("Fdd_Date");
                amount_DRP = rs.getString("Fdd_Amount");
                sAdjustDate_DRP = rs.getString("Adjusted_Date");
                sRealizationDate_DRP = rs.getString("Realization_Date");
                LDPB_No_DRP = rs.getString("LDBP_No");
                count_DRP = rs.getString("Count");
                quantity_DRP = rs.getString("Quantity");
                rate_DRP = rs.getString("Rate");
                Value_DRP = rs.getString("Value");
                total_DRP = rs.getString("Total");
                exim_DRP = rs.getString("Exim");
                scb_DRP = rs.getString("SCB");

                txt_amount_DRP.setText(Value_DRP);
                txt_totalvalue_DRP.setText(total_DRP);
                txt_fddammount_DRP.setText(amount_DRP);
                txt_LDPB_DRP.setText(LDPB_No_DRP);

                //txt_amount_DRP.setText(Value_DRP);
                DateChooser_DocRecDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_DRP));
                DateChooser_BankSUBDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_DRP));
                DateChooser_PurchaseDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sPurchaseDate_DRP));
                DateChooser_FddAdd_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sFddDate_DRP));
                DateChooser_Adjust_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sAdjustDate_DRP));
                DateChooser_Realization_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sRealizationDate_DRP));

                DateChooser_Issue_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sIssueDate_DRP));
                //txt_comments_DRP.setText(comments_DRP);

                DateChooser_MaturyDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_DRP));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_DRP2MouseClicked

    private void txt_amount_DRP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_amount_DRP1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_amount_DRP1ActionPerformed

    private void txt_totalvalue_DRP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalvalue_DRP1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalvalue_DRP1ActionPerformed

    private void txt_fddammount_DRP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_fddammount_DRP1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_fddammount_DRP1ActionPerformed

    private void bEditInfo_DRP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_DRP1ActionPerformed
        try {
            Date DocRecDate_DRP = DateChooser_DocRecDate_DRP.getDate();
            Date BankSUBDate_DRP = DateChooser_BankSUBDate_DRP.getDate();
            Date MaturityDate_DRP= DateChooser_MaturyDate_DRP.getDate();
            Date FddAddDate_DRP = DateChooser_FddAdd_DRP.getDate();
            Date PurchaseDate_DRP = DateChooser_PurchaseDate_DRP.getDate();
            Date AdjustDate_DRP = DateChooser_Adjust_DRP.getDate();
            Date IssueDate_DRP = DateChooser_Issue_DRP.getDate();
            Date RealizationDate_DRP = DateChooser_Realization_DRP.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String sDocRecDate_DRP = dateFormat.format(DocRecDate_DRP);
            String sBankSUBDate_DRP = dateFormat.format(BankSUBDate_DRP);
            String sMaturityDate_DRP = dateFormat.format(MaturityDate_DRP);
            String sFddAddDate_DRP = dateFormat.format(FddAddDate_DRP);
            String sPurchaseDate_DRP = dateFormat.format(PurchaseDate_DRP);
            String sAdjustDate_DRP = dateFormat.format(AdjustDate_DRP);
            String sIssueDate_DRP = dateFormat.format(IssueDate_DRP);
            String sRealizationDate_DRP = dateFormat.format(RealizationDate_DRP);

            String sql = "UPDATE drp SET Value =  ? , Total_Value = ? , Doc_Rec_Date = ? , "
            + " Bank_Sub_Date = ? ,  Maturity_Date = ? , LDBP_No = ? , Fdd_Amount = ? , Fdd_Date = ? , Purchase_Date = ? , Adjusted_Date = ? , Issuing_Date_Of_Maturity = ? , Realization_Date = ? , Exim = ? , SCB = ? WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_amount_DRP.getText());
            pst.setString(2, txt_totalvalue_DRP.getText());
            pst.setString(3, sDocRecDate_DRP);
            pst.setString(4, sBankSUBDate_DRP);
            //pst.setString(5, txt_comments_DRP.getText());
            pst.setString(5, sMaturityDate_DRP);
            pst.setString(6, txt_LDPB_DRP.getText());
            pst.setString(7, txt_fddammount_DRP.getText());
            pst.setString(8, sFddAddDate_DRP);
            pst.setString(9, sPurchaseDate_DRP);
            pst.setString(10, sAdjustDate_DRP);
            pst.setString(11, sIssueDate_DRP);
            pst.setString(12, sRealizationDate_DRP);
            pst.setString(13, txt_exim_DRP.getText());
            pst.setString(14, txt_SCB_DRP.getText());

            pst.setString(15, lcId_DRP);
            pst.setString(16, count_DRP);
            pst.executeUpdate();
            updateTableDRP();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_DRP1ActionPerformed

    private void bCreateDRPReport3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateDRPReport3ActionPerformed
        ReportDRP mf = new ReportDRP();
        //        Date fromDate = jDateChooser1.getDate();
        //        Date toDate = jDateChooser2.getDate();
        //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //        String sFromDate = dateFormat.format(fromDate);
        //        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);

        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateDRPReport3ActionPerformed

    private void bAddToPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAddToPurchaseActionPerformed
        // TODO add your handling code here:
        Date DocRecDate_DRP = DateChooser_DocRecDate_DRP.getDate();
        Date BankSUBDate_DRP = DateChooser_BankSUBDate_DRP.getDate();
        Date MaturityDate_DRP= DateChooser_MaturyDate_DRP.getDate();
        Date FddAddDate_DRP = DateChooser_FddAdd_DRP.getDate();
        Date PurchaseDate_DRP = DateChooser_PurchaseDate_DRP.getDate();
        Date AdjustDate_DRP = DateChooser_Adjust_DRP.getDate();
        Date IssueDate_DRP = DateChooser_Issue_DRP.getDate();
        Date RealizationDate_DRP = DateChooser_Realization_DRP.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sDocRecDate_DRP = dateFormat.format(DocRecDate_DRP);
        String sBankSUBDate_DRP = dateFormat.format(BankSUBDate_DRP);
        String sMaturityDate_DRP = dateFormat.format(MaturityDate_DRP);
        String sFddAddDate_DRP = dateFormat.format(FddAddDate_DRP);
        String sPurchaseDate_DRP = dateFormat.format(PurchaseDate_DRP);
        String sAdjustDate_DRP = dateFormat.format(AdjustDate_DRP);
        String sIssueDate_DRP = dateFormat.format(IssueDate_DRP);
        String sRealizationDate_DRP = dateFormat.format(RealizationDate_DRP);

        //System.out.println(sFromDate+" TO "+sToDate);
        try {
            String sql = "INSERT IGNORE INTO purchasereport (Buyer, LC_ID, LC_Date, Last_Date_Of_Shipment,"
            + " Expiry_Date, Doc_Rec_Date, submission_date , Issuing_Date_Maturity , Maturity_Date, Purchase_Date , Fdd_Issue_Date ,Fdd_Amount , Adjusted_Date , Realization_Date , LDBP_No , Count, Quantity, Rate, Value,"
            + " Total_value , Exim , SCB) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,? ,? ,?)";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, buyer_DRP);
            pst.setString(2, lcId_DRP);
            pst.setString(3, lcDate_DRP);
            pst.setString(4, lastDateOfShipment_DRP);
            pst.setString(5, expiryDate_DRP);
            //if(!(sDocRecDate_wfb==null) && !(sBankSUBDate_wfb==null) && !(sMaturityDate_wfb==null) && !(sFddDate_WFBA==null) && !(sPurchaseDate_WFBA==null) && !(sAdjustDate_WFBA==null) && !(sIssueDate_WFBA==null) && !(sRealizationDate_WFBA==null)){
                pst.setString(6, sDocRecDate_DRP);
                pst.setString(7, sBankSUBDate_DRP);
                pst.setString(8, sIssueDate_DRP);
                pst.setString(9, sMaturityDate_DRP);
                pst.setString(10, sPurchaseDate_DRP);
                pst.setString(11, sFddAddDate_DRP);
                pst.setString(12, txt_fddammount_DRP.getText());
                pst.setString(13, sAdjustDate_DRP);
                pst.setString(14, sRealizationDate_DRP);
                //} else{
                //JOptionPane.showMessageDialog(null,"Please Choose all three dates!!!" );
                //}
            pst.setString(15, txt_LDPB_DRP.getText());
            pst.setString(16, count_DRP);
            pst.setString(17, quantity_DRP);
            pst.setString(18, rate_DRP);
            pst.setString(19, txt_amount_DRP.getText());
            pst.setString(20, txt_totalvalue_DRP.getText());
            pst.setString(21, txt_exim_DRP.getText());
            pst.setString(22, txt_SCB_DRP.getText());
            pst.executeUpdate();
            System.out.println(":::::");

        } catch (Exception e) {
            System.out.println(e);
        }
        updateTablePurchase();
    }//GEN-LAST:event_bAddToPurchaseActionPerformed

    private void bDeleteFromDRPReport_DRP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromDRPReport_DRP1ActionPerformed
        try {
            String sql = "DELETE FROM drp WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lcId_DRP);
            pst.setString(2, count_DRP);
            pst.executeUpdate();
            updateTableDRP();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromDRPReport_DRP1ActionPerformed

    private void table_PurchaseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_PurchaseMouseClicked
        // TODO add your handling code here:
        try {
            System.out.println("ok1");
            int row = table_Purchase.getSelectedRow();
            lcId_DRP = (table_Purchase.getModel().getValueAt(row, 1).toString());
            count_DRP = (table_Purchase.getModel().getValueAt(row, 15).toString());
            String sql = "SELECT * FROM realization"
            + " WHERE LC_ID = '" + lcId_PU + "' AND Count = '" + count_PU + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_PU = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_PU = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_PU = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_PU = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_PU = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);

                sDocRecDate_PU= rs.getString("Doc_Rec_Date");
                sBankSUBDate_PU= rs.getString("Submission_Date");
                sIssueDate_PU= rs.getString("Issuing_Date_Maturity");
                sMaturityDate_PU = rs.getString("Maturity_Date");
                sPurchaseDate_PU= rs.getString("Purchase_Date");
                sFddDate_PU= rs.getString("Fdd_Issue_Date");
                amount_PU = rs.getString("Fdd_Amount");
                sAdjustDate_PU = rs.getString("Adjusted_Date");
                sRealizationDate_PU = rs.getString("Realization_Date");
                LDPB_No_PU = rs.getString("LDBP_No");
                count_PU = rs.getString("Count");
                quantity_PU = rs.getString("Quantity");
                rate_PU = rs.getString("Rate");
                Value_PU = rs.getString("Value");
                total_PU = rs.getString("Total_Value");
                exim_PU = rs.getString("Exim");
                scb_PU = rs.getString("SCB");

                //                txt_amount_RE.setText(Value_RE);
                //                txt_totalvalue_RE.setText(total_RE);
                //                txt_fddammount_RE.setText(amount_RE);
                //                txt_LDPB_RE.setText(LDPB_No_RE);

                //txt_amount_DRP.setText(Value_DRP);
                //                DateChooser_DocRecDate_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_RE));
                //                DateChooser_BankSUBDate_RE.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_RE));
                DateChooser_FddAdd_PU.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sFddDate_PU));
                DateChooser_PurchaseDate_PU.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sPurchaseDate_PU));

                DateChooser_Adjust_PU.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sAdjustDate_PU));
                DateChooser_Issue_PU.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sIssueDate_PU));
                DateChooser_Realization_PU.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sRealizationDate_PU));

                //txt_comments_DRP.setText(comments_DRP);

                //                DateChooser_MaturyDate_DRP.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_RE));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_PurchaseMouseClicked

    private void bEditInfo_RE1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_RE1ActionPerformed
        try {
            //            Date DocRecDate_RE = DateChooser_DocRecDate_RE.getDate();
            //            Date BankSUBDate_RE = DateChooser_BankSUBDate_RE.getDate();
            //            Date MaturityDate_RE= DateChooser_MaturyDate_RE.getDate();
            Date FddAddDate_PU = DateChooser_FddAdd_PU.getDate();
            Date PurchaseDate_PU = DateChooser_PurchaseDate_PU.getDate();
            Date AdjustDate_PU = DateChooser_Adjust_PU.getDate();
            Date IssueDate_PU = DateChooser_Issue_PU.getDate();
            Date RealizationDate_PU = DateChooser_Realization_PU.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            //            String sDocRecDate_RE = dateFormat.format(DocRecDate_RE);
            //            String sBankSUBDate_RE = dateFormat.format(BankSUBDate_RE);
            //            String sMaturityDate_RE = dateFormat.format(MaturityDate_RE);
            String sFAddDate_PU = dateFormat.format(FddAddDate_PU);
            String sPurchaseDate_PU = dateFormat.format(PurchaseDate_PU);
            String sAdjustDate_PU = dateFormat.format(AdjustDate_PU);
            String sIssueDate_PU = dateFormat.format(IssueDate_PU);
            String sRealizationDate_PU = dateFormat.format(RealizationDate_PU);

            String sql = "UPDATE purchasereport SET  Fdd_Date = ? , Purchase_Date = ? , Adjusted_Date = ? , Issuing_Date_Of_Maturity = ? , Realization_Date = ?  WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            //pst.setString(1, txt_amount_DRP.getText());
            //pst.setString(2, txt_totalvalue_DRP.getText());
            //pst.setString(3, sDocRecDate_DRP);
            //pst.setString(4, sBankSUBDate_DRP);
            //pst.setString(5, txt_comments_DRP.getText());
            //pst.setString(5, sMaturityDate_DRP);
            //pst.setString(6, txt_LDPB_DRP.getText());
            //pst.setString(7, txt_fddammount_DRP.getText());
            pst.setString(8, sFAddDate_PU);
            pst.setString(9, sPurchaseDate_DRP);
            pst.setString(10, sAdjustDate_DRP);
            pst.setString(11, sIssueDate_DRP);
            pst.setString(12, sRealizationDate_DRP);
            //pst.setString(13, txt_exim_DRP.getText());
            //pst.setString(14, txt_SCB_DRP.getText());

            //pst.setString(15, lcId_DRP);
            //pst.setString(16, count_DRP);
            //pst.executeUpdate();
            updateTablePurchase();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_RE1ActionPerformed

    private void bDeleteFromrealization1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromrealization1ActionPerformed
        try {
            String sql = "DELETE FROM purchasereport WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lcId_PU);
            pst.setString(2, count_PU);
            pst.executeUpdate();
            updateTablePurchase();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromrealization1ActionPerformed

    private void bCreateREeport1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateREeport1ActionPerformed
        ReportPurchase mf = new ReportPurchase();
        //        Date fromDate = jDateChooser1.getDate();
        //        Date toDate = jDateChooser2.getDate();
        //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //        String sFromDate = dateFormat.format(fromDate);
        //        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);

        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateREeport1ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        dispose();
        new DRPFrame().setVisible(true);
    }//GEN-LAST:event_jButton11ActionPerformed

    private void rDate5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rDate5ActionPerformed
        orderCondition = " LC_Date ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rDate5ActionPerformed

    private void rBuyer5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rBuyer5ActionPerformed
        orderCondition = " Buyer ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rBuyer5ActionPerformed

    private void rPi5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rPi5ActionPerformed
        orderCondition = " PI_No ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rPi5ActionPerformed

    private void rCount5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rCount5ActionPerformed
        orderCondition = " Count ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rCount5ActionPerformed

    private void rRate5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRate5ActionPerformed
        orderCondition = " Rate ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rRate5ActionPerformed

    private void rQuality5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rQuality5ActionPerformed
        orderCondition = " Quantity ASC " ;
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rQuality5ActionPerformed

    private void LC_previewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LC_previewActionPerformed
        Date from_PI = DateChooser_from.getDate();
        Date to_PI = DateChooser_to.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        sfrom_LC = dateFormat.format(from_PI);
        sto_LC = dateFormat.format(to_PI);
        sqlQuery = "SELECT b.name, lc.lc_id,lc.issue_date,lc.shipment_date, lc.expiry_date, "
        + " lc.sight_days, lpr.pi_id, g.description, g.quantity, g.unit_price "
        + " FROM letter_of_credit AS lc "
        + " JOIN buyer AS b ON b.buyer_id = lc.buyer_id "
        + " JOIN lc_pi_relation AS lpr ON lpr.lc_id=lc.lc_id "
        + " JOIN goods AS g ON g.pi_id=lpr.pi_id "
        + " WHERE lc.issue_date BETWEEN CAST( '"+sfrom_LC +"' AS DATE) AND CAST( '"+sto_LC+"'  AS DATE) ";
        updateLCTable();
    }//GEN-LAST:event_LC_previewActionPerformed

    private void bcreateLC_ReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcreateLC_ReportActionPerformed
        ReportLCRegister rpr = new ReportLCRegister();
        Date from_LC = DateChooser_from.getDate();
        Date to_LC = DateChooser_to.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        sfrom_LC = dateFormat.format(from_LC);
        sto_LC = dateFormat.format(to_LC);
        sqlQuery = "SELECT b.name,p.pi_id,p.pi_date,p.offer_validity,p.payment_period, "
        + " g.description,g.quantity,g.unit_price FROM proforma_invoice AS p "
        + " JOIN buyer AS b ON b.buyer_id = p.buyer_id "
        + " JOIN goods AS g ON g.pi_id = p.pi_id "
        + " WHERE p.pi_date BETWEEN CAST( '"+sfrom_LC +"' AS DATE) AND CAST( '"+sto_LC+"'  AS DATE) ";
        try{
            rpr.initiate(ordercondition,sfrom_LC,sto_LC);
        }catch(Exception ex){

        }
    }//GEN-LAST:event_bcreateLC_ReportActionPerformed

    private void PI_previewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PI_previewActionPerformed
        Date from_PI = DateChooser_from.getDate();
        Date to_PI = DateChooser_to.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        sfrom_PI = dateFormat.format(from_PI);
        sto_PI = dateFormat.format(to_PI);
        sqlQuery = "SELECT b.name,p.pi_id,p.pi_date,p.offer_validity,p.payment_period, "
        + " g.description,g.quantity,g.unit_price FROM proforma_invoice AS p "
        + " JOIN buyer AS b ON b.buyer_id = p.buyer_id "
        + " JOIN goods AS g ON g.pi_id = p.pi_id "
        + " WHERE p.pi_date BETWEEN CAST( '"+sfrom_PI +"' AS DATE) AND CAST( '"+sto_PI+"'  AS DATE) ";
        updatePITable();
    }//GEN-LAST:event_PI_previewActionPerformed

    private void bcreatePI_ReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcreatePI_ReportActionPerformed
        ReportPIRegister rpr = new ReportPIRegister();
        Date from_PI = DateChooser_from.getDate();
        Date to_PI = DateChooser_to.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        sfrom_PI = dateFormat.format(from_PI);
        sto_PI = dateFormat.format(to_PI);
        sqlQuery = "SELECT b.name,p.pi_id,p.pi_date,p.offer_validity,p.payment_period, "
        + " g.description,g.quantity,g.unit_price FROM proforma_invoice AS p "
        + " JOIN buyer AS b ON b.buyer_id = p.buyer_id "
        + " JOIN goods AS g ON g.pi_id = p.pi_id "
        + " WHERE p.pi_date BETWEEN CAST( '"+sfrom_PI +"' AS DATE) AND CAST( '"+sto_PI+"'  AS DATE) ";
        try{
            rpr.initiate(ordercondition,sfrom_PI,sto_PI);
        }catch(Exception ex){

        }
    }//GEN-LAST:event_bcreatePI_ReportActionPerformed
    
    
     private static void createAndShowGUI() {
        //Create and set up the window.
        primarydatatable frame = new primarydatatable();
        frame.setDefaultCloseOperation(primarydatatable.EXIT_ON_CLOSE);


       // Container c = frame.getContentPane();
//        frame.getContentPane();
      //  c.setBackground(Color.YELLOW);
        // adjust to need.
        Dimension d = new Dimension(400, 40);
        //c.setPreferredSize(d);

        //Add the ubiquitous "Hello World" label.
        //JLabel label = new JLabel("Hello World");
        //frame.getContentPane().add(label);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        frame.setSize(height, width);
        frame.setSize(d);
      //  LOG.info(height + "\t\t\t" + width);
        //Display the window.
        //frame.pack();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frames_in_tab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frames_in_tab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frames_in_tab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frames_in_tab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frames_in_tab();
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser DateChooser_Adjust_DRP;
    private com.toedter.calendar.JDateChooser DateChooser_Adjust_DRP1;
    private com.toedter.calendar.JDateChooser DateChooser_Adjust_PU;
    private com.toedter.calendar.JDateChooser DateChooser_Adjust_RE;
    private com.toedter.calendar.JDateChooser DateChooser_BankSUBDate_DRP;
    private com.toedter.calendar.JDateChooser DateChooser_BankSUBDate_DRP1;
    private com.toedter.calendar.JDateChooser DateChooser_BankSUBDate_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_BankSUBDate_WFBA1;
    private com.toedter.calendar.JDateChooser DateChooser_BankSUBDate_WFCA;
    private com.toedter.calendar.JDateChooser DateChooser_DelComDate_WFCA;
    private com.toedter.calendar.JDateChooser DateChooser_DelComDate_WFCA1;
    private com.toedter.calendar.JDateChooser DateChooser_DocRecDate_DRP;
    private com.toedter.calendar.JDateChooser DateChooser_DocRecDate_DRP1;
    private com.toedter.calendar.JDateChooser DateChooser_DocRecDate_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_DocRecDate_WFBA1;
    private com.toedter.calendar.JDateChooser DateChooser_DocRecDate_WFCA;
    private com.toedter.calendar.JDateChooser DateChooser_DocumentHandOverDate_WFCA;
    private com.toedter.calendar.JDateChooser DateChooser_DocumentHandOverDate_WFCA1;
    private com.toedter.calendar.JDateChooser DateChooser_FddAdd_DRP;
    private com.toedter.calendar.JDateChooser DateChooser_FddAdd_DRP1;
    private com.toedter.calendar.JDateChooser DateChooser_FddAdd_PU;
    private com.toedter.calendar.JDateChooser DateChooser_FddAdd_RE;
    private com.toedter.calendar.JDateChooser DateChooser_Issue_DRP;
    private com.toedter.calendar.JDateChooser DateChooser_Issue_DRP1;
    private com.toedter.calendar.JDateChooser DateChooser_Issue_PU;
    private com.toedter.calendar.JDateChooser DateChooser_Issue_RE;
    private com.toedter.calendar.JDateChooser DateChooser_Issue_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_Issuedate_WFCA;
    private com.toedter.calendar.JDateChooser DateChooser_Issuedate_WFCA1;
    private com.toedter.calendar.JDateChooser DateChooser_Maturitydate_WFCA;
    private com.toedter.calendar.JDateChooser DateChooser_MaturyDate_DRP;
    private com.toedter.calendar.JDateChooser DateChooser_MaturyDate_DRP1;
    private com.toedter.calendar.JDateChooser DateChooser_MaturyDate_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_MaturyDate_WFBA1;
    private com.toedter.calendar.JDateChooser DateChooser_PurchaseDate_DRP;
    private com.toedter.calendar.JDateChooser DateChooser_PurchaseDate_DRP1;
    private com.toedter.calendar.JDateChooser DateChooser_PurchaseDate_PU;
    private com.toedter.calendar.JDateChooser DateChooser_PurchaseDate_RE;
    private com.toedter.calendar.JDateChooser DateChooser_PurchaseDate_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_Realization_DRP;
    private com.toedter.calendar.JDateChooser DateChooser_Realization_DRP1;
    private com.toedter.calendar.JDateChooser DateChooser_Realization_PU;
    private com.toedter.calendar.JDateChooser DateChooser_Realization_RE;
    private com.toedter.calendar.JDateChooser DateChooser_SubmissionDate_WFCA;
    private com.toedter.calendar.JDateChooser DateChooser_SubmissionDate_WFCA1;
    private com.toedter.calendar.JDateChooser DateChooser_from;
    private com.toedter.calendar.JDateChooser DateChooser_from1;
    private com.toedter.calendar.JDateChooser DateChooser_to;
    private com.toedter.calendar.JDateChooser DateChooser_to1;
    private javax.swing.JButton LC_preview;
    private javax.swing.JTable OrderInHandTable;
    private javax.swing.JButton PI_preview;
    private javax.swing.JTable Table_LC;
    private javax.swing.JTable Table_PI;
    private javax.swing.JButton bAddToDRPReport;
    private javax.swing.JButton bAddToPurchase;
    private javax.swing.JButton bAddToRealization;
    private javax.swing.JButton bAddToWFBAReport1;
    private javax.swing.JButton bAddToWFCAReport;
    private javax.swing.JButton bAddToWFCAReport1;
    private javax.swing.JButton bCreateDRPReport1;
    private javax.swing.JButton bCreateDRPReport2;
    private javax.swing.JButton bCreateDRPReport3;
    private javax.swing.JButton bCreateOrderInHandReport;
    private javax.swing.JButton bCreateREeport;
    private javax.swing.JButton bCreateREeport1;
    private javax.swing.JButton bCreateWFBAReport_WFBA;
    private javax.swing.JButton bCreateWFBAReport_WFBA1;
    private javax.swing.JButton bCreateWFCAReport;
    private javax.swing.JButton bCreateWFCAReport1;
    private javax.swing.JButton bCreate_OIH_report;
    private javax.swing.JButton bDeleteFromDRPReport_DRP;
    private javax.swing.JButton bDeleteFromDRPReport_DRP1;
    private javax.swing.JButton bDeleteFromOrderInHandReport;
    private javax.swing.JButton bDeleteFromWFBAReport_WFBA;
    private javax.swing.JButton bDeleteFromWFCAReport;
    private javax.swing.JButton bDeleteFromWFCAReport1;
    private javax.swing.JButton bDeleteFromWFCAReport2;
    private javax.swing.JButton bDeleteFrom_OIH_Report;
    private javax.swing.JButton bDeleteFromrealization;
    private javax.swing.JButton bDeleteFromrealization1;
    private javax.swing.JButton bEditInfo;
    private javax.swing.JButton bEditInfo1_WFCA;
    private javax.swing.JButton bEditInfo_DRP;
    private javax.swing.JButton bEditInfo_DRP1;
    private javax.swing.JButton bEditInfo_OIH;
    private javax.swing.JButton bEditInfo_RE;
    private javax.swing.JButton bEditInfo_RE1;
    private javax.swing.JButton bEditInfo_WFBA;
    private javax.swing.JButton bEditInfo_WFBA1;
    private javax.swing.JButton bEditInfo_WFCA;
    private javax.swing.JButton bcreateLC_Report;
    private javax.swing.JButton bcreatePI_Report;
    private com.toedter.calendar.JDateChooser dateChooser_DelComDate_OIH;
    private com.toedter.calendar.JDateChooser dateChooser_DocumentHandOver_OIH;
    private com.toedter.calendar.JDateChooser dateChooser_SubmissionDate_OIH;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    private javax.swing.JButton jButton29;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser10;
    private com.toedter.calendar.JDateChooser jDateChooser11;
    private com.toedter.calendar.JDateChooser jDateChooser12;
    private com.toedter.calendar.JDateChooser jDateChooser13;
    private com.toedter.calendar.JDateChooser jDateChooser14;
    private com.toedter.calendar.JDateChooser jDateChooser15;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private com.toedter.calendar.JDateChooser jDateChooser4;
    private com.toedter.calendar.JDateChooser jDateChooser5;
    private com.toedter.calendar.JDateChooser jDateChooser6;
    private com.toedter.calendar.JDateChooser jDateChooser7;
    private com.toedter.calendar.JDateChooser jDateChooser8;
    private com.toedter.calendar.JDateChooser jDateChooser9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel100;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel102;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel105;
    private javax.swing.JLabel jLabel106;
    private javax.swing.JLabel jLabel107;
    private javax.swing.JLabel jLabel108;
    private javax.swing.JLabel jLabel109;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel110;
    private javax.swing.JLabel jLabel111;
    private javax.swing.JLabel jLabel112;
    private javax.swing.JLabel jLabel113;
    private javax.swing.JLabel jLabel114;
    private javax.swing.JLabel jLabel115;
    private javax.swing.JLabel jLabel116;
    private javax.swing.JLabel jLabel117;
    private javax.swing.JLabel jLabel118;
    private javax.swing.JLabel jLabel119;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel120;
    private javax.swing.JLabel jLabel121;
    private javax.swing.JLabel jLabel122;
    private javax.swing.JLabel jLabel123;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel97;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane18;
    private javax.swing.JScrollPane jScrollPane19;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane20;
    private javax.swing.JScrollPane jScrollPane21;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel panel_OrderInHand;
    private javax.swing.JTable primary1st;
    private javax.swing.JRadioButton rBuyer;
    private javax.swing.JRadioButton rBuyer1;
    private javax.swing.JRadioButton rBuyer2;
    private javax.swing.JRadioButton rBuyer3;
    private javax.swing.JRadioButton rBuyer4;
    private javax.swing.JRadioButton rBuyer5;
    private javax.swing.JRadioButton rCount;
    private javax.swing.JRadioButton rCount1;
    private javax.swing.JRadioButton rCount2;
    private javax.swing.JRadioButton rCount3;
    private javax.swing.JRadioButton rCount4;
    private javax.swing.JRadioButton rCount5;
    private javax.swing.JRadioButton rDate;
    private javax.swing.JRadioButton rDate1;
    private javax.swing.JRadioButton rDate2;
    private javax.swing.JRadioButton rDate3;
    private javax.swing.JRadioButton rDate4;
    private javax.swing.JRadioButton rDate5;
    private javax.swing.JRadioButton rPi;
    private javax.swing.JRadioButton rPi1;
    private javax.swing.JRadioButton rPi2;
    private javax.swing.JRadioButton rPi3;
    private javax.swing.JRadioButton rPi4;
    private javax.swing.JRadioButton rPi5;
    private javax.swing.JRadioButton rQuality;
    private javax.swing.JRadioButton rQuality1;
    private javax.swing.JRadioButton rQuality2;
    private javax.swing.JRadioButton rQuality3;
    private javax.swing.JRadioButton rQuality4;
    private javax.swing.JRadioButton rQuality5;
    private javax.swing.JRadioButton rRate;
    private javax.swing.JRadioButton rRate1;
    private javax.swing.JRadioButton rRate2;
    private javax.swing.JRadioButton rRate3;
    private javax.swing.JRadioButton rRate4;
    private javax.swing.JRadioButton rRate5;
    private javax.swing.JTable table_DRP;
    private javax.swing.JTable table_DRP1;
    private javax.swing.JTable table_DRP2;
    private javax.swing.JTable table_OrderInHand;
    private javax.swing.JTable table_Purchase;
    private javax.swing.JTable table_Realization;
    private javax.swing.JTable table_WFBA;
    private javax.swing.JTable table_WFBA1;
    private javax.swing.JTable table_WFCA;
    private javax.swing.JTable table_WFCA1;
    private javax.swing.JTextField txt_LDBP_WFCA;
    private javax.swing.JTextField txt_LDPB_DRP;
    private javax.swing.JTextField txt_LDPB_DRP1;
    private javax.swing.JTextField txt_LDPB_WFBA;
    private javax.swing.JTextField txt_LDPD_WFBA;
    private javax.swing.JTextField txt_amount;
    private javax.swing.JTextField txt_amount1;
    private javax.swing.JTextField txt_amount_DRP;
    private javax.swing.JTextField txt_amount_DRP1;
    private javax.swing.JTextField txt_amount_OIH;
    private javax.swing.JTextField txt_balanceQuantity;
    private javax.swing.JTextField txt_balanceQuantity1;
    private javax.swing.JTextField txt_balanceQuantity_OIH;
    private javax.swing.JTextField txt_comments;
    private javax.swing.JTextField txt_comments_OIH;
    private javax.swing.JTextField txt_comments_WFBA;
    private javax.swing.JTextField txt_comments_WFBA1;
    private javax.swing.JTextField txt_comments_WFCA;
    private javax.swing.JTextField txt_comments_WFCA1;
    private javax.swing.JTextField txt_description;
    private javax.swing.JTextField txt_description1;
    private javax.swing.JTextField txt_description2;
    private javax.swing.JTextField txt_description3;
    private javax.swing.JTextField txt_description4;
    private javax.swing.JTextField txt_description5;
    private javax.swing.JTextField txt_description6;
    private javax.swing.JTextField txt_expiry;
    private javax.swing.JTextField txt_fddammount_DRP;
    private javax.swing.JTextField txt_fddammount_DRP1;
    private javax.swing.JTextField txt_issue;
    private javax.swing.JTextField txt_lc;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_pi;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_shipment;
    private javax.swing.JTextField txt_sight;
    private javax.swing.JTextField txt_todayDelivery;
    private javax.swing.JTextField txt_todayDelivery1;
    private javax.swing.JTextField txt_todayDelivery_OIH;
    private javax.swing.JTextField txt_totalAmount;
    private javax.swing.JTextField txt_totalAmount1;
    private javax.swing.JTextField txt_totalAmount_OIH;
    private javax.swing.JTextField txt_totalvalue_DRP;
    private javax.swing.JTextField txt_totalvalue_DRP1;
    private javax.swing.JTextField txt_totalvalue_WFBA;
    private javax.swing.JTextField txt_totalvalue_WFBA1;
    private javax.swing.JTextField txt_totalvalue_WFCA;
    private javax.swing.JTextField txt_totalvalue_WFCA1;
    private javax.swing.JTextField txt_unit;
    private javax.swing.JTextField txt_value_WFBA;
    private javax.swing.JTextField txt_value_WFBA1;
    private javax.swing.JTextField txt_value_WFCA;
    private javax.swing.JTextField txt_value_WFCA1;
    // End of variables declaration//GEN-END:variables
}
