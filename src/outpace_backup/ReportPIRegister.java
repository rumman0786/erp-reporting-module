
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rumman
 */
public class ReportPIRegister {

    private String fromDate, toDate, orderCondition;//
    BigDecimal tTotal = new BigDecimal(0);

    public void initiate(String orderCondition, String fromDate, String toDate) throws IOException,
            DocumentException, Exception {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.orderCondition = orderCondition;
        System.out.println("PI Register Before :: In initiate");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String result = "C:\\Users\\rabbi\\Desktop\\PI Register" + "_" + date + "_.pdf";
        System.out.println(result);
        Document document = new Document(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(result));
        System.out.println("PI Register Before:: In initiate after date");
        document.open();
        document.add(createTable());
        document.add(new Paragraph("Amount: " + tTotal));
        document.close();
        System.out.println("PI Register After:: In Create Table");
    }

    private PdfPTable createTable() throws Exception {

        System.out.println("PI Register In Create Table 1");
        PdfPTable table = new PdfPTable(10);
        table.setWidths(new int[]{2, 3, 2, 2, 2, 2, 2, 2, 2, 2});
        PdfPCell cell;
        String[] coloumns = {"Buyer", "PI No", "PI Date", "Expiry Date", "Sight Days",
            "Count", "Quantity", "Unit Price", "Amount", "Total Amount"};
        Font font = new Font(Font.FontFamily.COURIER, 7, Font.BOLD,
                BaseColor.BLACK);
        cell = new PdfPCell(new Phrase("PI Report", font));
        cell.setColspan(10);
            table.addCell(cell);
        for (String colname : coloumns) {
            cell = new PdfPCell(new Phrase(colname, font));
            table.addCell(cell);
        }
        FetchDataPIRegister fd = new FetchDataPIRegister(fromDate, toDate, orderCondition);//
        System.out.println("PI Register In Create Table 2");
        fd.readDataBase();
        //ArrayList<PIRegister> piRegister = fd.getPIRegister();
        ArrayList<PI> piRegister = fd.getPIRegister();
        for (PI pr : piRegister) {
            boolean flag = true ;
            System.out.println(pr.getCount_size());
            cell = new PdfPCell(new Phrase(pr.getBuyer(), font));
            cell.setRowspan(pr.getCount_size());
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(pr.getPi_id(), font));
            cell.setRowspan(pr.getCount_size());
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(pr.getPi_date(), font));
            cell.setRowspan(pr.getCount_size());
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(pr.getExpiry_date(), font));
            cell.setRowspan(pr.getCount_size());
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(pr.getSight_days(), font));
            cell.setRowspan(pr.getCount_size());
            table.addCell(cell);
            for (CountDetails c : pr.getCountList()) {
                cell = new PdfPCell(new Phrase(c.getCount_id(), font));
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("" + c.getQuantity(), font));
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("" + c.getRate(), font));
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("" + c.getAmount(), font));
                table.addCell(cell);
                if(flag){
                cell = new PdfPCell(new Phrase("" + pr.getTotalAmount(), font));
                cell.setRowspan(pr.getCount_size());
                table.addCell(cell);
                tTotal = tTotal.add(BigDecimal.valueOf(pr.getTotalAmount())); 
                flag = false ;
                }
            }

//            cell = new PdfPCell(new Phrase("" + pr.getRate(), font));
//            table.addCell(cell);
            
        }

        return table;
    }
}
