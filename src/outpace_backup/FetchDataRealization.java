
import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rumman
 */
public class FetchDataRealization {
 private String fromDate, toDate;
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private String realiztionQuery = null;
    private ArrayList<Realization> realizationList;

    public FetchDataRealization(String orderCondition) {//String fromDate, String toDate, 
//        this.fromDate = fromDate;
//        this.toDate = toDate;
        String orderby =" ";
        if(!orderCondition.equals(null)){
            orderby = " ORDER BY " + orderCondition ;
        }
        realiztionQuery = "SELECT * FROM realization "
                //+ " WHERE lc.lc_id='000113040333'";
               // + " WHERE LC_Date BETWEEN CAST( '" + fromDate + "' AS DATE) AND CAST( '" + toDate + "'  AS DATE)"
                + orderby;
        System.out.println(realiztionQuery);
    }

    public void readDataBase() throws Exception {
        try {
            System.out.println("In FetchDataPurchaseReport.readDataBase() start");
            // this will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // setup the connection with the DB.
            System.out.println("In FetchDataPurchaseReport.readDataBase() 1");
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/outspace?user=root");
			// System.out.println("Conneted");
            // statements allow to issue SQL queries to the database
            System.out.println(realiztionQuery);
            statement = connect.createStatement();
            // resultSet gets the result of the SQL query
            resultSet = statement.executeQuery(realiztionQuery);
            System.out.println("In FetchDataPurchaseReport.readDataBase() 3");
            realizationList = new ArrayList<Realization>();
            writeResultSet(resultSet);
            System.out.println("In FetchDataPurchaseReport.readDataBase() End");
            // preparedStatements can use variables and are more efficient

        } catch (Exception e) {
            System.out.print(e);
            //throw e;
        } finally {
            // close();
        }

    }


     private void writeResultSet(ResultSet resultSet) throws SQLException {
        // resultSet is initialised before the first data set
        while (resultSet.next()) {
			// it is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g., resultSet.getSTring(2);
            String buyer = resultSet.getString("Buyer");
            String lc_id = resultSet.getString("LC_ID");
            String issue_date = resultSet.getString("LC_Date");
            String shipment_date = resultSet.getString("Last_Date_Of_Shipment");
            String expiry_date = resultSet.getString("Expiry_Date");
            String doc_Rec_Date = resultSet.getString("Doc_Rec_Date");
            String submission_Date = resultSet.getString("Submission_Date");
            String issuing_Date_Maturity = resultSet.getString("Issuing_Date_Maturity");
            String maturity_Date = resultSet.getString("Maturity_Date");
            String purchase_Date = resultSet.getString("Purchase_Date");
            String fdd_Issue_Date = resultSet.getString("Fdd_Issue_Date");
            double fdd_Amount = resultSet.getDouble("Fdd_Amount");
            String adjusted_Date = resultSet.getString("Adjusted_Date");
            String realization_Date = resultSet.getString("Realization_Date");
            String ldbp_No = resultSet.getString("LDBP_No");
            String count = resultSet.getString("Count");
            double quantity = Double.parseDouble(resultSet
                    .getString("Quantity"));
            double rate = Double.parseDouble(resultSet.getString("Rate"));
            double value = Double.parseDouble(resultSet.getString("Value"));
            double totalvalue = Double.parseDouble(resultSet.getString("Total_Value"));
            double exim = Double.parseDouble(resultSet.getString("Exim"));
            double scb = Double.parseDouble(resultSet.getString("SCB"));
            System.out.println("name: " + buyer);
            System.out.println("lc_id: " + lc_id);
            System.out.println("issue_date: " + issue_date);
            System.out.println("shipment_date: " + shipment_date);
//            System.out.println("site_days: " + sight_days);
//            System.out.println("pi_id: " + pi_id);
            System.out.println("count: " + count);
            System.out.println("quantity: " + quantity);
            System.out.println("rate: " + rate);
//            System.out.println("amount: " + amount);
//            System.out.println("total amount: " + totalAmount);
//            System.out.println("today delivery: " + todayDelivery);
//            System.out.println("Bal. Qty(kg): " + balanceQuantity);
            Realization realizationObject = new Realization(buyer, lc_id, issue_date,
                    shipment_date, expiry_date, doc_Rec_Date, 
                    submission_Date, issuing_Date_Maturity, 
                    maturity_Date, purchase_Date, fdd_Issue_Date,
                    fdd_Amount, adjusted_Date, realization_Date, ldbp_No,
                    count, quantity, rate, value, totalvalue, exim, scb);
            realizationList.add(realizationObject);
        }
    }


    public ArrayList<Realization> getRealizationList() {
        return realizationList;
    }

	// you need to close all three to make sure
    // private void close() {
    // close( resultSet);
    // close(statement);
    // close(connect);
    // }
    private void close(Closeable c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (Exception e) {
			// don't throw now as it might leave following closables in
            // undefined state
        }
    }
}
