
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package outspacetest01;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FetchData {
        private String fromDate,toDate;
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	private String orderInHandQuery = null;
	private ArrayList<OrderInHand> orderInHand;
        public FetchData(String fromDate,String toDate){
        this.fromDate = fromDate;
        this.toDate = toDate;
        orderInHandQuery = "SELECT b.name, lc.lc_id,lc.issue_date,lc.shipment_date, lc.expiry_date, lc.sight_days, lpr.pi_id, g.description, g.quantity, g.unit_price"
			+ " FROM letter_of_credit AS lc"
			+ " INNER JOIN buyer AS b ON b.buyer_id = lc.buyer_id"
			+ " INNER JOIN lc_pi_relation AS lpr ON lpr.lc_id=lc.lc_id"
			+ " INNER JOIN goods AS g ON g.pi_id=lpr.pi_id"
			//+ " WHERE lc.lc_id='000113040333'";
                        + " WHERE lc.issue_date BETWEEN CAST( '"+fromDate +"' AS DATE) AND CAST( '"+toDate+"'  AS DATE)"
                        + " ORDER BY b.name ASC ";
        System.out.println(orderInHandQuery);
        }
	public void readDataBase() throws Exception {
		try {
                    System.out.println("In FetchData.readDataBase() start");
			// this will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// setup the connection with the DB.
                        System.out.println("In FetchData.readDataBase() 1");
			connect = DriverManager
					.getConnection("jdbc:mysql://localhost/outspace?user=root");
			// System.out.println("Conneted");
			// statements allow to issue SQL queries to the database
                        System.out.println(orderInHandQuery);
			statement = connect.createStatement();
			// resultSet gets the result of the SQL query
			resultSet = statement.executeQuery(orderInHandQuery);
                        System.out.println("In FetchData.readDataBase() 3");
			orderInHand = new ArrayList<OrderInHand>();
			writeResultSet(resultSet);
			System.out.println("In FetchData.readDataBase() End");
			// preparedStatements can use variables and are more efficient

		} catch (Exception e) {
                    System.out.print(e);
			//throw e;
		} finally {
			// close();
		}

	}

	private void writeMetaData(ResultSet resultSet) throws SQLException {
		// now get some metadata from the database
		System.out.println("The columns in the table are: ");
		System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
		for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
			System.out.println("Column " + i + " "
					+ resultSet.getMetaData().getColumnName(i));
		}
	}

	private void writeResultSet(ResultSet resultSet) throws SQLException {
		// resultSet is initialised before the first data set
		while (resultSet.next()) {
			// it is possible to get the columns via name
			// also possible to get the columns via the column number
			// which starts at 1
			// e.g., resultSet.getSTring(2);
			String name = resultSet.getString("name");
			String lc_id = resultSet.getString("lc_id");
			String issue_date = resultSet.getString("issue_date");
			String shipment_date = resultSet.getString("shipment_date");
			String expiry_date = resultSet.getString("expiry_date");
			String sight_days = resultSet.getString("sight_days");
			String pi_id = resultSet.getString("pi_id");
			String count = resultSet.getString("description");
			double quantity = Double.parseDouble(resultSet
					.getString("quantity"));
			double rate = Double.parseDouble(resultSet.getString("unit_price"));
			double amount = quantity * rate;
			String totalAmount = "";
			String todayDelivery = "";
			String balanceQuantity = "";
			System.out.println("name: " + name);
			System.out.println("lc_id: " + lc_id);
			System.out.println("issue_date: " + issue_date);
			System.out.println("shipment_date: " + shipment_date);
			System.out.println("site_days: " + sight_days);
			System.out.println("pi_id: " + pi_id);
			System.out.println("count: " + count);
			System.out.println("quantity: " + quantity);
			System.out.println("rate: " + rate);
			System.out.println("amount: " + amount);
			System.out.println("total amount: " + totalAmount);
			System.out.println("today delivery: " + todayDelivery);
			System.out.println("Bal. Qty(kg): " + balanceQuantity);
//			OrderInHand o = new OrderInHand(name, lc_id, issue_date,
//					shipment_date, expiry_date, sight_days, pi_id, count,
//					quantity, rate);
//			orderInHand.add(o);
		}
	}

	public ArrayList<OrderInHand> getOrderInHand() {
		return orderInHand;
	}

	// you need to close all three to make sure
	// private void close() {
	// close( resultSet);
	// close(statement);
	// close(connect);
	// }
	private void close(Closeable c) {
		try {
			if (c != null) {
				c.close();
			}
		} catch (Exception e) {
			// don't throw now as it might leave following closables in
			// undefined state
		}
	}
}