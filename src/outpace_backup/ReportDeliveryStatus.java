
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rumman
 */
public class ReportDeliveryStatus {

    private String fromDate, toDate, orderCondition;//

    public void initiate(String orderCondition, String fromDate, String toDate) throws IOException,
            DocumentException, Exception {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.orderCondition = orderCondition;
        System.out.println("Delivery Status Before :: In initiate");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String desktopPath = System.getProperty("user.home") + "\\Desktop\\";
        String result = desktopPath+"Delivery Status" + "_" + date + "_.pdf";
        System.out.println(result);
        Document document = new Document(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(result));
        System.out.println("Delivery Status Before:: In initiate after date");
        document.open();
        document.add(createTable());
        document.close();
        System.out.println("Delivery Status After:: In initiate Table");
    }

    private PdfPTable createTable() throws Exception {
        System.out.println("Delivery Status In Create Table 1");
        PdfPTable table = new PdfPTable(11);
        table.setWidths(new int[]{2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 3});
        PdfPCell cell;
        String[] coloumns = {"Buyer", "Date", "Chalan",
            "L/C#", "PI#", "Count", "Lot#", "Quantity", "Rate", "Amount", "Remarks"};
        Font font = new Font(Font.FontFamily.COURIER, 7, Font.BOLD,
                BaseColor.BLACK);
        cell = new PdfPCell(new Phrase("SHIPMENT STATUS", font));
        cell.setColspan(11);
        table.addCell(cell);
        for (String colname : coloumns) {
            cell = new PdfPCell(new Phrase(colname, font));
            table.addCell(cell);
        }
        FetchDataDeliveryStatus fd = new FetchDataDeliveryStatus(fromDate, toDate, orderCondition);//
        System.out.println("Delivery Status In Create Table 2");
        fd.readDataBase();
        //ArrayList<PIRegister> piRegister = fd.getPIRegister();
        ArrayList<DeliveryStatus> deliveryStatus = fd.getdDeliveryStatuses();
        for (DeliveryStatus ds : deliveryStatus) {
            cell = new PdfPCell(new Phrase(ds.getBuyer(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(ds.getDate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(ds.getChalan(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(ds.getLc_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(ds.getPi_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(ds.getCount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(ds.getLot(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + ds.getQuantity(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + ds.getRate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + ds.getAmount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(ds.getRemarks(), font));
            table.addCell(cell);
        }
        return table;
    }
}