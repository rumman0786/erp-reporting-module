/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class PIRegister {
    private String buyerName;
    private String piNo;
    private String piDate;
    private String expiryDate;
    private String sightDays;
    private String count;
    private double quantity;
    private double unitPrice;
    private double totalValue;

    public PIRegister(String buyerName, String piNo, String piDate, String expiryDate, String sightDays, String count, double quantity, double unitPrice) {
        this.buyerName = buyerName;
        this.piNo = piNo;
        this.piDate = piDate;
        this.expiryDate = expiryDate;
        this.sightDays = sightDays;
        this.count = count;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.totalValue = quantity * unitPrice;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public String getPiNo() {
        return piNo;
    }

    public String getPiDate() {
        return piDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getSightDays() {
        return sightDays;
    }

    public String getCount() {
        return count;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public double getTotalValue() {
        return totalValue;
    }
    
}
