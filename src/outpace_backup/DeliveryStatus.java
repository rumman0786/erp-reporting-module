/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class DeliveryStatus {
    private String buyer;
    private String date;
    private String chalan;
    private String lc_id;
    private String pi_id;
    private String count;
    private String lot;
    private double quantity;
    private double rate;
    private double amount;
    private String remarks;

    public DeliveryStatus(String buyer, String date, String chalan, String lc_id, String pi_id, String count, String lot, double quantity, double rate, double amount, String remarks) {
        this.buyer = buyer;
        this.date = date;
        this.chalan = chalan;
        this.lc_id = lc_id;
        this.pi_id = pi_id;
        this.count = count;
        this.lot = lot;
        this.quantity = quantity;
        this.rate = rate;
        this.amount = amount;
        this.remarks = remarks;
    }

    public String getBuyer() {
        return buyer;
    }

    public String getDate() {
        return date;
    }

    public String getChalan() {
        return chalan;
    }

    public String getLc_id() {
        return lc_id;
    }

    public String getPi_id() {
        return pi_id;
    }

    public String getCount() {
        return count;
    }

    public String getLot() {
        return lot;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getRate() {
        return rate;
    }

    public double getAmount() {
        return amount;
    }

    public String getRemarks() {
        return remarks;
    }
    
}
