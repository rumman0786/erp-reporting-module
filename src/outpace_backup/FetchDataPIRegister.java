
import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
class FetchDataPIRegister {
    private String fromDate, toDate;
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private String piRegisterQuery = null;
    //private ArrayList<PIRegister> piRegisterList;
    private ArrayList<PI> piList;

    public FetchDataPIRegister(String fromDate, String toDate, String orderCondition) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        String orderby =" ";
        if(!orderCondition.equals(null)){
            orderby = " ORDER BY " + orderCondition ;
        }
        piRegisterQuery = "SELECT b.name,p.pi_id,p.pi_date,p.offer_validity,p.payment_period, "
                + " g.description,g.quantity,g.unit_price FROM proforma_invoice AS p "
                + " JOIN buyer AS b ON b.buyer_id = p.buyer_id "
                + " JOIN goods AS g ON g.pi_id = p.pi_id "
                + " WHERE p.pi_date BETWEEN CAST( '"+fromDate +"' AS DATE) AND CAST( '"+toDate+"'  AS DATE) "
                + orderby;
        System.out.println(piRegisterQuery);
    }

    public void readDataBase() throws Exception {
        try {
            System.out.println("In FetchDataPIRegister.readDataBase() start");
            // this will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // setup the connection with the DB.
            System.out.println("In FetchDataPIRegister.readDataBase() 1");
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/outspace?user=root");
			// System.out.println("Conneted");
            // statements allow to issue SQL queries to the database
            System.out.println(piRegisterQuery);
            statement = connect.createStatement();
            // resultSet gets the result of the SQL query
            resultSet = statement.executeQuery(piRegisterQuery);
            System.out.println("In FetchDataPIRegister.readDataBase() 3");
           // piRegisterList = new ArrayList<PIRegister>();
            piList = new ArrayList<PI>();
            writeResultSet(resultSet);
            System.out.println("In FetchDataPIRegister.readDataBase() End");
            // preparedStatements can use variables and are more efficient

        } catch (Exception e) {
            System.out.print(e);
            //throw e;
        } finally {
            // close();
        }

    }


       private void writeResultSet(ResultSet resultSet) throws SQLException {
        // resultSet is initialised before the first data set
        while (resultSet.next()) {
            String buyer = resultSet.getString("name");
            String pi_id = resultSet.getString("pi_id");
            String pi_date = resultSet.getString("pi_date");
            String offer_validity_date = resultSet.getString("offer_validity");
            String payment_period = resultSet.getString("payment_period");
            String description = resultSet.getString("description");
            double quantity = resultSet.getDouble("quantity");
            double rate = resultSet.getDouble("unit_price");
            //PIRegister piRegisterObject = new PIRegister(buyer, pi_id, pi_date, offer_validity_date,payment_period, description, quantity, rate);
            //piRegisterList.add(piRegisterObject);
            processPI(buyer,description,offer_validity_date,payment_period,pi_date,pi_id,quantity,rate);
        }
    }
    private void processPI(String buyer,String description,String offer_validity_date,String payment_period,
            String pi_date,String pi_id,double quantity,double rate){
          PI piObject = new PI(buyer, pi_id, pi_date, offer_validity_date, payment_period);//,description,quantity,rate);
            for (PI oldPiObject : piList) {
                if (oldPiObject.getPi_id().equals(pi_id)) {
                    CountDetails countDetails = new CountDetails(description, quantity, rate);
                    oldPiObject.addCountDetails(countDetails);
                    return;
                }
            }
            piList.add(piObject);
            CountDetails countDetails = new CountDetails(description, quantity, rate);
            piObject.addCountDetails(countDetails);
    
    }
public ArrayList<PI> getPIRegister() {
        return piList;
    }
//    public ArrayList<PIRegister> getPIRegister() {
//        return piRegisterList;
//    }

    private void close(Closeable c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (Exception e) {
	
        }
    }
}
