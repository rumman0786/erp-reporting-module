import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class MYFIRSTTABLE {
    public static String result = "C:\\Users\\rumman\\Desktop\\MYTHIRDTABLE.pdf";
    private String fromDate, toDate;

    public void initiate(String fromDate, String toDate) throws IOException,
            DocumentException, Exception {
        this.fromDate = fromDate;
        this.toDate = toDate;
        System.out.println("Before:: In initiate");
        Document document = new Document(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(result));
        document.open();
        document.add(createTable());
        document.close();
        System.out.println("After:: In Create Table");
    }

    private PdfPTable createTable() throws Exception {
        // TODO Auto-generated method stub
        System.out.println("In Create Table 1");
        PdfPTable table = new PdfPTable(14);
        PdfPCell cell;
        String[] coloumns = {"Buyer", "L//C#", "L//C Date",
            "Last Date of\n shipment", "Expiry Date", "L//C Sight",
            "PI No", "Count", "Qty", "Rate", "Amount", "Total Amount",
            "Today\nDelivery", "Bal Qty (kg)"};
        Font font = new Font(FontFamily.COURIER, 5, Font.BOLD,
                BaseColor.BLACK);;
        for (String colname : coloumns) {
            cell = new PdfPCell(new Phrase(colname, font));
            table.addCell(cell);
        }
        FetchData fd = new FetchData(fromDate, toDate);
        System.out.println("In Create Table 2");
        fd.readDataBase();
        ArrayList<OrderInHand> orderInHand = fd.getOrderInHand();

        for (OrderInHand o : orderInHand) {
            cell = new PdfPCell(new Phrase(o.getName(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLast_shipment_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getExpiry_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_sight_days(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getPi_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getCount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getQuantity(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getRate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getAmount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTotalAmount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTodayDelivery(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getBalanceQuantity(), font));
            table.addCell(cell);
        }
        return table;
    }
}
