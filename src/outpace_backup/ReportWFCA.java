
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class ReportWFCA {
    private String orderCondition;//fromDate, toDate,
    BigDecimal tValue = new BigDecimal(0),tTotalValue = new BigDecimal(0);
    
    
    public void initiate(String orderCondition) throws IOException, //String fromDate, String toDate,
            DocumentException, Exception {
//        this.fromDate = fromDate;
//        this.toDate = toDate;
        this.orderCondition = orderCondition ;
        System.out.println("ReportWFCA Before :: In initiate");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String result = "C:\\Users\\rabbi\\Desktop\\WFCA" + "_"+date+ "_.pdf";
        System.out.println(result);
        Document document = new Document(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream( result));
         System.out.println("ReportWFCA Before:: In initiate after date");
        document.open();
        document.add(createTable());
        document.add(new Paragraph("Value: "+tValue));
        document.add(new Paragraph("Total Value : "+tTotalValue));
        document.close();
        System.out.println("ReportWFCA After:: In Create Table");
    }

    private PdfPTable createTable() throws Exception {
        
        System.out.println("ReportWFCA In Create Table 1");
        PdfPTable table = new PdfPTable(16);
        table.setWidths(new int[]{2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2});
        PdfPCell cell;
        String[] coloumns = {"Buyer", "L/C#", "L/C Date",
            "Last Date of\n shipment", "Expiry Date", "L/C Sight",
            "PI No","Del.Com.Date","Submission Date","Comments",
            "Doc Han ov. Date", "Count", "Qty", "Rate", "Value", "Total Value"
            };
        Font font = new Font(Font.FontFamily.COURIER, 7, Font.BOLD,
                BaseColor.BLACK);
        for (String colname : coloumns) {
            cell = new PdfPCell(new Phrase(colname, font));
            table.addCell(cell);
        }
        FetchDataWFCA fd = new FetchDataWFCA(orderCondition);//fromDate, toDate,
        System.out.println("ReportWFCA :In Create Table 2");
        fd.readDataBase();
        ArrayList<WFCA> wfcaObject = fd.getWFCA();

        for (WFCA o : wfcaObject) {
            cell = new PdfPCell(new Phrase(o.getBuyer(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLast_shipment_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getExpiry_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_sight_days(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getPi_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getDel_Com_Date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getSubmissionDate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getComments(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getDocHandovDate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getCount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getQuantity(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getRate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getValue(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTotalValue(), font));
            table.addCell(cell);
            tValue = tValue.add(BigDecimal.valueOf(o.getValue())); 
            tTotalValue = tTotalValue.add(BigDecimal.valueOf(o.getTotalValue()));
            
        }
        
        return table;
    }
}
