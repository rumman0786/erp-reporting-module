/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//package outspacetest01;

public class OrderInHand {
	private String name ;
	private String lc_id ;
	private String lc_date ;
	private String last_shipment_date ;
	private String expiry_date ;
	private String lc_sight_days ;
	private String pi_id ;
	private String count ;
	private double quantity ;
	private double rate ;
	private double amount =0.0;
	private double totalAmount= 0.0;
	private double todayDelivery = 0.0;
	private double balanceQuantity = 0.0;
	
	public String getName() {
		return name;
	}

	public String getLc_id() {
		return lc_id;
	}

	public String getLc_date() {
		return lc_date;
	}

	public String getLast_shipment_date() {
		return last_shipment_date;
	}

	public String getExpiry_date() {
		return expiry_date;
	}

	public String getLc_sight_days() {
		return lc_sight_days;
	}

	public String getPi_id() {
		return pi_id;
	}

	public String getCount() {
		return count;
	}

	public double getQuantity() {
		return quantity;
	}

	public double getRate() {
		return rate;
	}

	public double getAmount() {
		return amount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public double getTodayDelivery() {
		return todayDelivery;
	}

	public double getBalanceQuantity() {
		return balanceQuantity;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLc_id(String lc_id) {
		this.lc_id = lc_id;
	}

	public void setLc_date(String lc_date) {
		this.lc_date = lc_date;
	}

	public void setLast_shipment_date(String last_shipment_date) {
		this.last_shipment_date = last_shipment_date;
	}

	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}

	public void setLc_sight_days(String lc_sight_days) {
		this.lc_sight_days = lc_sight_days;
	}

	public void setPi_id(String pi_id) {
		this.pi_id = pi_id;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

//	public void setAmount(double amount) {
//		this.amount = amount;
//	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setTodayDelivery(double todayDelivery) {
		this.todayDelivery = todayDelivery;
	}

	public void setBalanceQuantity(double balanceQuantity) {
		this.balanceQuantity = balanceQuantity;
	}

	public OrderInHand(String name,String lc_id,String lc_date,String last_shipment_date,String expiry_date, String lc_sight_days,String pi_id,String count,double quantity,double rate,double amount,double totalAmount,double todayDelivery,double balanceQuantity) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.lc_id = lc_id;
		this.lc_date =lc_date;
		this.last_shipment_date = last_shipment_date;
		this.expiry_date = expiry_date ;
		this.lc_sight_days = lc_sight_days ;
		this.pi_id = pi_id ;
		this.count = count ;
		this.quantity = quantity ;
		this.rate = rate ;
		this.amount = amount ;
                this.totalAmount = totalAmount ;
                this.balanceQuantity = balanceQuantity;
                this.todayDelivery = todayDelivery;
	}
}
