/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class CountDetails {

    String count_id;
    private double quantity;
    private double rate;
    private double amount;

    public CountDetails(String count_id, double quantity, double rate) {
        this.count_id = count_id;
        this.quantity = quantity;
        this.rate = rate;
        this.amount = rate * quantity ;
    }

    public String getCount_id() {
        return count_id;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getRate() {
        return rate;
    }

    public double getAmount() {
        return amount;
    }
    
}
