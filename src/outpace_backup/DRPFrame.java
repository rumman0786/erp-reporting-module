import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rabbi
 */
public class DRPFrame extends javax.swing.JFrame {

    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    String lc_id_WFCA = null;
    String count_WFCA = null;
    String buyer_wfba, lcId_wfba, lcDate_wfba, lastDateOfShipment_wfba, expiryDate_wfba, lcSight_wfba, piNo_wfba, LDPB_No_wfba, count_wfba, quantity_wfba, rate_wfba, amount_wfba, totalAmount_wfba, sDocRecDate_wfba, sBankSUBDate_wfba, comments_wfba, sMaturityDate_wfba, totalValue_wfba, value_wfba;
    String buyer_drp, lcId_drp, lcDate_drp, lastDateOfShipment_drp, expiryDate_drp, sDocRecDate_drp, sBankSUBDate_drp, DateChooser_DocRecDate_drp, DateChooser_BankSUBDate_drp, DateChooser_Adjust_drp, DateChooser_Issue_drp, DateChooser_MaturyDate_drp, DateChooser_PurchaseDate_drp, DateChooser_Realization_drp, DateChooser_FddAdd_drp, sIssueDate_drp, sMaturityDate_drp, sPurchaseDate_drp, sFddDate_drp, amount_drp, sAdjustDate_drp, sRealizationDate_drp, LDPB_No_drp, count_drp, quantity_drp, rate_drp, Value_drp, total_drp, exim_drp, scb_drp;
    String orderCondition = " Buyer ASC ";
    String beneficiary_bank;

    /**
     * Creates new form DRPFrame
     */
    public DRPFrame() {
        initComponents();
        conn = loginframe.MYSQLSetup();
        updateTableWFBA();
        updateTableDRP();
    }

    private void updateTableWFBA() {
        try {
            String sqlQuery = "SELECT * FROM wfba";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_WFBA.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }

    private void updateTableDRP() {
        try {
            String sqlQuery = "SELECT * FROM drp";
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            table_DRP.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }

    private void updateTable(JTable tablename, String sql) {
        try {

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            tablename.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }

    }

    public void updateSortedTable(String sortCondition) {
        String orderBy = " ORDER BY " + sortCondition;
        try {
            String sqlQuery = "SELECT * FROM drp " + orderBy;
            pst = conn.prepareStatement(sqlQuery);
            rs = pst.executeQuery();
            // table_WFBA.setModel(DbUtils.resultSetToTableModel(rs));
            table_DRP.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    private String getBankNameId(String reqirement) {
        String beneficiary_bank_id = "";
        try {
            String sql = "SELECT * FROM `letter_of_credit` WHERE `lc_id`=" + lcId_wfba;
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            if (rs.next()) {
                beneficiary_bank_id = rs.getString("beneficiary_bank_id");
            }
            String sql_bank = "SELECT * FROM `banks` WHERE `bank_id`=" + beneficiary_bank_id;
            pst = conn.prepareStatement(sql_bank);
            rs = pst.executeQuery();
            if (rs.next()) {
                beneficiary_bank = rs.getString("name");
                //System.out.println("beneficiary_bank");
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
        if (reqirement.equals("name")) {
            return beneficiary_bank;
        }
        return beneficiary_bank_id;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_WFBA = new javax.swing.JTable();
        txt_totalvalue_WFBA = new javax.swing.JTextField();
        DateChooser_MaturyDate_WFBA = new com.toedter.calendar.JDateChooser();
        DateChooser_BankSUBDate_WFBA = new com.toedter.calendar.JDateChooser();
        DateChooser_DocRecDate_WFBA = new com.toedter.calendar.JDateChooser();
        txt_value_WFBA = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txt_LDPB_WFBA = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txt_comments_WFBA = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        DateChooser_PurchaseDate_WFBA = new com.toedter.calendar.JDateChooser();
        jLabel27 = new javax.swing.JLabel();
        DateChooser_Issue_WFBA = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_DRP = new javax.swing.JTable();
        bDeleteFromWFCAReport1 = new javax.swing.JButton();
        bCreateDRPReport2 = new javax.swing.JButton();
        bEditInfo_WFBA = new javax.swing.JButton();
        bCreateWFBAReport_WFBA = new javax.swing.JButton();
        bAddToDRPReport = new javax.swing.JButton();
        rDate = new javax.swing.JRadioButton();
        rBuyer = new javax.swing.JRadioButton();
        rPi = new javax.swing.JRadioButton();
        rCount = new javax.swing.JRadioButton();
        rRate = new javax.swing.JRadioButton();
        rQuality = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        ComboBox_name = new javax.swing.JComboBox();
        jButton3 = new javax.swing.JButton();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        dt_To_drp = new com.toedter.calendar.JDateChooser();
        btn_SearchByDate_drp = new javax.swing.JButton();
        jLabel28 = new javax.swing.JLabel();
        txt_search_drp = new javax.swing.JTextField();
        btn_LcSearch_drp = new javax.swing.JButton();
        btn_PiSearch_drp = new javax.swing.JButton();
        dt_from_drp = new com.toedter.calendar.JDateChooser();
        txt_bankName_WFBA1 = new javax.swing.JTextField();
        jButton4_drp = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        DateChooser_FddIssueDate_WFBA = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPane3.setAutoscrolls(true);

        table_WFBA.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_WFBA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_WFBAMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(table_WFBA);

        txt_totalvalue_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalvalue_WFBAActionPerformed(evt);
            }
        });

        txt_value_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_value_WFBAActionPerformed(evt);
            }
        });

        jLabel15.setText("Maturity.Date");

        jLabel19.setText("value");

        jLabel21.setText("Total value");

        jLabel23.setText("Comments");

        jLabel2.setText("LDPD No.");

        jLabel14.setText("Doc. REC. Date");

        jLabel24.setText("Bank.SUB.Date");

        txt_comments_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_comments_WFBAActionPerformed(evt);
            }
        });

        jLabel26.setText("Purchase.Date");

        jLabel27.setText("Issue Date");

        table_DRP.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_DRP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_DRPMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table_DRP);

        bDeleteFromWFCAReport1.setText("Delete From WFBA Report");
        bDeleteFromWFCAReport1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteFromWFCAReport1ActionPerformed(evt);
            }
        });

        bCreateDRPReport2.setText("DRP Report");
        bCreateDRPReport2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateDRPReport2ActionPerformed(evt);
            }
        });

        bEditInfo_WFBA.setText("Edit Info");
        bEditInfo_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditInfo_WFBAActionPerformed(evt);
            }
        });

        bCreateWFBAReport_WFBA.setText("WFBA  Report");
        bCreateWFBAReport_WFBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCreateWFBAReport_WFBAActionPerformed(evt);
            }
        });

        bAddToDRPReport.setText("ADD to DRP");
        bAddToDRPReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAddToDRPReportActionPerformed(evt);
            }
        });

        buttonGroup1.add(rDate);
        rDate.setText("Date");
        rDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rDateActionPerformed(evt);
            }
        });

        buttonGroup1.add(rBuyer);
        rBuyer.setText("Buyer");
        rBuyer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rBuyerActionPerformed(evt);
            }
        });

        buttonGroup1.add(rPi);
        rPi.setText("PI");
        rPi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rPiActionPerformed(evt);
            }
        });

        buttonGroup1.add(rCount);
        rCount.setText("Count");
        rCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rCountActionPerformed(evt);
            }
        });

        buttonGroup1.add(rRate);
        rRate.setText("Rate");
        rRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rRateActionPerformed(evt);
            }
        });

        buttonGroup1.add(rQuality);
        rQuality.setText("Quality");
        rQuality.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rQualityActionPerformed(evt);
            }
        });

        jButton1.setText("Go To Realization Report");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel20.setText("Bank Name");

        ComboBox_name.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "EXIM", "SCB" }));
        ComboBox_name.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                ComboBox_namePopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        ComboBox_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBox_nameActionPerformed(evt);
            }
        });

        jButton3.setText("Go To Purchase Report");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel30.setText("From");

        jLabel31.setText("To");

        btn_SearchByDate_drp.setText("Date");
        btn_SearchByDate_drp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SearchByDate_drpActionPerformed(evt);
            }
        });

        jLabel28.setText("Search");

        btn_LcSearch_drp.setText("LC");
        btn_LcSearch_drp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_LcSearch_drpActionPerformed(evt);
            }
        });

        btn_PiSearch_drp.setText("PI");
        btn_PiSearch_drp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_PiSearch_drpActionPerformed(evt);
            }
        });

        txt_bankName_WFBA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_bankName_WFBA1ActionPerformed(evt);
            }
        });

        jButton4_drp.setText("Refresh");
        jButton4_drp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4_drpActionPerformed(evt);
            }
        });

        jLabel16.setText("Fdd Issue Date");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(298, 298, 298)
                .addComponent(bCreateDRPReport2, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(139, 139, 139)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rPi)
                    .addComponent(rDate)
                    .addComponent(rBuyer)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rQuality)
                            .addComponent(rCount)
                            .addComponent(rRate))
                        .addGap(98, 98, 98)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(91, 91, 91)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bCreateWFBAReport_WFBA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bEditInfo_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(91, 91, 91)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bDeleteFromWFCAReport1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bAddToDRPReport, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 709, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGap(2, 2, 2)
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                    .addComponent(txt_comments_WFBA)
                                                    .addComponent(DateChooser_DocRecDate_WFBA, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                                    .addComponent(txt_value_WFBA)))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(DateChooser_Issue_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 127, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel15))
                                        .addGap(24, 24, 24))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(DateChooser_PurchaseDate_WFBA, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                            .addComponent(txt_bankName_WFBA1))
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(36, 36, 36)
                                                .addComponent(ComboBox_name, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(122, 122, 122)
                                                .addComponent(jLabel16)))
                                        .addGap(65, 65, 65)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(DateChooser_FddIssueDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_totalvalue_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(DateChooser_MaturyDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(DateChooser_BankSUBDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_LDPB_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(198, 198, 198)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel30)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(dt_from_drp, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel31)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(dt_To_drp, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel28)
                                                .addGap(34, 34, 34)
                                                .addComponent(txt_search_drp, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(11, 11, 11)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGap(21, 21, 21)
                                                        .addComponent(btn_LcSearch_drp, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                        .addGap(22, 22, 22)
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                            .addComponent(jButton4_drp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                            .addComponent(btn_PiSearch_drp, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)))))))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(btn_SearchByDate_drp, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(357, 357, 357))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_value_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_totalvalue_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel21)
                                    .addComponent(jLabel19))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(DateChooser_BankSUBDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(txt_LDPB_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel14)
                                            .addComponent(DateChooser_DocRecDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel24))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(txt_comments_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel23)
                                            .addComponent(jLabel2))))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel27)
                                        .addComponent(DateChooser_Issue_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel15))
                                    .addComponent(DateChooser_MaturyDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel30)
                                    .addComponent(dt_from_drp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_SearchByDate_drp)
                                    .addComponent(dt_To_drp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel31))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGap(61, 61, 61)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(txt_search_drp, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel28)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(33, 33, 33)
                                        .addComponent(btn_LcSearch_drp)
                                        .addGap(16, 16, 16)
                                        .addComponent(btn_PiSearch_drp)))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButton4_drp)
                                .addGap(0, 25, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(DateChooser_FddIssueDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel16)
                                            .addGap(6, 6, 6)))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(DateChooser_PurchaseDate_WFBA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(ComboBox_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_bankName_WFBA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(110, 110, 110)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bEditInfo_WFBA)
                    .addComponent(bAddToDRPReport))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bCreateWFBAReport_WFBA)
                    .addComponent(bDeleteFromWFCAReport1))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(239, 239, 239)
                        .addComponent(rDate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rBuyer)
                        .addGap(0, 0, 0)
                        .addComponent(rPi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rCount)
                            .addComponent(jButton2))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rRate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rQuality))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jButton1))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)
                        .addComponent(bCreateDRPReport2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton3)
                .addGap(491, 491, 491))
        );

        jScrollPane3.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_totalvalue_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalvalue_WFBAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalvalue_WFBAActionPerformed

    private void txt_value_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_value_WFBAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_value_WFBAActionPerformed

    private void txt_comments_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_comments_WFBAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_comments_WFBAActionPerformed

    private void bDeleteFromWFCAReport1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteFromWFCAReport1ActionPerformed
        try {
            String sql = "DELETE FROM wfba WHERE LC_ID = ? AND Count = ? ";
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, lcId_wfba);
            pst.setString(2, count_wfba);
            pst.executeUpdate();
            updateTableWFBA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bDeleteFromWFCAReport1ActionPerformed

    private void bCreateDRPReport2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateDRPReport2ActionPerformed
        ReportDRP rDRP = new ReportDRP();
        try {
            rDRP.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateDRPReport2ActionPerformed

    private void bEditInfo_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditInfo_WFBAActionPerformed
        try {
            Date DocRecDate_wfba = DateChooser_DocRecDate_WFBA.getDate();
            Date BankSUBDate_wfba = DateChooser_BankSUBDate_WFBA.getDate();
            Date MaturityDate_wfba = DateChooser_MaturyDate_WFBA.getDate();
            //Date FddAddDate_WFBA = DateChooser_FddAdd_WFBA.getDate();
            Date PurchaseDate_WFBA = DateChooser_PurchaseDate_WFBA.getDate();
            //Date AdjustDate_WFBA = DateChooser_Adjust_WFBA.getDate();
            Date IssueDate_WFBA = DateChooser_Issue_WFBA.getDate();
            //Date RealizationDate_WFBA = DateChooser_Realization_WFBA.getDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String sDocRecDate_wfb = dateFormat.format(DocRecDate_wfba);
            String sBankSUBDate_wfb = dateFormat.format(BankSUBDate_wfba);
            String sMaturityDate_wfb = dateFormat.format(MaturityDate_wfba);
            //String sFddAddDate_WFBA = dateFormat.format(FddAddDate_WFBA);
            String sPurchaseDate_WFBA = dateFormat.format(PurchaseDate_WFBA);
            //String sAdjustDate_WFBA = dateFormat.format(AdjustDate_WFBA);
            String sIssueDate_WFBA = dateFormat.format(IssueDate_WFBA);
            //String sRealizationDate_WFBA = dateFormat.format(RealizationDate_WFBA);

            String sql = "UPDATE wfba SET Value =  ? , Total_Value = ? , Doc_Rec_Date = ? , "
                    + " Bank_Sub_Date = ? , Comments = ? ,  Maturity_Date = ? , LDBP_No = ?  WHERE  LC_ID = ? AND Count = ? ";
            //conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_value_WFBA.getText());
            pst.setString(2, txt_totalvalue_WFBA.getText());
            pst.setString(3, sDocRecDate_wfb);
            pst.setString(4, sBankSUBDate_wfb);
            pst.setString(5, txt_comments_WFBA.getText());
            pst.setString(6, sMaturityDate_wfb);
            pst.setString(7, txt_LDPB_WFBA.getText());
//            pst.setString(8, txt_fddammount_WFBA.getText());
//            pst.setString(9, sFddAddDate_WFBA);
//            pst.setString(10, sPurchaseDate_WFBA);
//            pst.setString(11, sAdjustDate_WFBA);
//            pst.setString(12, sIssueDate_WFBA);
//            pst.setString(13, sRealizationDate_WFBA);

            pst.setString(8, lcId_wfba);
            pst.setString(9, count_wfba);
            pst.executeUpdate();
            updateTableWFBA();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_bEditInfo_WFBAActionPerformed

    private void bCreateWFBAReport_WFBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCreateWFBAReport_WFBAActionPerformed
        ReportWFBA mf = new ReportWFBA();
        //        Date fromDate = jDateChooser1.getDate();
        //        Date toDate = jDateChooser2.getDate();
        //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //        String sFromDate = dateFormat.format(fromDate);
        //        String sToDate = dateFormat.format(toDate);
        //System.out.println(sFromDate+" TO "+sToDate);

        try {
            mf.initiate(orderCondition);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bCreateWFBAReport_WFBAActionPerformed

    private void bAddToDRPReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAddToDRPReportActionPerformed
        Date DocRecDate_wfba = DateChooser_DocRecDate_WFBA.getDate();
        Date BankSUBDate_wfba = DateChooser_BankSUBDate_WFBA.getDate();
        Date MaturityDate_wfba = DateChooser_MaturyDate_WFBA.getDate();
        //Date FddDate_WFBA = DateChooser_FddAdd_WFBA.getDate();
        Date PurchaseDate_WFBA = DateChooser_PurchaseDate_WFBA.getDate();
        //Date AdjustDate_WFBA = DateChooser_Adjust_WFBA.getDate();
        Date IssueDate_WFBA = DateChooser_Issue_WFBA.getDate();
        Date FddIssueDate_WFBA = DateChooser_FddIssueDate_WFBA.getDate();
        //Date RealizationDate_WFBA = DateChooser_Realization_WFBA.getDate();
        Date todayDate = new Date();
        String sFddIssueDate ="";
        String sDocRecDate_wfb = "", sBankSUBDate_wfb = "", sMaturityDate_wfb = "", sPurchaseDate_WFBA = "", sIssueDate_WFBA = "", sTodayDate = "";
        if (!(DocRecDate_wfba == null) && !(BankSUBDate_wfba == null) && !(MaturityDate_wfba == null) && !(PurchaseDate_WFBA == null) && !(IssueDate_WFBA == null)) {//!(sFddDate_WFBA==null) &&  && !(sRealizationDate_WFBA==null)  && !(sAdjustDate_WFBA==null)
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            sDocRecDate_wfb = dateFormat.format(DocRecDate_wfba);
            sBankSUBDate_wfb = dateFormat.format(BankSUBDate_wfba);
            sMaturityDate_wfb = dateFormat.format(MaturityDate_wfba);
            //String sFddDate_WFBA = dateFormat.format(FddDate_WFBA);
            sPurchaseDate_WFBA = dateFormat.format(PurchaseDate_WFBA);
            // String sAdjustDate_WFBA = dateFormat.format(AdjustDate_WFBA);
            sIssueDate_WFBA = dateFormat.format(IssueDate_WFBA);
            //String sRealizationDate_WFBA = dateFormat.format(RealizationDate_WFBA);
            sTodayDate = dateFormat.format(todayDate);
        } else {
            JOptionPane.showMessageDialog(null, "Please Choose all three dates!!!");
            return;
        }
        if (!(FddIssueDate_WFBA == null)){
        sFddIssueDate = new SimpleDateFormat("yyyy-MM-dd").format(FddIssueDate_WFBA);
        }
        //System.out.println(sFromDate+" TO "+sToDate);
        try {
            String sql = "INSERT IGNORE INTO drp (Buyer, LC_ID, LC_Date, Last_Date_Of_Shipment,"
                    + " Expiry_Date, Doc_Rec_Date, Submission_Date , Issuing_Date_Maturity , Maturity_Date,"
                    + " Purchase_Date , Realization_Date ,Fdd_Issue_Date , LDBP_No , Count,"
                    + " Quantity, Rate, Value, Total_Value , Exim , SCB, HSBC,table_entry_date) "
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,? ,?)";

            String scbValue = "0.0";
            String hsbcValue = "0.0";
            String eximValue = "0.0";
            System.out.println(getBankNameId(""));
            switch (getBankNameId("")) {
                case "2":
                    scbValue = value_wfba;
                    break;
                case "3":
                    eximValue = value_wfba;
                    break;
                case "9":
                    eximValue = value_wfba;
                    break;
                case "24":
                    eximValue = value_wfba;
                    break;
                case "49":
                    hsbcValue = value_wfba;
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Contact Software Developer");
                    break;
            }

            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            pst.setString(1, buyer_wfba);
            pst.setString(2, lcId_wfba);
            pst.setString(3, lcDate_wfba);
            pst.setString(4, lastDateOfShipment_wfba);
            pst.setString(5, expiryDate_wfba);

            pst.setString(6, sDocRecDate_wfb);
            pst.setString(7, sBankSUBDate_wfb);
            pst.setString(8, sIssueDate_WFBA);
            pst.setString(9, sMaturityDate_wfb);
            pst.setString(10, sPurchaseDate_WFBA);
            System.out.println("In add to drp: upto dates ok");
                // pst.setString(11, "2014-10-12");//fdd issue date
            //pst.setString(12, "0.0");//txt_fddammount_WFBA.getText()
            //pst.setString(13, "2014-10-12");//adjusted date    
            pst.setString(11, "2014-10-12");//realizatoin date
            pst.setString(12, sFddIssueDate);
            pst.setString(13, txt_LDPB_WFBA.getText());
            pst.setString(14, count_wfba);
            pst.setString(15, quantity_wfba);
            pst.setString(16, rate_wfba);
            pst.setString(17, txt_value_WFBA.getText());
            pst.setString(18, txt_totalvalue_WFBA.getText());
            System.out.println("In add to drp: before add bank val");
            pst.setString(19, eximValue);
            System.out.println("In add to drp: exim");
            pst.setString(20, scbValue);
            System.out.println("In add to drp: scb");
            pst.setString(21, hsbcValue);
            System.out.println("In add to drp: hsbc");
            pst.setString(22, sTodayDate);
            System.out.println("In add to drp: hsbc");
            pst.executeUpdate();
            System.out.println(":::::");
            
        } catch (Exception e) {
            System.out.println("In add to drp: " + e);
        }
        updateTableDRP();

    }//GEN-LAST:event_bAddToDRPReportActionPerformed

    private void table_WFBAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_WFBAMouseClicked
        // TODO add your handling code here:
        try {
            int row = table_WFBA.getSelectedRow();
            lcId_wfba = (table_WFBA.getModel().getValueAt(row, 1).toString());
            count_wfba = (table_WFBA.getModel().getValueAt(row, 12).toString());
            String sql = "SELECT * FROM wfba"
                    + " WHERE LC_ID = '" + lcId_wfba + "' AND Count = '" + count_wfba + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_wfba = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_wfba = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_wfba = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_wfba = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_wfba = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);
                lcSight_wfba = rs.getString("LC_sight_Days");
                //txt_sight.setText(lcSight);
                piNo_wfba = rs.getString("PI_No");
                //txt_pi.setText(piNo);
                sDocRecDate_wfba = rs.getString("Doc_Rec_Date");
                sBankSUBDate_wfba = rs.getString("Bank_Sub_Date");
                comments_wfba = rs.getString("Comments");
                sMaturityDate_wfba = rs.getString("Maturity_Date");
                LDPB_No_wfba = rs.getString("LDBP_No");
                count_wfba = rs.getString("Count");
                quantity_wfba = rs.getString("Quantity");
                rate_wfba = rs.getString("Rate");
                value_wfba = rs.getString("Value");
                totalValue_wfba = rs.getString("Total_Value");

                txt_value_WFBA.setText(value_wfba);
                txt_totalvalue_WFBA.setText(totalValue_wfba);
                DateChooser_DocRecDate_WFBA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_wfba));
                DateChooser_BankSUBDate_WFBA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_wfba));
                txt_comments_WFBA.setText(comments_wfba);

                DateChooser_MaturyDate_WFBA.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_wfba));

                txt_LDPB_WFBA.setText(LDPB_No_wfba);
                txt_bankName_WFBA1.setText(getBankNameId("name"));
            }
            System.out.println("after loop mouse click");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_WFBAMouseClicked

    private void table_DRPMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_DRPMouseClicked
        // TODO add your handling code here:
        try {

            int row = table_DRP.getSelectedRow();
            lcId_drp = (table_DRP.getModel().getValueAt(row, 1).toString());
            count_drp = (table_DRP.getModel().getValueAt(row, 15).toString());
            String sql = "SELECT * FROM drp"
                    + " WHERE LC_ID = '" + lcId_drp + "' AND Count = '" + count_drp + "' ";
            System.out.println("after query");
            conn = loginframe.MYSQLSetup();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            System.out.println("after exc");
            if (rs.next()) {
                System.out.println("after loop");
                buyer_drp = rs.getString("Buyer");
                //txt_name.setText(add1);
                lcId_drp = rs.getString("LC_ID");
                //txt_lc.setText(add2);
                lcDate_drp = rs.getString("LC_Date");
                //txt_issue.setText(lcDate);
                lastDateOfShipment_drp = rs.getString("Last_Date_Of_Shipment");
                //txt_shipment.setText(lastDateOfShipment);
                expiryDate_drp = rs.getString("Expiry_Date");
                //txt_expiry.setText(expiryDate);

                sDocRecDate_drp = rs.getString("Doc_Rec_Date");
                sBankSUBDate_drp = rs.getString("Submission_Date");
                sIssueDate_drp = rs.getString("Issuing_Date_Of_Maturity");
                sMaturityDate_drp = rs.getString("Maturity_Date");
                sPurchaseDate_drp = rs.getString("Purchase_Date");
                sFddDate_drp = rs.getString("Fdd_Date");
                amount_drp = rs.getString("Fdd_Amount");
                sAdjustDate_drp = rs.getString("Adjusted_Date");
                sRealizationDate_drp = rs.getString("Realization_Date");
                LDPB_No_drp = rs.getString("LDBP_No");
                count_drp = rs.getString("Count");
                quantity_drp = rs.getString("Quantity");
                rate_drp = rs.getString("Rate");
                Value_drp = rs.getString("Value");
                total_drp = rs.getString("Total");
                exim_drp = rs.getString("Exim");
                scb_drp = rs.getString("SCB");

//                DateChooser_DocRecDate_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sDocRecDate_drp));
//                DateChooser_BankSUBDate_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sBankSUBDate_drp));
//                DateChooser_Adjust_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sAdjustDate_drp));
//                DateChooser_FddAdd_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sFddDate_drp));
//                DateChooser_Issue_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sIssueDate_drp));
//                DateChooser_MaturyDate_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sMaturityDate_drp));
//                DateChooser_PurchaseDate_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sPurchaseDate_drp));
//                DateChooser_Realization_drp.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sRealizationDate_drp));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
    }//GEN-LAST:event_table_DRPMouseClicked

    private void rDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rDateActionPerformed
        orderCondition = " LC_Date ASC ";
        updateSortedTable(orderCondition);

    }//GEN-LAST:event_rDateActionPerformed

    private void rBuyerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rBuyerActionPerformed
        orderCondition = " Buyer ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rBuyerActionPerformed

    private void rPiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rPiActionPerformed
        orderCondition = " PI_No ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rPiActionPerformed

    private void rCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rCountActionPerformed
        orderCondition = " Count ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rCountActionPerformed

    private void rRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRateActionPerformed
        orderCondition = " Rate ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rRateActionPerformed

    private void rQualityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rQualityActionPerformed
        orderCondition = " Quantity ASC ";
        updateSortedTable(orderCondition);
    }//GEN-LAST:event_rQualityActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        dispose();
        new RealizationFrame().setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
        new FrameWFBA().setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void ComboBox_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBox_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboBox_nameActionPerformed

    private void txt_bankName_WFBA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_bankName_WFBA1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_bankName_WFBA1ActionPerformed

    private void btn_PiSearch_drpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_PiSearch_drpActionPerformed
        // TODO add your handling code here:
        try {
            String sqlQuery = "SELECT * FROM wfba WHERE PI_No LIKE ? ";
            pst = conn.prepareStatement(sqlQuery);
            pst.setString(1, "%" + txt_search_drp.getText() + "%");
            pst.executeQuery();
            rs = pst.executeQuery();
            table_WFBA.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }
    }//GEN-LAST:event_btn_PiSearch_drpActionPerformed

    private void btn_LcSearch_drpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_LcSearch_drpActionPerformed
        // TODO add your handling code here:
        try {
            String sqlQuery = "SELECT * FROM wfba WHERE lc_id LIKE ? ";
            pst = conn.prepareStatement(sqlQuery);
            pst.setString(1, "%" + txt_search_drp.getText() + "%");
            pst.executeQuery();
            rs = pst.executeQuery();
            table_WFBA.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.out.println(ex);
        }

    }//GEN-LAST:event_btn_LcSearch_drpActionPerformed

    private void btn_SearchByDate_drpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SearchByDate_drpActionPerformed
        // TODO add your handling code here:
        rs = null;
        Date dtFrom = dt_from_drp.getDate();
        Date dtTo = dt_To_drp.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sFromDate = dateFormat.format(dtFrom);
        String sToDate = dateFormat.format(dtTo);;//"2014-08-09"
        try {
            // if(!(sFromDate.equals(null)) && !(sToDate.equals(null))){
            String sql = "SELECT * FROM wfba WHERE Doc_Rec_Date BETWEEN CAST('" + sFromDate + "' AS DATE) AND CAST('" + sToDate + "' AS DATE) ";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                // System.out.println(rs.getString("date"));
                //buyer_wfca = rs.getString("Buyer");
            }
            //Table_Delivery.setModel(DbUtils.resultSetToTableModel(rs));
            updateTable(table_WFBA, sql);
            System.out.println("In: If query executed properly");
            //}
        } catch (NullPointerException e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Please Choose all two dates!!!");
        } catch (SQLException ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Please Check Query!!!");
        }

    }//GEN-LAST:event_btn_SearchByDate_drpActionPerformed

    private void jButton4_drpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4_drpActionPerformed
        // TODO add your handling code here:
        updateTableWFBA();
    }//GEN-LAST:event_jButton4_drpActionPerformed

    private void ComboBox_namePopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_ComboBox_namePopupMenuWillBecomeInvisible
        // TODO add your handling code here:

    }//GEN-LAST:event_ComboBox_namePopupMenuWillBecomeInvisible

    private static void createAndShowGUI() {
        //Create and set up the window.
        DRPFrame frame = new DRPFrame();
        frame.setDefaultCloseOperation(primarydatatable.EXIT_ON_CLOSE);

        // Container c = frame.getContentPane();
//        frame.getContentPane();
        //  c.setBackground(Color.YELLOW);
        // adjust to need.
        Dimension d = new Dimension(400, 40);
        //c.setPreferredSize(d);

        //Add the ubiquitous "Hello World" label.
        //JLabel label = new JLabel("Hello World");
        //frame.getContentPane().add(label);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        frame.setSize(height, width);
        frame.setSize(d);
        //  LOG.info(height + "\t\t\t" + width);
        //Display the window.
        //frame.pack();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DRPFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DRPFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DRPFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DRPFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DRPFrame();
                createAndShowGUI();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox ComboBox_name;
    private com.toedter.calendar.JDateChooser DateChooser_BankSUBDate_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_DocRecDate_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_FddIssueDate_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_Issue_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_MaturyDate_WFBA;
    private com.toedter.calendar.JDateChooser DateChooser_PurchaseDate_WFBA;
    private javax.swing.JButton bAddToDRPReport;
    private javax.swing.JButton bCreateDRPReport2;
    private javax.swing.JButton bCreateWFBAReport_WFBA;
    private javax.swing.JButton bDeleteFromWFCAReport1;
    private javax.swing.JButton bEditInfo_WFBA;
    private javax.swing.JButton btn_LcSearch_drp;
    private javax.swing.JButton btn_PiSearch_drp;
    private javax.swing.JButton btn_SearchByDate_drp;
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JDateChooser dt_To_drp;
    private com.toedter.calendar.JDateChooser dt_from_drp;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4_drp;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JRadioButton rBuyer;
    private javax.swing.JRadioButton rCount;
    private javax.swing.JRadioButton rDate;
    private javax.swing.JRadioButton rPi;
    private javax.swing.JRadioButton rQuality;
    private javax.swing.JRadioButton rRate;
    private javax.swing.JTable table_DRP;
    private javax.swing.JTable table_WFBA;
    private javax.swing.JTextField txt_LDPB_WFBA;
    private javax.swing.JTextField txt_bankName_WFBA1;
    private javax.swing.JTextField txt_comments_WFBA;
    private javax.swing.JTextField txt_search_drp;
    private javax.swing.JTextField txt_totalvalue_WFBA;
    private javax.swing.JTextField txt_value_WFBA;
    // End of variables declaration//GEN-END:variables
}
