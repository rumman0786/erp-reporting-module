
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class LC {
   private String buyer,lc_id,lc_date,shipment_date,expiry_date,lc_sight;
   ArrayList<PI> piList = new ArrayList<PI>();
    double totalAmount = 0.0 ;
    int pi_size = 0;
    int count_size=0;
    public LC(String buyer, String lc_id, String lc_date, String shipment_date, String expiry_date, String lc_sight) {
        this.buyer = buyer;
        this.lc_id = lc_id;
        this.lc_date = lc_date;
        this.shipment_date = shipment_date;
        this.expiry_date = expiry_date;
        this.lc_sight = lc_sight;
    }

    public void addPIDetails(PI newpiObject,CountDetails countDetails) {
        //PI piObject = new PI(buyer, pi_id, pi_date, offer_validity_date, payment_period);//,description,quantity,rate);
        for (PI oldPiObject : piList) {
            if (oldPiObject.getPi_id().equals(newpiObject.getPi_id())) {
                oldPiObject.addCountDetails(countDetails);
                count_size++;
                return;
            }
        }
        piList.add(newpiObject);
        pi_size++;
        newpiObject.addCountDetails(countDetails);
        count_size++;
    }
    
    public double getTotalAmount() {
        double totalAmount = 0.0;
        if (!piList.isEmpty()) {
            for (PI pi : piList) {
                totalAmount += pi.getTotalAmount();
            }
        }
        return totalAmount;
    }

    public String getBuyer() {
        return buyer;
    }

    public int getCount_size() {
        return count_size;
    }
    
    public String getLc_id() {
        return lc_id;
    }

    public String getLc_date() {
        return lc_date;
    }

    public String getShipment_date() {
        return shipment_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public String getLc_sight() {
        return lc_sight;
    }

    public ArrayList<PI> getPiList() {
        return piList;
    }

    public int getPi_size() {
        return pi_size;
    }

}
