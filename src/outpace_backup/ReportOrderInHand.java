
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rumman
 */
public class ReportOrderInHand {

    private String orderCondition;//fromDate, toDate,
    BigDecimal tTotal = new BigDecimal(0),tTotalAmout = new BigDecimal(0),tTodayDelivery = new BigDecimal(0) ,tBalanceQuantity = new BigDecimal(0);
    
    
    public void initiate(String orderCondition) throws IOException, //String fromDate, String toDate,
            DocumentException, Exception {
//        this.fromDate = fromDate;
//        this.toDate = toDate;
        this.orderCondition = orderCondition ;
        System.out.println("Before:: In initiate");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String result = "C:\\Users\\rabbi\\Desktop\\OrderInHand" + "_"+date+ "_.pdf";
        System.out.println(result);
        Document document = new Document(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream( result));
         System.out.println("Before:: In initiate after date");
        document.open();
        document.add(createTable());
        document.add(new Paragraph("Amount: "+tTotal));
        document.add(new Paragraph("Total Amount : "+tTotalAmout));
        document.add(new Paragraph("Total Today Delivery: "+tTodayDelivery));
        document.add(new Paragraph("Total Balance QUantity: "+tBalanceQuantity));
        document.close();
        System.out.println("After:: In Create Table");
    }

    private PdfPTable createTable() throws Exception {
        
        System.out.println("In Create Table 1");
        PdfPTable table = new PdfPTable(14);
        table.setWidths(new int[]{2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 2, 2});
        PdfPCell cell;
        String[] coloumns = {"Buyer", "L/C#", "L/C Date",
            "Last Date of\n shipment", "Expiry Date", "L/C Sight",
            "PI No", "Count", "Qty", "Rate", "Amount", "Total Amount",
            "Today\nDelivery", "Bal Qty (kg)"};
        Font font = new Font(Font.FontFamily.COURIER, 7, Font.BOLD,
                BaseColor.BLACK);
        for (String colname : coloumns) {
            cell = new PdfPCell(new Phrase(colname, font));
            table.addCell(cell);
        }
        FetchDataOrderInHand fd = new FetchDataOrderInHand(orderCondition);//fromDate, toDate,
        System.out.println("In Create Table 2");
        fd.readDataBase();
        ArrayList<OrderInHand> orderInHand = fd.getOrderInHand();

        for (OrderInHand o : orderInHand) {
            cell = new PdfPCell(new Phrase(o.getName(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLast_shipment_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getExpiry_date(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getLc_sight_days(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(o.getPi_id(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getCount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getQuantity(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getRate(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getAmount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTotalAmount(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getTodayDelivery(), font));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("" + o.getBalanceQuantity(), font));
            table.addCell(cell);
            
            tTotal = tTotal.add(BigDecimal.valueOf(o.getAmount())); 
            tTotalAmout = tTotalAmout.add(BigDecimal.valueOf(o.getAmount()));
            tTodayDelivery = tTodayDelivery.add(BigDecimal.valueOf(o.getTodayDelivery())); 
            tBalanceQuantity = tBalanceQuantity.add(BigDecimal.valueOf(o.getBalanceQuantity())); 
        }
        
        return table;
    }
}
