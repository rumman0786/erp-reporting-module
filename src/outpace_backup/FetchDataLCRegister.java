
import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rumman
 */
public class FetchDataLCRegister {

    private String fromDate, toDate;
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private String lcRegisterQuery = null;
    //private ArrayList<PIRegister> piRegisterList;
    private ArrayList<LC> lcList;

    public FetchDataLCRegister(String fromDate, String toDate, String orderCondition) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        String orderby = " ";
        if (!orderCondition.equals(null)) {
            orderby = " ORDER BY " + orderCondition;
        }
        lcRegisterQuery = "SELECT b.name, lc.lc_id,lc.issue_date,lc.shipment_date, lc.expiry_date, "
                + " lc.sight_days, lpr.pi_id, g.description, g.quantity, g.unit_price "
                + " FROM letter_of_credit AS lc"
                + " JOIN buyer AS b ON b.buyer_id = lc.buyer_id "
                + " JOIN lc_pi_relation AS lpr ON lpr.lc_id=lc.lc_id "
                + " JOIN goods AS g ON g.pi_id=lpr.pi_id "
                + " WHERE lc.issue_date BETWEEN CAST( '" + fromDate + "' AS DATE) AND CAST( '" + toDate + "'  AS DATE) "
                + orderby;
        System.out.println(lcRegisterQuery);
    }

    public void readDataBase() throws Exception {
        try {
            System.out.println("In FetchDataLCRegister.readDataBase() start");
            // this will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // setup the connection with the DB.
            System.out.println("In FetchDataLCRegister.readDataBase() 1");
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/outspace?user=root");
            // System.out.println("Conneted");
            // statements allow to issue SQL queries to the database
            System.out.println(lcRegisterQuery);
            statement = connect.createStatement();
            // resultSet gets the result of the SQL query
            resultSet = statement.executeQuery(lcRegisterQuery);
            System.out.println("In FetchDataLCRegister.readDataBase() 3");
            // piRegisterList = new ArrayList<PIRegister>();
            lcList = new ArrayList<LC>();
            writeResultSet(resultSet);
            System.out.println("In FetchDataLCRegister.readDataBase() End");
            // preparedStatements can use variables and are more efficient

        } catch (Exception e) {
            System.out.print(e);
            //throw e;
        } finally {
            // close();
        }

    }

    private void writeResultSet(ResultSet resultSet) throws SQLException {
        // resultSet is initialised before the first data set
        while (resultSet.next()) {
            String buyer = resultSet.getString("name");
            String lc_id = resultSet.getString("lc_id");
            String issue_date = resultSet.getString("issue_date");
            String shipment_date = resultSet.getString("shipment_date");
            String expiry_date = resultSet.getString("expiry_date");
            String sight_days = resultSet.getString("sight_days");
            String pi_id = resultSet.getString("pi_id");
            String count = resultSet.getString("description");
            double quantity = Double.parseDouble(resultSet
                    .getString("quantity"));
            double rate = Double.parseDouble(resultSet.getString("unit_price"));
//            processPI(buyer, description, offer_validity_date, payment_period, pi_date, pi_id, quantity, rate);
              processLC(buyer, lc_id, issue_date, shipment_date, expiry_date, sight_days, pi_id, count,quantity,rate);
        }
    }
    
    private void processLC(String buyer, String lc_id, String issue_date, String shipment_date,
            String expiry_date, String sight_days, String pi_id, String count, double quantity,double rate) {
        LC lcObject = new LC( buyer, lc_id, issue_date, shipment_date, expiry_date, sight_days);//,description,quantity,rate);
        PI piDetails = new PI(buyer, pi_id, null, expiry_date, sight_days);
        CountDetails countDetails = new CountDetails(count, quantity, rate);
        for (LC oldlcObject : lcList) {
            if (oldlcObject.getLc_id().equals(lc_id)) {
                oldlcObject.addPIDetails(piDetails,countDetails);
                return;
            }
        }
        lcList.add(lcObject);
        lcObject.addPIDetails(piDetails,countDetails);
    }
    

    public ArrayList<LC> getLCRegister() {
        return lcList;
    }
//    public ArrayList<PIRegister> getPIRegister() {
//        return piRegisterList;
//    }

    private void close(Closeable c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (Exception e) {

        }
    }

}
